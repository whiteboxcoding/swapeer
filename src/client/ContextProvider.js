
import React from 'react'
import PropTypes from 'prop-types'
import Root from 'components/Root'

class ContextProvider extends React.Component {
	static childContextTypes = {
		insertCss: PropTypes.func
	}

	static propTypes = {
		context: PropTypes.object
	}

	getChildContext () {
		return { ...this.props.context }
	}

	render () {
		return <Root { ...this.props } />
	}
}

export default ContextProvider
