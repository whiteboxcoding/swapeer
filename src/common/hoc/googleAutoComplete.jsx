import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

export default function googleAutoComplete (WrappedComponent) {
	class ReactGoogleAutocomplete extends PureComponent {
		static propTypes = {
			onPlaceSelected: PropTypes.func,
			types: PropTypes.array,
			componentRestrictions: PropTypes.object,
			bounds: PropTypes.object
		};

		constructor (props) {
			super(props)
			this.autocomplete = null
			this.event = null
		}

		componentDidMount () {
			const { componentRestrictions, bounds } = this.props
			const config = {
				bounds
			}

			if (componentRestrictions) {
				config.componentRestrictions = componentRestrictions
			}

			this.disableAutofill();
			const self = this;
			setTimeout(() => {
                self.autocomplete = window.google ? new window.google.maps.places.Autocomplete(document.getElementById('element'), config) : document.getElementById('element')
                if (!window.google) {
                    self.event = self.autocomplete.addEventListener('change', self.onPlaceChange.bind(self))
				} else {
                    self.event = self.autocomplete.addListener('place_changed', self.onSelected.bind(self))
				}
			}, 0);
		}

		disableAutofill () {
			// Autofill workaround adapted from https://stackoverflow.com/questions/29931712/chrome-autofill-covers-autocomplete-for-google-maps-api-v3/49161445#49161445
			if (window.MutationObserver) {
				const input = document.getElementById('element')
				const observerHack = new MutationObserver(() => {
					observerHack.disconnect()
					if (input) {
						input.autocomplete = 'disable-autofill'
					}
				})
				observerHack.observe(input, {
					attributes: true,
					attributeFilter: ['autocomplete']
				})
			}
		}

		componentWillUnmount () {
			if (this.event) {
				this.event.remove()
			}
		}

		onSelected () {
			if (this.props.onPlaceSelected) {
				const place = this.autocomplete.getPlace();
				if (!place.id) {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition((position) => {
                            this.props.onPlaceSelected({
                                geometry: {
                                    location: {
                                        lat: () => (position.coords.latitude),
										lng: () => (position.coords.longitude)
									}
								}
							})
						});
                    }
				} else {
                    this.props.onPlaceSelected(this.autocomplete.getPlace())
				}
			}
		}

		onPlaceChange () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((position) => {
                    this.props.onPlaceSelected({
                        geometry: {
                            location: {
                                lat: () => (position.coords.latitude),
                                lng: () => (position.coords.longitude)
                            }
                        }
                    })
                });
            }
		}

		getPlace(result, el, callback) {
            const autocompleteService = new window.google.maps.places.AutocompleteService();
            if (result.name.length > 0) {
                const d = { input: result.name, offset: result.name.length };
                autocompleteService.getPlacePredictions(d, function (list, status) {
                    if (list == null || list.length == 0) callback(null);

                    const placesService = new window.google.maps.places.PlacesService(el);
                    const ref = { 'reference': list[0].reference }

                    placesService.getDetails(ref, function (detailsResult, placesServiceStatus) {
                        if (placesServiceStatus == window.google.maps.GeocoderStatus.OK) {
                            callback(detailsResult);
                        }
                        else {
                            callback(null);
                        }
                    });
                });
            }
        }

		render () {
			const {onPlaceSelected, types, componentRestrictions, bounds, ...rest} = this.props
			return (
				<WrappedComponent
					id='element'
					{...rest}
				/>
			)
		}
	}
	const wrappedComponentName = WrappedComponent.displayName || WrappedComponent.name || 'Component'
	ReactGoogleAutocomplete.displayName = `Autocomplete(${wrappedComponentName})`
	return ReactGoogleAutocomplete
}
