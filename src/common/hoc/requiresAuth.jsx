import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { selectLoginState, selectCurrentPath } from 'common/selectors'

const allowedPaths = ['/signup', '/login', '/privacy/policy', '/authors-corner', '/contact-us', '/feature/request'];

export default function requiresAuth (WrappedComponent) {
	class AuthenticatedComponent extends PureComponent {
		static propTypes = {
			login: PropTypes.object,
			dispatch: PropTypes.func.isRequired,
			pathname: PropTypes.string
		};

		componentDidMount () {
			this._checkAndRedirect()
		}

		componentDidUpdate () {
			this._checkAndRedirect()
		}

		_checkAndRedirect () {
			const { dispatch, login, pathname } = this.props
			const { loggedIn, userData = {} } = login;
            if (!loggedIn && (pathname !== '/home')) {
                if (!allowedPaths.includes(pathname)) {
                    dispatch(push('/home'))
                }
            }
            if (loggedIn && userData.id && userData.address && (pathname === '/login' || pathname === '/signup')) {
                dispatch(push(`/home/collection/${userData.id}`))
			}
		}

		render () {
			return (
				<WrappedComponent {...this.props} />
			)
		}
	}

	const wrappedComponentName = WrappedComponent.displayName || WrappedComponent.name || 'Component'
	AuthenticatedComponent.displayName = `Authenticated(${wrappedComponentName})`

	const mapStateToProps = (state) => ({
		login: selectLoginState(state),
		pathname: selectCurrentPath(state)
	})

	return connect(mapStateToProps)(AuthenticatedComponent)
}
