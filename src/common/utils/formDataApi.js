const superagent = require('superagent');

// const BASE_SYSTEM = __baseUrl__; // eslint-disable-line no-undef

const BASE_URL = '/api';

const makeAPIUrl = (url) => `${BASE_URL}${url}`;

const helpers = {};
const methods = ['post'];

methods.forEach((method) => {
  const fn = (url) => {
    const request = superagent[method](makeAPIUrl(url));
    request.set('processData', false)
    // Add default headers
    return request;
  };

  helpers[method] = fn;
});

// I know this is sucky!
const {
  post
} = helpers;

export { post };
