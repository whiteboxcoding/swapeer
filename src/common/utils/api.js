import { getUserTokenFromCookie } from '../helpers/cookie';
const superagent = require('superagent');

// const BASE_SYSTEM = __baseUrl__; // eslint-disable-line no-undef

const BASE_URL = '/api';

const makeAPIUrl = (url) => `${BASE_URL}${url}`;

const helpers = {};
const methods = ['get', 'post', 'put', 'destroy', 'patch'];

methods.forEach((method) => {
  const fn = (url, options = {}) => {
    const verb = method === 'destroy' ? 'delete' : method;
    const request = superagent[verb](options.external ? url : makeAPIUrl(url));

    // Add default headers
    request.accept('application/json');
    request.withCredentials();
    const token = getUserTokenFromCookie() || ''
    request.set('x-auth-token', token)

    if (options.external) {
      request.set('Access-Control-Allow-Origin', '*');
    }


    return request;
  };

  helpers[method] = fn;
});

// I know this is sucky!
const {
  get,
  post,
  put,
  destroy,
  patch,
} = helpers;

export { get, post, put, destroy, patch };
