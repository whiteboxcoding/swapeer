import get from 'lodash/get'
/**
 * Direct selector to the login state domain
 */
const selectLoginState = (state) => get(state, 'login')

const selectUserDetailsState = (state) => get(state, 'login.userDetails', {})


/**
 * Other specific selectors
 */

/**
 * Default selector used by Login
 */

export {
	selectLoginState,
    selectUserDetailsState
}
