import get from 'lodash/get'
/**
 * Direct selector to the login state domain
 */
const selectCurrentPath = (state) => get(state, 'routing.location.pathname', '');
const selectNewUserData = (state) => get(state, 'app.newUserData', {});
const selectMessagePopup = (state) => get(state, 'app.messagePopup', {});
const selectFollowers = (state) => get(state, 'app.followers', []);
const selectFollowings = (state) => get(state, 'app.followings', []);
const selectSearchedUsersData = (state) => get(state, 'app.searchedUser', []);
const selectSearchedProfileState = (state) => get(state, 'app.searchedProfile', {});
const selectSearchBarState = (state) => get(state, 'app.openSearchBar', false);
const selectReadersStats = (state) => get(state, 'app.readersStats', {});

/**
 * Other specific selectors
 */

/**
 * Default selector used by Login
 */

export {
	selectCurrentPath,
    selectNewUserData,
    selectMessagePopup,
    selectFollowers,
    selectFollowings,
    selectSearchedUsersData,
    selectSearchedProfileState,
    selectSearchBarState,
    selectReadersStats
}
