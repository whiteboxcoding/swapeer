import get from 'lodash/get'
/**
 * Direct selector to the collection state domain
 */
const selectUserCollections = (state) => get(state, 'collection.collection', [])


export {
	selectUserCollections
}
