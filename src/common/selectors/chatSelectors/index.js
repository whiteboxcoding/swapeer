import get from 'lodash/get'
/**
 * Direct selector to the collection state domain
 */
const selectUserChat = (state) => get(state, 'chat.userMessages', {})


export {
    selectUserChat
}
