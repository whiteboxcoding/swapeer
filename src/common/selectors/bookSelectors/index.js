import get from 'lodash/get'
/**
 * Direct selector to the login state domain
 */
const selectSearchedBooksState = (state) => get(state, 'books.searchedBooks', {})
const selectUserBooksState = (state) => get(state, 'books.userBooks', {})

const selectCollectionsBook = (state) => get(state, 'books.books', {});

const selectSearchedBookModal = (state) => get(state, 'books.searchedBookModal', {});

const selectUserWishlist = (state) => get(state, 'books.userWishlist', {})

const selectAddBookImage = (state) => get(state, 'books.addBookImage', {});

const selectUserBookHistory = (state) => get(state, 'books.userBookHistory', {});

export {
	selectSearchedBooksState,
	selectCollectionsBook,
    selectAddBookImage,
    selectUserWishlist,
    selectSearchedBookModal,
    selectUserBooksState,
		selectUserBookHistory,
}
