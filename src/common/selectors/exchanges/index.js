import get from 'lodash/get'

const selectExchangeState = (state) => get(state, 'exchanges.exchanges');
const selectDonatesState = (state) => get(state, 'exchanges.donates');
const selectRequestsState = (state) => get(state, 'exchanges.requests');

const  selectRequestedSwaps = (state) => get(state, 'exchanges.requestedSwaps', {});

export {
    selectExchangeState,
    selectRequestedSwaps,
    selectDonatesState,
    selectRequestsState
}
