// @flow
import React, {PureComponent} from 'react'
import { Grid, Image, Button } from 'semantic-ui-react'
import config from '../../../../config/environments'
import io from 'socket.io-client';


type Props = {
    user: Object,
    isActiveUser: Boolean,
    getChatMessages: Function,
    saveNewMessages: Function,
    updatePageUrl: Function,
    userId: String,
}

class UserProfileImage extends PureComponent<Props> {
    constructor() {
        super()
        this.onUserImageClick = this.onUserImageClick.bind(this)
    }

    onUserImageClick() {
        const { user, isActiveUser, userId } = this.props;
        this.props.updatePageUrl(null, {
            tab: 'messages',
            userId: user.id,
        })
        const that = this;
        this.props.getUserDetails({
            user_id: user.id
        });
        this.props.getChatMessages({
            user_id: userId,
            other_user_id : user.id,
            count: 0
        });
        this.props.getUserRequestedSwaps({
            swap_user_id: user.id
        })
        if (this.socket) {
            this.socket.emit('room', `${user.id}/${userId}`);
            this.socket.on('new_message', (message) => {
                const messageObj = {
                    message,
                    from: user.id,
                    to: userId,
                    id: Math.random()
                }
                that.props.saveNewMessages(messageObj)
            })
        } else {
            that.socket = io(`${config.frontendBase}/socket/message`);
            that.socket.on('connect', (soc) => {
                that.socket.emit('room', `${user.id}/${userId}`);
                that.socket.on('new_message', (message) => {
                    const messageObj = {
                        message,
                        from: user.id,
                        to: userId,
                        id: Math.random()
                    }
                    that.props.saveNewMessages(messageObj)
                })
            });
        }
    }
    componentDidMount() {
        const { user, isActiveUser, userId } = this.props;
        const that = this;
        if (isActiveUser) {
            this.props.getUserDetails({
                user_id: user.id
            });
            that.socket = io(`${config.frontendBase}/socket/message`);
            that.socket.on('connect', (soc) => {
                that.socket.emit('room', `${user.id}/${userId}`);
                that.props.getChatMessages({
                    user_id: userId,
                    other_user_id : user.id,
                    count: 0
                });
                that.socket.on('new_message', (message) => {
                    const messageObj = {
                        message,
                        from: user.id,
                        to: userId,
                        id: Math.random()
                    }
                    that.props.saveNewMessages(messageObj)
                })
            });
            this.props.getUserRequestedSwaps({
                swap_user_id: user.id
            })
        }
    }

    render () {
        const { user, isActiveUser } = this.props
        return (
            <Grid.Column key={user.id}>
                <Button style={{ padding: 0, borderRadius: '50%' }} onClick={this.onUserImageClick}>
                    <Image centered size={isActiveUser ? 'small' : 'tiny'} circular src={user.profile_pic} />
                </Button>
            </Grid.Column>
        )
    }
}

export default UserProfileImage
