// @flow
import React, {PureComponent} from 'react'
import FormField from 'components/FormField'
import googleAutoComplete from '../../hoc/googleAutoComplete'

type Props = {
}

class GoogleAutoCompleteAddress extends PureComponent<Props> {
	render () {
		return (
			<FormField {...this.props} />
		)
	}
}

export default googleAutoComplete(GoogleAutoCompleteAddress)
