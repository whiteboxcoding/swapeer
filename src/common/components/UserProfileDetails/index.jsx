// @flow
import React, {PureComponent} from 'react'
import { Grid } from 'semantic-ui-react'
import get from 'lodash/get'
import { Text, TableText, Marker } from './style'


type Props = {
    userId: String,
    getUserDetails: Function,
    userDetails: Object,
    currentUser: boolean,
}

class UserProfileDetails extends PureComponent<Props> {
    componentDidMount() {
        const { userId, getUserDetails } = this.props
        if (getUserDetails) {
            this.props.getUserDetails({
                user_id: userId
            })
        }
    }
    render () {
        const { userDetails, currentUser } = this.props;
        let data = userDetails.data || {}
        if (data.user_profile && !data.bookmark_use && !data.favourite_genres) {
            data = {
                ...data,
                ...data.user_profile,
            }
            delete data.user_profile;
            if (!data.favourite_genres) {
                data.favourite_genres = []
            }
            if (!data.bookmark_use) {
                data.bookmark_use = []
            }
        }
        if (typeof data.favourite_genres === 'string') {
            data.favourite_genres = data.favourite_genres.split(',')
        }
        if (typeof data.bookmark_use === 'string') {
            data.bookmark_use = data.bookmark_use.split(',')
        }

        return (
            <div>
                <h2>{get(data, 'name', '')}</h2>
                <Text>
                    <p>{get(data, 'email', '')}</p>
                    {
                        (get(data, 'follow_you', '')) &&
                        <p>Follows you</p>
                    }
                    <p style={{ marginTop: '0.375rem' }}>
                        <Marker src={'/images/marker.png'} />
                        <span>{get(data, 'address', '')}</span>
                    </p>
                </Text>
                <Text>{get(data, 'book_read_story', '')}</Text>
                <Text>{get(data, 'blog_link', '')}</Text>
                <h4>{get(data, 'name', '')}&#39;s Stats:</h4>
                <Grid columns={2}>
                    {/*<TableText>Saved:</TableText>*/}
                    {/*<TableText>--</TableText>*/}
                    {/*<TableText>Donated:</TableText>*/}
                    {/*<TableText>--</TableText>*/}
                    <TableText>Followers:</TableText>
                    <TableText>{get(data, 'followers', '')}</TableText>
                    <TableText>Following:</TableText>
                    <TableText>{get(data, 'following', '')}</TableText>
                    <TableText>Genres:</TableText>
                    <TableText>{get(data, 'favourite_genres', []).join(', ')}</TableText>
                    <TableText>My top 3:</TableText>
                    <TableText>
                        <Text>{get(data, 'bookmark_use', []).join(', ')}</Text>
                    </TableText>
                </Grid>
            </div>
        )
    }
}

export default UserProfileDetails
