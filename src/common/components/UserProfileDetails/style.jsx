import styled from 'styled-components'
import { Grid } from 'semantic-ui-react'

export const Text = styled.p`
    font-size: 1.1rem;
`;

export const Marker = styled.img`
    width: 1.8rem;
    vertical-align: middle;
    margin-right: 1rem;
`;

export const TableText = styled(Grid.Column)`
    font-size: 1.1rem;
    padding-bottom: 0
`;
