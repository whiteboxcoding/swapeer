// @flow
import React, {PureComponent} from 'react'
import { Grid, Header, Button, Image, Modal, Segment, Divider } from 'semantic-ui-react'
import { CloseBtn } from '../../styles/common'
import {getModalMountNode} from "../../helpers/functions";

type Props = {
    text: String,
    buttonText: String
}

class NoDataFoundScreen extends PureComponent<Props> {
    constructor() {
        super();
        this.state = {
          inviteModal: false,
        };
        this.openInviteModal = this.openInviteModal.bind(this)
        this.closeInviteModal = this.closeInviteModal.bind(this)
    }

    openInviteModal() {
        this.setState({
            inviteModal: true
        })
    }

    closeInviteModal() {
        this.setState({
            inviteModal: false
        })
    }

    render () {
        const { text, buttonText } = this.props
        return (
            <Grid columns={1} verticalAlign={'middle'} style={{ height: '60vh' }}>
                <Grid.Column textAlign={'center'}>
                    <Image centered src={'/images/look-like-no-has-your-wishlist.png'} />
                </Grid.Column>
                {
                    text &&
                    <Grid.Column textAlign={'center'}>
                        <Header as={'h3'}>{text}</Header>
                    </Grid.Column>
                }
                {
                    buttonText &&
                    <Grid.Column textAlign={'center'}>
                        <Modal
                            onClose={this.closeInviteModal}
                            mountNode={getModalMountNode()}
                            size='tiny'
                            closeIcon={<CloseBtn><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>}
                            trigger={<Button onClick={this.openInviteModal} style={{borderRadius: '6.25rem'}} primary>{buttonText}</Button>}
                        >
                            <Modal.Content>
                              <Segment basic textAlign='center' padded>
                                <Header textAlign={'center'} as={'h2'}>Invite your friends to share books!</Header>
                                <Divider hidden />
                                <a target='_blank' href="https://web.whatsapp.com/send?text=http://www.readsnet.com" data-action="share/whatsapp/share">
                                  <img style={{ width: '6rem', margin: '1rem' }} src='/images/whatsapp.svg' />
                                </a>
                                <a target='_blank' href={'https://www.facebook.com/sharer/sharer.php?u=http://www.readsnet.com'}>
                                    <img style={{ width: '6rem', margin: '1rem' }} src='/images/facebook.svg' />
                                </a>
                                <a target='_blank' href={'https://twitter.com/home?status=http://www.readsnet.com'}>
                                    <img style={{ width: '6rem', margin: '1rem' }} src='/images/twitter.svg' />
                                </a>
                                <Divider hidden />
                                <Segment size={'big'} basic textAlign={'center'}>
                                    <b>Copy link:</b>&nbsp;&nbsp;&nbsp;
                                    <span style={{ border: '1px solid', padding: '0.5rem' }}>http://www.readsnet.com</span>
                                </Segment>
                              </Segment>
                            </Modal.Content>
                        </Modal>
                    </Grid.Column>
                }
            </Grid>
        )
    }
}

export default NoDataFoundScreen
