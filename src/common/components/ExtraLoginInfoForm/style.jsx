import styled from 'styled-components'
import { Form, Header, Input, Divider, Grid, Segment, Button, Image } from 'semantic-ui-react'
import FormField from 'components/FormField'
import FormDropDown from 'components/FormDropDown'


export const FormTitle = styled.h3`
    font-size: 1.875rem;
    margin: 0;
    text-align: center;
`;

export const FormInput = styled(FormField)`
    width: 80%;
`;

export const StyledFormDropDown = styled(FormDropDown)`
    width: 80%;
`;

export const AddImageBtn = styled(Button)`
    width: 15rem;
    height: 17rem;
    font-size: 1.5rem !important;
`;

export const HiddenInput = styled.input`
    display: none;
`;

export const ProfileImg = styled(Image)`
    max-width: 18rem !important;
    max-height: 18rem !important;
    margin: 0 auto;
`;

export const ChangePicture = styled(Button)`
    position: absolute;
    bottom: -5px;
    height: 25%;
    width: 100%;
    left: 0;
`;
