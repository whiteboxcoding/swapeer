// @flow
import React, {PureComponent} from 'react'
import { Form, Grid, Button, Dimmer, Loader } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { required, validEmail } from 'common/validators'
import GoogleAutoCompleteAddress from '../GoogleAutoCompleteAddress'
import { dataURItoBlob } from '../../helpers/functions'
import FormDropdown from '../FormDropDown'
import countryCode from '../../countryCode';
import {
    FormTitle,
    FormInput,
    AddImageBtn,
    HiddenInput,
    ProfileImg,
    ChangePicture
} from './style'

type Props = {
  viewType: String,
  handleSubmit: Function,
  getUserData: Function,
    changeLocationValue: Function,
    saveLatLngValue: Function,
    uploadProfileImage: Function,
    initialValues: Object,
}

class ExtraLoginInfoForm extends PureComponent<Props> {
	constructor(props) {
		super();
		this.state = {
            url: props.profileImage,
		};
		this.onPlaceSelected = this.onPlaceSelected.bind(this);
		this.getProfileImageView = this.getProfileImageView.bind(this);
		this.onFileInputChange = this.onFileInputChange.bind(this);
	}

	componentDidMount() {
		this.props.getUserData();
	}

    onPlaceSelected(placeData) {
        const lat = placeData.geometry.location.lat();
        const lng = placeData.geometry.location.lng();
        this.props.saveLatLngValue({
            lat,
            lng
		}, placeData.formatted_address);
		this.props.changeLocationValue(placeData.formatted_address)
	}

    triggerFileUpload(e) {
		e.preventDefault();
        e.currentTarget.nextElementSibling.click();
	}

    onFileInputChange(e) {
		const { files } = e.currentTarget;
		const file = files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        const self = this
        reader.onloadend = () => {
            const img = new Image();
            img.onload = function() {
                const cvs = document.createElement('canvas');
                const minSize = Math.min(img.width, img.height);
                const left = (img.width - minSize) / 2;
                const top = (img.height - minSize) / 2;
                const ctx = cvs.getContext('2d');
                ctx.canvas.width = 250;
                ctx.canvas.height = 250;
                ctx.drawImage(img, left, top, minSize, minSize, 0, 0, ctx.canvas.width, ctx.canvas.height);
                const newImageData = cvs.toDataURL('image/jpeg', 0.7);
                const blob = dataURItoBlob(newImageData);
                const fileObj = new File([blob], file.name);
                self.setState({
                    url: newImageData,
                });
                self.props.uploadProfileImage([fileObj])
                    .then(() => {
                        self.setState({
                            url: '',
                        });
                    })
                    .catch(() => {
                        self.setState({
                            url: '',
                        });
                    })
            };
            img.src = reader.result;
        };
	}

    getProfileImageView(url) {
      const { profileImage } = this.props
      if (profileImage) {
        return (
            <div style={{ display: 'inline-block', position: 'relative', clipPath: 'circle(50% at center)' }}>
                <ProfileImg src={profileImage} size='medium' circular />
                <ChangePicture primary onClick={this.triggerFileUpload}>Change Picture</ChangePicture>
                <HiddenInput onChange={this.onFileInputChange} type={'file'} />
            </div>
        )
      } else if (url) {
         return (
             <div>
                 <Dimmer active inverted>
                     <Loader inverted>Loading</Loader>
                 </Dimmer>
                <ProfileImg style={{ opacity: 0.4 }} src={url} size='medium' circular />
             </div>
         )
      }
        return (
            <div>
                <AddImageBtn basic color={'black'} onClick={this.triggerFileUpload}>Click to add a picture</AddImageBtn>
                <HiddenInput onChange={this.onFileInputChange} type={'file'} />
            </div>
        )
	}

	render () {
		const { viewType, initialValues = {}, profileImage } = this.props;
		const { url } = this.state;
		return (
			<Form onSubmit={this.props.handleSubmit}>
				<FormTitle>Define your reading space</FormTitle>
				<Grid>
					<Grid.Row only={'tablet computer'}>
						<Grid.Column width={8}>
							<p style={{ marginTop: '1.5rem' }}>Name</p>
							<Field component={FormInput} disabled={initialValues.name} validate={required} name='name' />
							<p style={{ marginTop: '1.5rem' }}>Email Id</p>
							<Field component={FormInput} disabled={initialValues.email} validate={[required, validEmail]} name='email' />
							<p style={{ marginTop: '1.5rem' }}>Address</p>
							<Field
								componentRestrictions={{country: 'in'}}
								component={GoogleAutoCompleteAddress}
								validate={required}
								style={{ width: '80%' }}
								name='address'
								onPlaceSelected={this.onPlaceSelected}
							/>
							<p style={{ marginTop: '1.5rem' }}>Contact Number</p>
                            <Grid>
                                <Grid.Column width={3}>
                                    <Field
                                        validate={required}
                                        component={FormDropdown}
                                        fluid
                                        selection
                                        search
                                        options={countryCode}
                                        name="countryCode"
                                    />
                                </Grid.Column>
                                <Grid.Column width={12}>
                                    <Field type='number' validate={required} component={FormInput} name='contact_no' />
                                </Grid.Column>
                            </Grid>
						</Grid.Column>
						<Grid.Column width={8} textAlign={'center'} verticalAlign={'middle'}>
							{
								this.getProfileImageView(url)
							}
						</Grid.Column>
						<Grid.Column width={16} textAlign='right'>
							<Button style={{ borderRadius: '6.25rem' }} primary type='submit'>Next</Button>
						</Grid.Column>
					</Grid.Row>
                    <Grid.Row only={'mobile'}>
                        <Grid.Column width={16} textAlign={'center'} style={{ margin: '5rem 0' }} verticalAlign={'middle'}>
                            {
                                this.getProfileImageView(url)
                            }
                        </Grid.Column>
                        <Grid.Column width={16}>
                            <p style={{ marginTop: '1.5rem' }}>Name</p>
                            <Field component={FormInput} disabled={initialValues.name} validate={required} name='name' />
                            <p style={{ marginTop: '1.5rem' }}>Email Id</p>
                            <Field component={FormInput} disabled={initialValues.email} validate={[required, validEmail]} name='email' />
                            <p style={{ marginTop: '1.5rem' }}>Address</p>
                            <Field
                                componentRestrictions={{country: 'in'}}
                                component={GoogleAutoCompleteAddress}
                                validate={required}
                                style={{ width: '80%' }}
                                name='address'
                                onPlaceSelected={this.onPlaceSelected}
                            />
                            <p style={{ marginTop: '1.5rem' }}>Contact Number</p>
                            <Grid>
                                <Grid.Column width={5}>
                                    <Field
                                        validate={required}
                                        component={FormDropdown}
                                        fluid
                                        selection
                                        search
                                        options={countryCode}
                                        name="countryCode"
                                    />
                                </Grid.Column>
                                <Grid.Column width={10}>
                                    <Field type='number' validate={required} component={FormInput} name='contact_no' />
                                </Grid.Column>
                            </Grid>
                        </Grid.Column>
                        <Grid.Column width={16} textAlign='right'>
                            <Button style={{ borderRadius: '6.25rem' }} primary type='submit'>Next</Button>
                        </Grid.Column>
                    </Grid.Row>
				</Grid>
			</Form>
		)
	}
}

export default reduxForm({
	form: 'extraInfoForm',
	enableReinitialize: true,
})(ExtraLoginInfoForm)
