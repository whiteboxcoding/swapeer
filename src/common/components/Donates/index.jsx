// @flow
import React, {PureComponent} from 'react'
import { List, Image, Segment, Grid, Icon, Button, Popup, Modal, Form, Divider } from 'semantic-ui-react'
import get from 'lodash/get'
import { NavLink } from 'react-router-dom'
import { TextWrapper, ItemWrapper, Text, ChatIcon, FollowingBtn } from './style';
import UserDetailsCard from '../UserDetailsCard'
import NoDataFoundScreen from '../NoDataFoundScreen'
import DataLoadingScreen from '../DataLoadingScreen'
import { getModalMountNode } from '../../helpers/functions'
import { CloseBtn } from '../../styles/common'

type Props = {
    getUserDonatesList: Function,
    updateFollowStatus: Function,
    params: Object,
    donates: Object,
    getUserDetails: Function,
    requestUserBookSwap: Function,
    requestForGiveaway: Function,
    userDetails: Object,
}

class Donates extends PureComponent<Props> {
    constructor() {
        super()
        this.state = {
            donateBookModal: {
                open: false,
                data: {}
            }
        }
        this.getDonateButtons = this.getDonateButtons.bind(this)
        this.unfollowUser = this.unfollowUser.bind(this)
        this.followUser = this.followUser.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.onFormSubmit = this.onFormSubmit.bind(this)
        this.openDonateBookModal = this.openDonateBookModal.bind(this)
        this.getComponentView = this.getComponentView.bind(this)
    }
	componentDidMount() {
		const { params } = this.props;
        this.props.getUserDonatesList({
            user_id: params.userId,
            count: 0
        })
	}

    openDonateBookModal(data) {
        this.setState({
            donateBookModal: {
                open: true,
                data
            }
        })
    }

    closeModal(e) {
        e.preventDefault()
        this.setState({
            donateBookModal: {
                open: false,
                data: {}
            }
        })
    }

    onFormSubmit(e) {
        const { params } = this.props;
        const val = e.currentTarget.elements[0].value
        const { data } = this.state.donateBookModal
        this.closeModal(e)
        this.props.requestForGiveaway({
            user_to: {},
            user_from: {
                id: data.id,
                name: data.name,
                book: data.book,
                user_book_id: data.user_book_id,
                profile_pic: data.profile_pic
            },
            message: val
        })
        this.props.getUserDonatesList({
            user_id: params.userId,
            count: 0
        })
    }

	getDonateButtons(donateObj, bookObj) {
        const { request } = bookObj
        const data = {
            name: donateObj.name,
            id: donateObj.id,
            profile_pic: donateObj.profile_pic || '',
            bookName: get(bookObj, 'book.name', ''),
            authors: get(bookObj, 'book.authors', []).join(', '),
            user_book_id: bookObj.user_book_id,
            book: bookObj.book,

        }
        if (request) {
            if (request.status !== 'accepted') {
                return (
                    <Button size='tiny' color={'red'} style={{ borderRadius: '6.25rem' }}>
                        <b style={{ color: 'black' }}>Requested</b>
                    </Button>
                )
            }
            return (
                <NavLink to={`?tab=messages&userId=${donateObj.id}`}>
                    <ChatIcon src={'/images/chatIcon.svg'} />
                </NavLink>
            )
        }
        return (
            <Button onClick={() => this.openDonateBookModal(data)} size='tiny' secondary style={{ borderRadius: '6.25rem' }}>
                <b style={{ color: 'black' }}>Request Book</b>
            </Button>
        )
    }

    unfollowUser(userId) {
        this.props.updateFollowStatus({
            follow: false,
            following_id: userId
        }, 'donate')
    }

    followUser(userId) {
        this.props.updateFollowStatus({
            follow: true,
            following_id: userId
        }, 'donate')
    }

    getComponentView(donates, donateBookModal, userDetails) {
        if (get(donates, 'data', []).length === 0 && donates.status !== 'fetched') {
            return (
                <DataLoadingScreen text={'Checking for exchanges and donations for your wishlist...'} />
            )
        }
        if (get(donates, 'data', []).length === 0) {
            return (
                <NoDataFoundScreen
                    text={'Looks like no one has your wishlist yet :('}
                    buttonText={'Invite friends to join for more books!'}
                />
            )
        }
        let booksCount = 0;
        get(donates, 'data', []).forEach((donateObj) => {
            booksCount += donateObj.books.length
        })
        const userCount = get(donates, 'data', []).length
        return (
            <div>
                <Text style={{ textAlign: 'center' }}>You matched with {(userCount === 1) ? '1 reader' : `${userCount} readers`} for {(booksCount === 1) ? '1 new book' : `${booksCount} new books!`} Add more books/wishlist
                    for more :)</Text>
                <Segment padded basic>
                    {
                        get(donates, 'data', []).map((donateObj) => (
                            donateObj.books.map((bookObj) => (
                                <ItemWrapper key={bookObj.id} verticalAlign='middle'>
                                    <Grid.Column mobile={4} tablet={1} computer={1}>
                                        <Image circular src={donateObj.profile_pic} />
                                    </Grid.Column>
                                    <TextWrapper only={'mobile'} tablet={12}>
                                        <Popup
                                            style={{ padding: 0, width: '20rem', maxWidth: '18.75rem' }}
                                            mountNode={getModalMountNode()}
                                            onOpen={() => this.props.getUserDetails({ user_id: donateObj.id })}
                                            content={<UserDetailsCard userData={userDetails.data} />}
                                            trigger={<Text style={{ marginBottom: '0.5rem' }}>{donateObj.name}</Text>}
                                        />
                                        <Text
                                            style={{ marginBottom: '0.5rem' }}>
                                            {get(bookObj, 'book.name', '')}
                                        </Text>
                                        <Text style={{ marginBottom: '2.5rem' }}>{Math.round(donateObj.distance / 1000)} km</Text>
                                        {this.getDonateButtons(donateObj, bookObj)}&nbsp;&nbsp;
                                        {
                                            donateObj.following
                                                ? (
                                                    <FollowingBtn size='tiny' onClick={() => this.unfollowUser(donateObj.id)}>
                                                        <b>Following</b>
                                                    </FollowingBtn>
                                                ) : (
                                                    <Button onClick={() => this.followUser(donateObj.id)} style={{ borderRadius: '6.25rem' }} size='tiny' basic color='teal'>
                                                        <b>Follow</b>
                                                    </Button>
                                                )
                                        }
                                    </TextWrapper>
                                    <TextWrapper only={'tablet computer'} tablet={15} computer={15}>
                                        <Grid verticalAlign={'middle'}>
                                            <Grid.Column tablet={3} computer={3}>
                                                <Popup
                                                    style={{ padding: 0, width: '20rem', maxWidth: '18.75rem' }}
                                                    mountNode={getModalMountNode()}
                                                    onOpen={() => this.props.getUserDetails({ user_id: donateObj.id })}
                                                    content={<UserDetailsCard userData={userDetails.data} />}
                                                    trigger={<Text>{donateObj.name}</Text>}
                                                />
                                            </Grid.Column>
                                            <Grid.Column textAlign={'center'} tablet={5} computer={5}>
                                                <Text
                                                    style={{ width: '100%', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                                                    {get(bookObj, 'book.name', '')}
                                                </Text>
                                            </Grid.Column>

                                            <Grid.Column textAlign={'center'} tablet={3} computer={3}>
                                                <Text>{Math.round(donateObj.distance / 1000)} km</Text>
                                            </Grid.Column>
                                            <Grid.Column textAlign={'center'} tablet={3} computer={3}>
                                                {this.getDonateButtons(donateObj, bookObj)}
                                            </Grid.Column>
                                            <Grid.Column textAlign={'center'} tablet={2} computer={2}>
                                                {
                                                    donateObj.following
                                                        ? (
                                                            <FollowingBtn size='tiny' onClick={() => this.unfollowUser(donateObj.id)}>
                                                                <b>Following</b>
                                                            </FollowingBtn>
                                                        ) : (
                                                            <Button onClick={() => this.followUser(donateObj.id)} style={{ borderRadius: '6.25rem' }} size='tiny' basic color='teal'>
                                                                <b>Follow</b>
                                                            </Button>
                                                        )
                                                }
                                            </Grid.Column>
                                        </Grid>
                                    </TextWrapper>
                                </ItemWrapper>
                            ))
                        ))
                    }
                    <Modal
                        size='tiny'
                        open={donateBookModal.open}
                        mountNode={getModalMountNode()}
                        onClose={this.closeModal}
                        closeIcon={<CloseBtn><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>}
                    >
                        <Modal.Content>
                            <p style={{ fontSize: '1.5rem', paddingRight: '3rem' }}>
                                Request <span style={{ color: '#2CBAE5' }}>{donateBookModal.data.name}</span> to
                                donate his/her <span style={{ color: '#2CBAE5' }}>{donateBookModal.data.bookName}</span> by
                                {donateBookModal.data.authors} to you
                            </p>
                            <Form onSubmit={this.onFormSubmit}>
                                <textarea style={{ height: '8em' }} name='message' placeholder='Type a message to let them know how much you want to read the book!' />
                                <Divider hidden />
                                <div style={{ textAlign: 'right' }}>
                                    <Button style={{ borderRadius: '6.25rem' }} size='tiny' secondary onClick={this.closeModal}>
                                        <b style={{ color: 'black' }}>Cancel</b>
                                    </Button>
                                    <Button style={{ borderRadius: '6.25rem' }} size='tiny' secondary type='submit'>
                                        <b style={{ color: 'black' }}>Request</b>
                                    </Button>
                                </div>
                            </Form>
                        </Modal.Content>
                    </Modal>
                </Segment>
            </div>
        )
    }

	render () {
		const { donates, userDetails } = this.props;
		const { donateBookModal } = this.state
		return this.getComponentView(donates, donateBookModal, userDetails)
	}
}

export default Donates
