import styled from 'styled-components'
import { Grid, Button } from 'semantic-ui-react'

export const TextWrapper = styled(Grid.Column)`
    border: 1px solid #707070;
`;

export const ItemWrapper = styled(Grid)`
    position: relative;
    margin: 0.625rem 0 !important;
`;

export const Text = styled.p`
    font-size: 1.5rem;
`;

export const ChatIcon = styled.img`
    width: 2.1rem;
`;

export const CloseIcon = styled.img`
    width: 1.3rem;
`;

export const FollowingBtn = styled(Button)`
    border-radius: 6.25rem;
    background: #2CBAE5;
`;
