// @flow
import React, {PureComponent} from 'react'
import GiveAwayForm from '../GiveAwayForm'
import SwapRequestForm from '../SwapRequestForm'
import { Menu, Segment } from 'semantic-ui-react'
import get from 'lodash/get'

type Props = {
    userId: String,
    getUserBooksList: Function,
    getUsersList: Function,
    userBooksList: Object,
    searchedUser: Array,
    searchedBooks: Object,
    requestUserBookSwap: Function,
    requestForGiveaway: Function
}

class SwapRequestModal extends PureComponent<Props> {
    constructor() {
        super()
        this.state = {
            activeTab: 'Giveaway',
        };
        this.handleItemClick = this.handleItemClick.bind(this)
        this.onSwapRequestSubmit = this.onSwapRequestSubmit.bind(this)
        this.onGiveAwayRequestSubmit = this.onGiveAwayRequestSubmit.bind(this)
    }

    handleItemClick (e, { name }) {
        this.setState({ activeTab: name })
    }

    onGiveAwayRequestSubmit(data) {
        const { userBooksList, searchedUser, userId } = this.props;
        const userToId = get(data, 'user_to.id', '')
        const userFromBookList = get(userBooksList, `data.${userId}`, [])
        const userFromBookObj =  userFromBookList.find((obj) => (obj.book.id === get(data, 'user_from.book.id', ''))) || {}
        const userToData = searchedUser.find((obj) => (obj.id === userToId)) || {}
        data.user_to.name = userToData.name
        data.user_to.profile_pic = userToData.profile_pic
        data.user_from.user_book_id = userFromBookObj.id
        data.user_from.book = userFromBookObj.book
        data.reverse = true
        this.props.requestForGiveaway(data)
    }

    onSwapRequestSubmit(data) {
        const { userBooksList, searchedUser, searchedBooks, userId } = this.props;
        const userToId = get(data, 'user_to.id', '')
        const userToBookList = get(userBooksList, `data.${userToId}`, [])
        const userFromBookList = get(userBooksList, `data.${userId}`, [])
        const userToBookObj =  userToBookList.find((obj) => (obj.book.id === get(data, 'user_to.book.id', ''))) || {}
        const userFromBookObj =  userFromBookList.find((obj) => (obj.book.id === get(data, 'user_from.book.id', ''))) || {}
        const userToData = searchedUser.find((obj) => (obj.id === userToId)) || {}
        data.user_to.name = userToData.name
        data.user_to.profile_pic = userToData.profile_pic
        data.user_to.user_book_id = userToBookObj.id
        data.user_to.book = userToBookObj.book
        data.user_from.user_book_id = userFromBookObj.id
        data.user_from.book = userFromBookObj.book
        data.reverse = true
        this.props.requestUserBookSwap(data)
    }

    render() {
        const { activeTab } = this.state
        const { userId, userBooksList, searchedUser, searchedBooks } = this.props;
        return (
            <Segment padded basic>
                <Menu pointing secondary>
                    <Menu.Item
                        name='Giveaway'
                        style={{ fontSize: '1.5rem' }}
                        active={activeTab === 'Giveaway'}
                        onClick={this.handleItemClick}
                    />
                    <Menu.Item
                        name='Swap'
                        style={{ fontSize: '1.5rem' }}
                        active={activeTab === 'Swap'}
                        onClick={this.handleItemClick}
                    />
                </Menu>
                <Segment padded basic>
                    {
                        (activeTab === 'Giveaway')
                        ? (
                            <GiveAwayForm
                                userBooksList={userBooksList}
                                getUsersList={this.props.getUsersList}
                                searchedUser={searchedUser}
                                userId={userId}
                                onSubmit={this.onGiveAwayRequestSubmit}
                                getUserBooksList={this.props.getUserBooksList}
                            />
                        )
                        : (
                            <SwapRequestForm
                                userBooksList={userBooksList}
                                getUsersList={this.props.getUsersList}
                                searchedUser={searchedUser}
                                onSubmit={this.onSwapRequestSubmit}
                                userId={userId}
                                searchedBooks={searchedBooks}
                                getUserBooksList={this.props.getUserBooksList}
                            />
                        )
                    }
                </Segment>
            </Segment>
        )
    }
}

export default SwapRequestModal
