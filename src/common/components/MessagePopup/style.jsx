import styled from 'styled-components'
import { Message } from 'semantic-ui-react'

export const PopupMessage = styled(Message)`
    position: fixed;
    left: 30%;
    width: 40%;
    bottom: 0;
    z-index: 100000;
    text-align: center;
`;

export const PopupText = styled.p`
    font-size: 1.3rem;
    font-weight: bold;
`;
