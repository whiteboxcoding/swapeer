// @flow
import React, {PureComponent} from 'react'
import { PopupMessage, PopupText } from './style'

type Props = {
    data: Object,
    hideMessagePopup: Function,
}

class MessagePopup extends PureComponent<Props> {
    componentDidMount() {
        const that = this;
        setTimeout(() => that.props.hideMessagePopup(), 5000)
    }
    render () {
        const { data } = this.props;
        return (
            <PopupMessage positive={data.type === 'success'} negative={data.type === 'error'}>
                <PopupText>{data.text}</PopupText>
            </PopupMessage>
        )
    }
}

export default MessagePopup
