/**
 *
 * StarRating
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StarLabel = styled.label`
  font-size: 1.3rem;
  overflow: hidden;
  &:before {
    content: '★ ';
  }
`;

function StarRating({ rating, className, updateRating }) {
    const goldColor = { color: 'gold' };
    const greyColor = { color: 'lightgrey' };
    return (
        <div className={className}>
            <StarLabel name={1} style={rating >= 1 ? goldColor : greyColor } onClick={updateRating || null} selected={rating >= 1} />
            <StarLabel name={2} style={rating >= 2 ? goldColor : greyColor } onClick={updateRating || null} selected={rating >= 2} />
            <StarLabel name={3} style={rating >= 3 ? goldColor : greyColor } onClick={updateRating || null} selected={rating >= 3} />
            <StarLabel name={4} style={rating >= 4 ? goldColor : greyColor } onClick={updateRating || null} selected={rating >= 4} />
            <StarLabel name={5} style={rating >= 5 ? goldColor : greyColor } onClick={updateRating || null} selected={rating === 5} />
        </div>
    );
}

StarRating.propTypes = {
  rating: PropTypes.number,
  className: PropTypes.string,
  updateRating: PropTypes.func,
};

export default StarRating;
