import styled from 'styled-components'
import { Sidebar, Segment, Input, Grid, Icon, Button, Menu } from 'semantic-ui-react'
export const StyledPusher = styled(Sidebar.Pusher)`
  overflow-y: scroll !important;
`
