// @flow
import React, {PureComponent} from 'react'
import { Sidebar, Segment, Input, Grid, Icon, Button, Menu } from 'semantic-ui-react'
import { StyledPusher } from './style'

type Props = {
  show: Boolean,
  children: React$Node,
  hideMenuSideBar: Function,
  pathname: String,
  updateRoute: Function
}

class MenuSideBar extends PureComponent<Props> {
	render () {
		const { children, show, pathname } = this.props
		return (
			<Sidebar.Pushable as={Segment}>
				<Sidebar
					as={Menu}
					animation='overlay'
					icon='labeled'
					inverted
					onHide={this.props.hideMenuSideBar}
					vertical
					visible={show}
					width='thin'
				>
					<Menu.Item
						name='Home'
						active={pathname && pathname.includes('/home')}
						onClick={this.props.updateRoute}
					/>
					<Menu.Item
						name='Explore'
						active={pathname && pathname.includes('/explore')}
						onClick={this.props.updateRoute}
					/>
					<Menu.Item
						name='Swaps'
						active={pathname && pathname.includes('/swaps')}
						onClick={this.props.updateRoute}
					/>
				</Sidebar>
				<StyledPusher>
					{children}
				</StyledPusher>
			</Sidebar.Pushable>
		)
	}
}

export default MenuSideBar
