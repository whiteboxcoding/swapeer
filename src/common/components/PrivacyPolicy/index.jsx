// @flow
import React, {PureComponent} from 'react'
import { Grid } from 'semantic-ui-react'

type Props = {}
class PrivacyPolicy extends PureComponent<Props> {
    render () {
        return (
            <Grid container style={{ padding: '2rem 0 5rem' }} columns={1}>
                <Grid.Column>
                <h5 style={{ marginBottom: '0.3rem' }}>Privacy Policy</h5>
                <p>Your privacy is of utmost importance to us and we will work hard to ensure that your data is not misused.</p>
                <h5 style={{ marginBottom: '0.3rem' }}>Information Collection</h5>
                <p>We collect information during registration, use of the services site, communicating
                    with site members, admin, using content, content upload to our site. We also receive
                    information about your IP address, cookies and pages you visited. Such information
                    is collected to provide the best service, research, and overall product experience.</p>
                <h5 style={{ marginBottom: '0.3rem' }}>Information Usage</h5>
                <p>We require information collected to understand your needs and provide you with
                    better services.</p>
                <ol>
                    <li>We may send promotional emails about new products/events, special offers,
                        or other information which we think you may find interesting based on the
                        email-id and phone number provided by you.</li>
                    <li>We will be contacting you via email, phone or text messages, facsimile etc. to
                        deliver certain services and information that is relevant like giving you alerts
                        and notifications regarding your books and usage of platform etc.</li>
                    <li>From time to time we may reach out to you for market research purposes to
                        be able to customize your experience over the platform according to your
                        interests.</li>
                </ol>
                <h5 style={{ marginBottom: '0.3rem' }}>Information Sharing</h5>
                <p>We will always obtain your prior specific consent before we share or disclose your
                    personally identifiable information to any partner, and user. We are also accountable
                    to disclose user information and the contents of the Accounts to the local law
                    enforcement authorities under specifically defined circumstances. In case Readsnet
                    gets merged or acquired, we will be informing on the website about such
                    developments. When you have opted for receiving related information, we will use
                    your information to show you relevant advertisements, contents, events from us and
                    our partners. Access to your name, email address and private details is restricted to
                    our employees who need to know such information in connection with our services
                    and are bound by confidentiality obligations.</p>
                <h5 style={{ marginBottom: '0.3rem' }}>IP Address</h5>
                <p>We use advanced web analytics, which provides information such as the URL of the
                    site from which you came, your location, session time etc.</p>
                <h5 style={{ marginBottom: '0.3rem' }}>Links from Our Website</h5>
                <p>We have many pages containing external links. You are advised to verify the privacy
                    practices of such externally linked websites. We are not responsible for the manner
                    of use or misuse of information made available by you at such other websites. We
                    encourage you not to provide Personal Information, without assuring yourselves of
                    the privacy policy of external linked websites.</p>
                <h5 style={{ marginBottom: '0.3rem' }}>How Secure is Your Information</h5>
                <p>We adopt appropriate data collection, storage and processing practices and security
                    measures, as well as physical security measures to protect against unauthorized
                    access, alteration, disclosure or destruction of your Personal Information, username,
                    password, transaction information and data stored in your Account. Our employees
                    also sign confidentiality agreements to use your data for product enhancements, bug
                    fixes and for services only.</p>
                <h5 style={{ marginBottom: '0.3rem' }}>Notification of Changes</h5>
                <p>When there are significant changes to the privacy policy, the same will be posted on
                    our website in order to keep you informed of any changes in the nature of
                    information collected, manner of collection, use and sharing of information.</p>
                <h5 style={{ marginBottom: '0.3rem' }}>Conflicts Between Privacy Policy & Terms</h5>
                <p>At any time, when there is a conflict between the terms of service and privacy policy,
                    the Terms shall control. If you have any questions, suggestions and comments
                    regarding this privacy policy, please contact us at twiceread@readsnet.com</p>
                </Grid.Column>
            </Grid>
        )
    }
}

export default PrivacyPolicy
