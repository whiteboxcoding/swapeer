// @flow
import React, {PureComponent} from 'react'
import { List, Image, Segment, Grid, Icon, Button, Popup, Modal } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import get from 'lodash/get'
import { TextWrapper, ItemWrapper, Text, ChatIcon, FollowingBtn } from './style';
import UserDetailsCard from '../UserDetailsCard'
import NoDataFoundScreen from '../NoDataFoundScreen'
import DataLoadingScreen from '../DataLoadingScreen'
import BookSwapRequestModal from '../BookSwapRequestModal'
import { CloseBtn } from '../../styles/common'
import { getModalMountNode } from '../../helpers/functions'

type Props = {
    getUserDonatesList: Function,
    getUserMatchesList: Function,
    updateFollowStatus: Function,
    params: Object,
    exchanges: Object,
    getUserDetails: Function,
    requestUserBookSwap: Function,
    getUserRequestedSwaps: Function,
    userDetails: Object,
    requestedSwaps: Object,
}

class Exchange extends PureComponent<Props> {
    constructor() {
        super()
        this.state = {
            bookSwapModal: {
                open: false,
                data: {}
            }
        };
        this.getDonateButtons = this.getDonateButtons.bind(this)
        this.getExchangeButtons = this.getExchangeButtons.bind(this)
        this.unfollowUser = this.unfollowUser.bind(this)
        this.followUser = this.followUser.bind(this)
        this.openBookSwapModal = this.openBookSwapModal.bind(this)
        this.closeBookSwapModal = this.closeBookSwapModal.bind(this)
        this.getComponentView = this.getComponentView.bind(this)
        this.requestUserBookSwap = this.requestUserBookSwap.bind(this)
    }
	componentDidMount() {
		const { params } = this.props;
        this.props.getUserMatchesList({
            user_id: params.userId,
            count: 0
        })
	}

  requestUserBookSwap(data) {
    const { params } = this.props;
    this.setState({
      bookSwapModal: {
          open: false,
          data: {}
      }
    })
    this.props.requestUserBookSwap(data)
    this.props.getUserMatchesList({
        user_id: params.userId,
        count: 0
    })
  }

	getDonateButtons() {
        return (
            <Button size='tiny' secondary>
                <b>Request Book</b>
            </Button>
        )
    }

    unfollowUser(userId) {
        this.props.updateFollowStatus({
            follow: false,
            following_id: userId
        }, 'exchange')
    }

    followUser(userId) {
        this.props.updateFollowStatus({
            follow: true,
            following_id: userId
        }, 'exchange')
    }

    getExchangeButtons(exchangeId) {
        return (
            <NavLink to={`?tab=messages&userId=${exchangeId}`}>
                <ChatIcon src={'/images/chatIcon.svg'} />
            </NavLink>
        )
    }

    openBookSwapModal(data) {
        this.setState({
            bookSwapModal: {
                open: true,
                data
            }
        })
        this.props.getUserRequestedSwaps({
            swap_user_id: data.id
        })
    }

    closeBookSwapModal() {
        this.setState({
            bookSwapModal: {
                open: false,
                data: {}
            }
        })
    }

    getComponentView(exchanges, userDetails, requestedSwaps, bookSwapModal) {
        if (get(exchanges, 'data', []).length === 0 && exchanges.status !== 'fetched') {
            return (
                <DataLoadingScreen text={'Checking for exchanges and donations for your wishlist...'} />
            )
        }
        if (get(exchanges, 'data', []).length === 0) {
            return (
                <NoDataFoundScreen
                    text={'Looks like no one has your wishlist yet :('}
                    buttonText={'Invite friends to join for more books!'}
                />
            )
        }
          const readers = get(exchanges, 'data', []).length
          const matchCount = get(exchanges, 'data', []).reduce((sum, obj) => (sum + obj.match_count), 0)
          return (
              <div>
                  <Text style={{textAlign: 'center'}}>You matched with {readers === 1 ? '1 reader' : `${readers} readers`} for {matchCount} new books! Add more
                      books/wishlist
                      for more :)</Text>
                  <Segment padded basic>
                      {
                          get(exchanges, 'data', []).map((exchange) =>
                              <ItemWrapper key={exchange.id} verticalAlign='middle'>
                                  <Grid.Column mobile={4} tablet={1} computer={1}>
                                      <Image circular src={exchange.profile_pic}/>
                                  </Grid.Column>
                                  <TextWrapper only={'mobile'} tablet={12}>
                                      <Popup
                                          style={{padding: 0, width: '20rem', maxWidth: '18.75rem'}}
                                          mountNode={getModalMountNode()}
                                          onOpen={() => this.props.getUserDetails({user_id: exchange.id})}
                                          content={<UserDetailsCard userData={userDetails.data}/>}
                                          trigger={<Text style={{ marginBottom: '0' }}>{exchange.name}</Text>}
                                      />
                                      <button onClick={() => this.openBookSwapModal(exchange)}
                                              style={{border: 'none', background: 'white', paddingLeft: 0, cursor: 'pointer'}}>
                                          {
                                              exchange.match_count === 1
                                                  ? <Text>1 match</Text>
                                                  : <Text>{exchange.match_count} matches</Text>
                                          }
                                      </button>
                                      <Text>{Math.round(exchange.distance / 1000)} km</Text>
                                      {this.getExchangeButtons(exchange.id)}
                                      {
                                          exchange.following
                                              ? (
                                                  <FollowingBtn style={{ verticalAlign: 'bottom', marginLeft: '1rem' }} size='tiny'
                                                                onClick={() => this.unfollowUser(exchange.id)}>
                                                      <b>Following</b>
                                                  </FollowingBtn>
                                              ) : (
                                                  <Button onClick={() => this.followUser(exchange.id)}
                                                          style={{ verticalAlign: 'bottom', marginLeft: '1rem', borderRadius: '6.25rem'}} size='tiny' basic
                                                          color='teal'>
                                                      <b>Follow</b>
                                                  </Button>
                                              )
                                      }
                                  </TextWrapper>
                                  <TextWrapper only={'tablet computer'} tablet={15} computer={15}>
                                      <Grid verticalAlign={'middle'}>
                                          <Grid.Column tablet={5} computer={5}>
                                              <Popup style={{padding: 0, width: '20rem', maxWidth: '18.75rem'}}
                                                     mountNode={getModalMountNode()}
                                                     onOpen={() => this.props.getUserDetails({user_id: exchange.id})}
                                                     content={<UserDetailsCard userData={userDetails.data}/>}
                                                     trigger={<Text>{exchange.name}</Text>}/>
                                          </Grid.Column>
                                          <Grid.Column textAlign={'center'} tablet={3} computer={3}>
                                              <button onClick={() => this.openBookSwapModal(exchange)}
                                                      style={{border: 'none', cursor: 'pointer'}}>
                                                  {
                                                      exchange.match_count === 1
                                                          ? <Text>1 match</Text>
                                                          : <Text>{exchange.match_count} matches</Text>
                                                  }
                                              </button>
                                          </Grid.Column>
                                          <Grid.Column textAlign={'center'} tablet={3} computer={3}>
                                              <Text>{Math.round(exchange.distance / 1000)} km</Text>
                                          </Grid.Column>
                                          <Grid.Column textAlign={'center'} tablet={3} computer={3}>
                                              {this.getExchangeButtons(exchange.id)}
                                          </Grid.Column>
                                          <Grid.Column textAlign={'center'} tablet={2} computer={2}>
                                              {
                                                  exchange.following
                                                      ? (
                                                          <FollowingBtn size='tiny'
                                                                        onClick={() => this.unfollowUser(exchange.id)}>
                                                              <b>Following</b>
                                                          </FollowingBtn>
                                                      ) : (
                                                          <Button onClick={() => this.followUser(exchange.id)}
                                                                  style={{borderRadius: '6.25rem'}} size='tiny' basic
                                                                  color='teal'>
                                                              <b>Follow</b>
                                                          </Button>
                                                      )
                                              }
                                          </Grid.Column>
                                      </Grid>
                                  </TextWrapper>
                              </ItemWrapper>
                          )
                      }
                      <Modal
                          onClose={this.closeBookSwapModal}
                          mountNode={getModalMountNode()}
                          open={bookSwapModal.open}
                          closeIcon={<CloseBtn><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>}
                      >
                          <Modal.Content>
                              <BookSwapRequestModal
                                  data={bookSwapModal.data}
                                  requestUserBookSwap={this.requestUserBookSwap}
                                  requestedSwaps={requestedSwaps}
                              />
                          </Modal.Content>
                      </Modal>
                  </Segment>
              </div>
          )
    }

	render () {
		const { exchanges, userDetails, requestedSwaps } = this.props;
		const { bookSwapModal } = this.state
        return this.getComponentView(exchanges, userDetails, requestedSwaps, bookSwapModal)
	}
}

export default Exchange
