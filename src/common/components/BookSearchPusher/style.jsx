import { Segment, Input, Button, Sidebar } from 'semantic-ui-react'
import styled from 'styled-components'

export const SearchBar = styled(Input)`
    input {
        color: white !important;
        border-bottom-color: white !important;
        padding: 0.625rem !important;
        font-size: 1.3rem !important;
    }
`;

export const CloseIcon = styled(Button)`
    position: absolute;
    right: 1.25rem;
    top: 0;
    z-index: 2;
`;

export const SearchDropDown = styled(Segment)`
    width: 100%;
    margin: 0 auto;
    opacity: 0.8;
    padding: 1.625rem 0.625rem;
    border-radius: 0;
    position: absolute;
    left: 0;
    max-height: 23rem;
    overflow-y: scroll;
`;

export const SearchItem = styled.button`
    border: none;
    width: 100%;
    font-size: 1rem;
    margin-bottom: 0.625rem;
    cursor: pointer;
    text-align: left;
    padding: 0.312rem;
    outline: none;
    background: white;
`;

export const StyledSidebar = styled(Sidebar)`
    overflow-y: visible !important;
`;

export const SearchDropdown = styled(Segment)`
    border: none;
    position: absolute;
    width: 100%;
    z-index: 10;
    top: 58%;
    left: 0;
    border-radius: 0;
`;

export const MobileSearchDropdown = styled(Segment)`
    border: none;
    position: absolute;
    z-index: 10;
    top: 58%;
    left: 0;
    border-radius: 0;
`;

export const DropdownBtn = styled(Button)`
    padding-top: 1rem;
    padding-bottom: 1rem;
    box-shadow: none !important;
`;
