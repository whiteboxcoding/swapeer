// @flow
import React, {PureComponent} from 'react'
import qs from 'qs';
import { Sidebar, Segment, Input, Grid, Icon, Button, Dimmer, Loader, Modal, Header } from 'semantic-ui-react'
import { CloseIcon, SearchDropDown, SearchBar, SearchItem, StyledSidebar, SearchDropdown, DropdownBtn, MobileSearchDropdown } from './style'
import {getModalMountNode} from "../../helpers/functions";
import AddBookForm from '../../components/AddBookForm'
import { getQueryParamsInURL } from '../../helpers'
import { CloseBtn } from '../../styles/common'

type Props = {
  show: Boolean,
    content: React$Node,
  hidePusher: Function,
  searchBookByName: Function,
  searchedBooks: Object,
    searchedProfiles: Object,
    openAddSearchedBookModal: Function,
    resetSearchedBookData: Function,
    bookImage: String,
    saveUploadedFile: Function,
    onAddBookSubmit: Function,
    searchProfileByName: Function,
    updateRoute: Function,
    location: Object,
    resetSearchedProfileData: Function,
    onUserProfileClick: Function,
    onPageScrollEnds: Function,
    getUserCollection: Function,
    setCollectionInputVal: Function,
    collections: Array,
    userId: String,
}

class BookSearchPusher extends PureComponent<Props> {
    constructor () {
        super();
        this.state = {
            inputVal: '',
            addBookModalOpen: false,
        }
        this.onSearchBarChange = this.onSearchBarChange.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.onFormSubmit = this.onFormSubmit.bind(this)
        this.openAddBookModal = this.openAddBookModal.bind(this)
        this.toggleSearchDropdown = this.toggleSearchDropdown.bind(this)
        this.onSearchDropdownClick = this.onSearchDropdownClick.bind(this)
        this.onDropDownScroll = this.onDropDownScroll.bind(this)
        this.onDrawerBodyScroll = this.onDrawerBodyScroll.bind(this)
    }

    componentDidMount() {
        document.getElementById('sideBarPushable').onscroll = this.onDrawerBodyScroll;
    }

    onDrawerBodyScroll() {
        const el = document.getElementById('sideBarPushable');
        const { scrollHeight } = el;
        const height = el.offsetHeight + el.scrollTop
        if (height >= scrollHeight) {
            this.props.onPageScrollEnds()
        }
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.show && this.props.show) {
            this.setState({
                inputVal: ''
            })
        }
    }

    toggleSearchDropdown(e) {
        const { location, searchedQuery } = this.props;
        const { inputVal } = this.state;
        const val = (inputVal.includes('Book:') || inputVal.includes('Profile:')) ? inputVal.split(': ')[1] || '' : inputVal
        const obj = getQueryParamsInURL(location.search)
        if (e.type === 'focus') {
            this.props.resetSearchedBookData()
            this.props.resetSearchedProfileData()
            return (
                this.setState({
                    openDropdown: true,
                    inputVal: this.getSearchFieldType(obj.searchType, val, true)
                })
            )
        } else if (e.type === 'blur') {
            return (
                this.setState({
                    openDropdown: false,
                    inputVal: this.getSearchFieldType(obj.searchType, val, false)
                })
            )
        }
    }

    getSearchFieldType (searchType = 'Book', searchedQuery, isActive) {
        const valueString = isActive ? `${searchType}: ` : ''
        return valueString + searchedQuery
    }

    onDropDownScroll(e) {
        const el = e.currentTarget;
        const height = el.offsetHeight + el.scrollTop
        const scrollHeight = el.scrollHeight
        if (height >= (scrollHeight - 10)) {
            const { searchedBooks, searchedProfiles, location } = this.props;
            const { inputVal } = this.state
            const obj = getQueryParamsInURL(location.search)
            const val = inputVal.split(': ')[1] || ''
            if (obj.searchType === 'Profile') {
              if (!searchedProfiles.fetchedAll) {
                  this.props.searchProfileByName({
                      text: val,
                      count: searchedProfiles.data.length
                  })
              }
            } else {
              if (!searchedBooks.fetchedAll) {
                  this.props.searchBookByName({
                      text: val,
                      count: searchedBooks.data.length
                  })
              }
            }
        }
    }

    onSearchBarChange(e, data) {
        if (data.value.startsWith('Profile: ') || data.value.startsWith('Book: ')) {
            this.setState({
                inputVal: data.value,
                openDropdown: false
            })
            const arr = data.value.split(': ')
            if (arr[1].length >= 3) {
              data.value = arr[1];
              data.count = 0;
              if (arr[0] === 'Profile') {
                this.props.searchProfileByName({
                    text: data.value,
                    count: 0
                })
              } else {
                this.props.searchBookByName({
                    text: data.value,
                    count: 0
                })
              }
            }
        } else {
            if (data.value.includes('Profile:')) {
                this.setState({
                    inputVal: 'Profile: ',
                    openDropdown: false
                })
            } else if (data.value.includes('Book:')) {
                this.setState({
                    inputVal: 'Book: ',
                    openDropdown: false
                })
            }
        }
    }

    openAddBookModal() {
        this.setState({
            addBookModalOpen: true
        })
    }

    onFormSubmit (data) {
        const { bookImage } = this.props;
        if (!data.book) {
            data.book = {}
        }
        data.book.images = {
            thumbnail: bookImage.url
        };
        this.handleClose()
        this.props.onAddBookSubmit(data.book, data.collection)
    }

    handleClose () {
        this.setState({
            addBookModalOpen: false
        })
    }

    onSearchDropdownClick (e) {
        e.preventDefault()
        const { location } = this.props
        const obj = getQueryParamsInURL(location.search)
        const name = e.currentTarget.getAttribute('name')
        this.setState({
            inputVal: `${name}: `,
            openDropdown: false
        })
        obj.searchType = name
        this.props.updateRoute({
            search: qs.stringify(obj)
        });
        // this.props.changeDashboard(dashboardToPathMap[name])
    }

    getDropdownBody (searchedData, searchType) {
        const { bookImage } = this.props
        switch (searchedData.status) {
          case 'fetched': {
              if (searchType === 'book') {
                  return (
                      <div>
                          {
                              (searchedData.data.length === 0)
                                  ? <div>
                                      <p>Sorry, seems like the book is not available</p>
                                      <Modal
                                          trigger={<Button secondary onClick={this.openAddBookModal}>Add this book</Button>}
                                          mountNode={getModalMountNode()}
                                          open={this.state.addBookModalOpen}
                                          onClose={this.handleClose}
                                          closeIcon={<CloseBtn><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>}
                                      >
                                          <Modal.Content>
                                              <Modal.Description>
                                                  <AddBookForm
                                                      onSubmit={this.onFormSubmit}
                                                      getUserCollection={this.props.getUserCollection}
                                                      collections={this.props.collections}
                                                      saveUploadedFile={this.props.saveUploadedFile}
                                                      setCollectionInputVal={this.props.setCollectionInputVal}
                                                      bookImage={bookImage}
                                                      userId={this.props.userId}
                                                  />
                                              </Modal.Description>
                                          </Modal.Content>
                                      </Modal>
                                  </div>
                                  : searchedData.data.map((book) => <SearchItem key={book.id}
                                                                                 onClick={() => this.props.openAddSearchedBookModal(book)}>{book.name}</SearchItem>)
                          }
                      </div>
                  )
              }
              return (
                  <div>
                      {
                          (searchedData.data.length === 0)
                              ? <div>
                                  <p>Sorry, seems like the user is not available</p>
                              </div>
                              : searchedData.data.map((profile) => <SearchItem onClick={() => this.props.onUserProfileClick(profile.id)} key={profile.id}>{profile.name}</SearchItem>)
                      }
                  </div>
              )
          }
          case 'fetching': {
              if (searchType === 'book') {
                  return (
                      <div>
                          {
                              searchedData.data.map((book) => <SearchItem key={book.id}
                                                                          onClick={() => this.props.openAddSearchedBookModal(book)}>{book.name}</SearchItem>)
                          }
                          {
                              (searchedData.data.length === 0) &&
                              <Dimmer inverted active>
                                  <Loader />
                              </Dimmer>
                          }
                      </div>
                  )
              } else {
                  return (
                      <div>
                          {
                              searchedData.data.map((profile) => <SearchItem onClick={() => this.props.onUserProfileClick(profile.id)} key={profile.id}>{profile.name}</SearchItem>)
                          }
                          {
                              (searchedData.data.length === 0) &&
                              <Dimmer inverted active>
                                  <Loader />
                              </Dimmer>
                          }
                      </div>
                  )
              }
          }
          default:
            return null
        }
    }
	render () {
		const { content, show, searchedBooks, searchedProfiles } = this.props;
		const { inputVal, openDropdown } = this.state
		return (
			<Sidebar.Pushable style={{ position: 'inherit' }} as={Segment} id={'sideBarPushable'}>
				<StyledSidebar
					as={Segment}
					animation='push'
					direction='top'
					icon='labeled'
					inverted
					onHide={this.hideSearchbar}
					vertical
					visible={show}
				>
					<Segment inverted basic padded textAlign='center'>
						<CloseIcon onClick={this.props.hidePusher} icon basic>
                            <img style={{ width: '1.3rem' }} src='/images/closeWhite.svg' />
                        </CloseIcon>
                        <Grid>
                            <Grid.Column tablet={4} computer={4} only={'tablet computer'}>
                            </Grid.Column>
                            <Grid.Column tablet={8} computer={8} mobile={16}>
                                <SearchBar
                                    fluid
                                    placeholder='Search by ISBN number, author or title'
                                    transparent
                                    onChange={this.onSearchBarChange}
                                    onFocus={this.toggleSearchDropdown}
                                    onBlur={this.toggleSearchDropdown}
                                    value={inputVal}
                                />
                                {
                                    openDropdown &&
                                    <SearchDropdown raised>
                                        <DropdownBtn attached='top' basic fluid size='medium' name='Book' onMouseDown={this.onSearchDropdownClick}>
                                            <Header as={'h5'} textAlign='left' normal='true'>Book</Header>
                                        </DropdownBtn>
                                        <DropdownBtn attached='top' basic fluid size='medium' name='Profile' onMouseDown={this.onSearchDropdownClick}>
                                            <Header as={'h5'} textAlign='left' normal='true'>Profile</Header>
                                        </DropdownBtn>
                                    </SearchDropdown>
                                }
                                {
                                    (searchedBooks.status && searchedBooks.searchQuery) &&
                                    <SearchDropDown fluid size="tiny" onScroll={this.onDropDownScroll}>
                                        {this.getDropdownBody(searchedBooks, 'book')}
                                    </SearchDropDown>
                                }
                                {
                                    (searchedProfiles.status && searchedProfiles.searchQuery) &&
                                    <SearchDropDown fluid size="tiny" onScroll={this.onDropDownScroll}>
                                        {this.getDropdownBody(searchedProfiles, 'profile')}
                                    </SearchDropDown>
                                }
                            </Grid.Column>
                            <Grid.Column tablet={4} computer={4} only={'tablet computer'}>
                            </Grid.Column>
                        </Grid>
					  </Segment>
				</StyledSidebar>
                <div>
                    {content}
                </div>
			</Sidebar.Pushable>
		)
	}
}

export default BookSearchPusher
