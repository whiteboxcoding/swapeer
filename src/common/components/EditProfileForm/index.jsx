// @flow
import React, {PureComponent} from 'react'
import {Form, Input, Grid} from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { required, validEmail, length10 } from 'common/validators'
import FormDropDown from '../FormDropDown'
import { FieldWrapper } from './style'
import countryCode from '../../countryCode';

const FormField = (
	{
	   input,
	   meta: { touched, error },
	   as,
	   ...rest
   }) => {
    const Element = as || Input
    return (
        <FieldWrapper>
            <Element className='inputField' style={{ margin: 0 }} {...input} {...rest} error={!!(touched && error)} />
            {touched && error && rest.showMessage(`${input.name.replace('_', ' ')} is required`, 'error')}
        </FieldWrapper>
    )
}

type Props = {
}

class EditProfileForm extends PureComponent<Props> {
	render () {
		return (
			<Form onSubmit={this.props.handleSubmit}>
                <p style={{ marginTop: '0.5rem' }}>Email Id</p>
                <Field showMessage={this.props.showMessagePopup} fluid component={FormField} validate={[required, validEmail]} name='email' />
                <p style={{ marginTop: '0.5rem' }}>Name</p>
                <Field showMessage={this.props.showMessagePopup} fluid component={FormField} validate={required} name='name' />
                <p style={{ marginTop: '0.5rem' }}>Contact Number</p>
                <Grid>
                    <Grid.Column width={6}>
                        <Field
                            validate={required}
                            component={FormDropDown}
                            fluid
                            selection
                            search
                            showMessage={this.props.showMessagePopup}
                            options={countryCode}
                            name="countryCode"
                        />
                    </Grid.Column>
                    <Grid.Column width={10}>
                        <Field showMessage={this.props.showMessagePopup} fluid component={FormField} validate={[required, length10]} name='contact_no' />
                    </Grid.Column>
                </Grid>
			</Form>
		)
	}
}

export default reduxForm({
	form: 'editProfile'
})(EditProfileForm)
