import styled from "styled-components";

export const FieldWrapper = styled.div`
    position: relative;
    margin-bottom: 2rem;
`;
