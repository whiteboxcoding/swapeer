// @flow
import React, {PureComponent} from 'react'
import { Form, Header, Input, Divider, Grid, Segment, Button } from 'semantic-ui-react'
import FormField from 'components/FormField'
import FormDropDown from 'components/FormDropDown'
import { Field, reduxForm } from 'redux-form'
import uniqBy from 'lodash/uniqBy'
import isEmpty from 'lodash/isEmpty'
import { genresList } from '../../constants'
import {
    FormTitle,
    FormInput,
    StyledFormDropDown
} from '../ExtraLoginInfoForm/style'

type Props = {
	viewType: String,
	handleSubmit: Function,
    searchBookByName: Function,
    searchedBooks: Array,
    favoriteBooks: Array,
    goToPrevStep: Function,
    showMessagePopup: Function,
    skipStep: Function,
    searchQuery: String,
}

class ReadingHabitForm extends PureComponent<Props> {
	constructor() {
		super()
		this.state = {
			value: ''
		};
		this.timer = null
        this.selectedBooks = []
		this.onBookSearch = this.onBookSearch.bind(this)
		this.onBackBtnClick = this.onBackBtnClick.bind(this)
		this.onDropdownChange = this.onDropdownChange.bind(this)
	}

    onBookSearch(e, data) {
		if (this.timer) {
			clearTimeout(this.timer)
		}
		this.setState({
			value: data.searchQuery
		});
		const that = this;
        this.timer = setTimeout(() => {
            const { favoriteBooks, searchedBooks } = that.props
            if (favoriteBooks.length < 3 && data.searchQuery) {
                that.props.searchBookByName({ text: data.searchQuery, count: 0 }, this.selectedBooks)
            }
		}, 500);
    }

    onBackBtnClick(e) {
		e.stopPropagation();
		e.preventDefault();
        this.props.goToPrevStep()
	}

    onDropdownChange(e) {
		const { searchedBooks } = this.props
        const {preventDefault, ...rest} = e
		const bookArr = Object.keys(rest)
		const books = bookArr.map((ind) => rest[ind])
		const selectedBooks = searchedBooks.filter((sb) => books.includes(sb.name))
		this.selectedBooks = selectedBooks
		this.setState({
            value: ''
		});
		if (bookArr.length > 3) {
			preventDefault()
			this.props.showMessagePopup('You can not select more than 3 Books', 'error')
		}
	}

	render () {
        const { searchQuery, searchedBooks } = this.props
		// const allBooks = !isEmpty(this.selectedBook) ? uniqBy(searchedBooks.concat(this.selectedBook), 'id') : searchedBooks
		const options = searchedBooks.map((book) => ({
            key: book.id,
            text: book.name,
            value: book.name
        }));
        return (
			<Form onSubmit={this.props.handleSubmit}>
				<FormTitle>Define your reading habit</FormTitle>
				<br />
				<Grid>
					<Grid.Column tablet={8} computer={8} mobile={16}>
						<Segment basic>
							<p style={{ marginTop: '1.5rem' }}>Your top 3 recommended all-time reads:</p>
							<Field
								placeholder='What is it that you would want everyone to read?'
								component={StyledFormDropDown}
								name='bookmark_use'
                                multiple
                                search
                                minCharacters={3}
                                selection
                                onSearchChange={this.onBookSearch}
                                onChange={this.onDropdownChange}
                                searchInput={{ value: this.state.value }}
                                options={options}
							/>
							<p style={{ marginTop: '1.5rem' }}>Select your favorite genres:</p>
							<Field
								placeholder='Your all time favs'
								component={StyledFormDropDown}
								name='favourite_genres'
                                multiple
                                search
                                selection
                                options={genresList}
							/>
							<p style={{ marginTop: '1.5rem' }}>Describe your reading self in 160 characters:</p>
							<Field placeholder='Describe the reader in you' component={FormInput} name='book_read_story' />
                            <p style={{ marginTop: '1.5rem' }}>Add your personal blog link, if any:</p>
                            <Field placeholder='Show everyone your craftsmanship' component={FormInput} name='blog_link' />
						</Segment>
					</Grid.Column>
					<Grid.Column width={8}></Grid.Column>
					<Grid.Column width={16} textAlign='right'>
                        <Button style={{ borderRadius: '6.25rem' }} primary onClick={this.onBackBtnClick}>Back</Button>
						<Button style={{ borderRadius: '6.25rem' }} primary onClick={this.props.skipStep}>Skip</Button>
						<Button style={{ borderRadius: '6.25rem' }} primary type='submit'>Next</Button>
					</Grid.Column>
				</Grid>
			</Form>
		)
	}
}

export default reduxForm({
	form: 'readingHabit'
})(ReadingHabitForm)
