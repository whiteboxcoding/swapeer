// @flow
import React, {PureComponent} from 'react'
import { Form, Button, Segment } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import get from 'lodash/get'
import { required } from '../../validators'
import FormField from '../FormField'
import FormDropDown from '../FormDropDown'

type Props = {
  handleSubmit: Function,
    getUserBooksList: Function,
    getUsersList: Function,
    userBooksList: Object,
    searchedUser: Array,
};

class SwapRequestForm extends PureComponent<Props> {
    constructor() {
        super()
        this.selectedUserId = ''
        this.onSwapBookSearch = this.onSwapBookSearch.bind(this)
        this.onUserSearch = this.onUserSearch.bind(this)
        this.onBookSearch = this.onBookSearch.bind(this)
        this.saveSelectedUserId = this.saveSelectedUserId.bind(this)
    }
    onSwapBookSearch(e, data) {
        this.props.getUserBooksList({
            user_id: this.props.userId,
            count: 0,
            type: 'swap',
            text: data.searchQuery
        })
    }

    onUserSearch(e, data) {
        this.props.getUsersList({
            count: 0,
            text: data.searchQuery
        })
    }

    onBookSearch(e, data) {
        if (this.selectedUserId) {
            this.props.getUserBooksList({
                user_id: this.selectedUserId,
                count: 0,
                type: 'swap',
                text: data.searchQuery
            })
        }
    }

    saveSelectedUserId(e, data) {
        this.selectedUserId = data
    }

	render () {
        const { userBooksList, searchedUser, userId } = this.props
		return (
			<Form onSubmit={this.props.handleSubmit}>
				<p style={{ fontSize: '1.5rem' }}>Reader's name:</p>
                <Field
                    className="formInput"
                    size='large'
                    component={FormDropDown}
                    validate={required}
                    name='user_to.id'
                    search
                    fluid
                    selection
                    onChange={this.saveSelectedUserId}
                    onSearchChange={this.onUserSearch}
                    options={searchedUser.map(user => ({
                        key: user.name,
                        text: user.name,
                        value: user.id
                    }))}
                />
				<p style={{ fontSize: '1.5rem' }}>Name of the book:</p>
                <Field
                    className="formInput"
                    size='large'
                    component={FormDropDown}
                    validate={required}
                    name='user_to.book.id'
                    search
                    fluid
                    selection
                    onSearchChange={this.onBookSearch}
                    noResultsMessage={
                        userBooksList.status === 'fetched'
                            ?
                                <Segment textAlign={'center'} basic>
                                    <p>This book is not added in reader's collection</p>
                                </Segment>
                            : <div></div>
                    }
                    options={get(userBooksList, `data.${this.selectedUserId}`, []).map(book => ({
                        key: get(book, 'book.id'),
                        text: get(book, 'book.name'),
                        value: get(book, 'book.id')
                    }))}
                />
                <p style={{ fontSize: '1.5rem' }}>Which book have you given?</p>
                <Field
                    className="formInput"
                    component={FormDropDown}
                    validate={required}
                    name='user_from.book.id'
                    size='large'
                    search
                    fluid
                    selection
                    onSearchChange={this.onSwapBookSearch}
                    options={get(userBooksList, `data.${userId}`, []).map(book => ({
                        key: get(book, 'book.id'),
                        text: get(book, 'book.name'),
                        value: get(book, 'book.id')
                    }))}
                />
                <br />
				<Button size='large' secondary style={{ float: 'right' }}>
                    <b style={{ color: 'black' }}>Submit</b>
				</Button>
                <br />
			</Form>
		)
	}
}

export default reduxForm({
	form: 'swapRequestForm'
})(SwapRequestForm)
