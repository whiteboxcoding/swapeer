import { Menu } from 'semantic-ui-react'
import styled from 'styled-components'

export const StyledMenu = styled(Menu)`
    border: none;
    box-shadow: none;
    align-items: center;
    justify-content: center;
`;

export const StyledMenuItem = styled(Menu.Item)`
    padding-left: 5rem;
    padding-right: 5rem;
`;
