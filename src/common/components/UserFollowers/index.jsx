// @flow
import React, {PureComponent} from 'react'
import { Menu, Grid, Header, Divider, Loader, Segment, Dimmer } from 'semantic-ui-react'
import UserDetailsCard from '../UserDetailsCard'
import { StyledMenu, StyledMenuItem } from './style'

type Props = {
    fetchFollowersUsers: Function,
    fetchFollowingUsers: Function,
    followers: Object,
    followings: Object,
    userId: String,
    updateFollowStatus: Function
}

class UserFollowers extends PureComponent<Props> {
    constructor() {
        super();
        this.state = {
            activeMenu: 'Following'
        };
        this.onMenuItemClick = this.onMenuItemClick.bind(this)
    }

    componentDidMount() {
        const { userId } = this.props;
        this.props.fetchFollowingUsers({ count: 0, user_id: userId })
    }

    onMenuItemClick(e, data) {
        const { userId } = this.props;
        if (data.name === 'Followers') {
            this.props.fetchFollowersUsers({ count: 0, user_id: userId })
        } else {
            this.props.fetchFollowingUsers({ count: 0, user_id: userId })
        }
        this.setState({
            activeMenu: data.name
        })
    }

    render () {
        const { activeMenu } = this.state;
        const { followers, followings } = this.props
        return (
            <div>
                <StyledMenu pointing size={'massive'} secondary>
                    <StyledMenuItem
                        key={'0'}
                        name={'Following'}
                        active={activeMenu === 'Following'}
                        onClick={this.onMenuItemClick}
                    />
                    <StyledMenuItem
                        key={'1'}
                        onClick={this.onMenuItemClick}
                        active={activeMenu === 'Followers'}
                        name={'Followers'}
                    />
                </StyledMenu>
                <Divider section hidden />
                {
                    (activeMenu === 'Following')
                    ? (
                        <div>
                          {
                            (followings.status === 'fetched')
                            ? (
                              <Grid container>
                                  {
                                      followings.data.map((following) =>
                                          <Grid.Column mobile={16} tablet={5} computer={5} key={following.id}>
                                              <UserDetailsCard
                                                  userData={following}
                                                  unFollowUser={this.props.updateFollowStatus}
                                              />
                                          </Grid.Column>
                                      )
                                  }
                                  {
                                      (followings.data.length === 0) &&
                                      <Grid.Column width={16} textAlign={'center'}>
                                          <Header as="h3" >You are not following anyone</Header>
                                      </Grid.Column>
                                  }
                              </Grid>
                            ) : <Segment basic padded='very'><Dimmer active inverted><Loader inverted>Loading</Loader></Dimmer></Segment>
                          }
                          </div>
                        )
                    : (
                        <div>
                          {
                            (followers.status === 'fetched')
                            ? (
                              <Grid container>
                                  {
                                      followers.data.map((follower) =>
                                          <Grid.Column mobile={16} tablet={5} computer={5} key={follower.id}>
                                              <UserDetailsCard userData={follower} />
                                          </Grid.Column>
                                      )
                                  }
                                  {
                                      (followers.data.length === 0) &&
                                      <Grid.Column width={16} textAlign={'center'}>
                                          <Header as="h3" >No user following you</Header>
                                      </Grid.Column>
                                  }
                              </Grid>
                            ) : <Segment basic padded='very'><Dimmer active inverted><Loader inverted>Loading</Loader></Dimmer></Segment>
                          }
                        </div>
                      )
                }
            </div>
        )
    }
}

export default UserFollowers
