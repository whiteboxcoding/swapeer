// @flow
import React, {PureComponent} from 'react'
import { Modal, Header, Grid, Button, Dropdown } from 'semantic-ui-react'

type Props = {
    updateNewUserData: Function,
};

class DemoCarousel extends PureComponent<Props> {
    render () {
        return (
            <Modal open size={'tiny'} dimmer={'inverted'} onClose={this.props.updateNewUserData}>
                <Modal.Content>
                    <Header textAlign={'center'} as={'h2'}>Create library of books you own</Header>
                    <br />
                    <Grid columns={2} verticalAlign={'middle'}>
                        <Grid.Column width={4}>
                            <Button circular color='twitter' icon={{ name: 'plus', size: 'big' }} />
                        </Grid.Column>
                        <Grid.Column width={12}>
                            <p style={{ fontSize: '1.5rem' }}>Add books as per category</p>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <Button circular color='twitter' icon={{ name: 'plus', size: 'big' }} />
                        </Grid.Column>
                        <Grid.Column width={12}>
                            <p style={{ fontSize: '1.5rem' }}>Click here to add books</p>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <Dropdown style={{ opacity: 1 }} text='Exchange' disabled>
                            </Dropdown>
                        </Grid.Column>
                        <Grid.Column width={12}>
                            <p style={{ fontSize: '1.5rem' }}>Select if the book is for exchange, donation or both</p>
                        </Grid.Column>
                    </Grid>
                    <Button onClick={this.props.updateNewUserData} secondary style={{ margin: '2rem', float: 'right', borderRadius: '2rem', color: 'black', fontWeight: 'bold' }}>OK</Button>
                </Modal.Content>
            </Modal>
        );
    }
}

export default DemoCarousel
