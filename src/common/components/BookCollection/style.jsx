import styled from 'styled-components'
import { Button, Segment, Modal, Header } from 'semantic-ui-react'

export const PrimaryModal = styled(Modal)`
    margin-top: 0;
`;
