// @flow
import React, {PureComponent} from 'react'
import { Button, Segment, Modal, Header } from 'semantic-ui-react'
import BookEntryItem from '../BookEntryItem'
import AddBookForm from '../AddBookForm'
import isEmpty from 'lodash/isEmpty'
import get from 'lodash/get'
import { getModalMountNode } from '../../helpers/functions';

type Props = {
	getCollectionBooks: Function,
    saveUploadedFile: Function,
	collection: String,
    addBookToWishlist: Function,
    books: Object,
    bookImage: Object,
    wishlist: Object,
    removeFromWishlist: Function,
    updateBookStatus: Function,
    openSearchBar: Function,
    removeBookFromCollection: Function,
}

class BookCollection extends PureComponent<Props> {
	constructor() {
		super();
		this.addBookToWishlist = this.addBookToWishlist.bind(this)
	}
	componentDidMount () {
        const { params, userData } = this.props
        const { userId } = params
		if (userData.id !== userId) {
            this.props.getUserWishlist({ user_id: userData.id, count: 0 })
		}
		this.props.getCollectionBooks({ user_id: userId, category: this.props.collection, count: 0 })
	}

    addBookToWishlist(book) {
        const { userData, collection } = this.props
        book.user_id = userData.id
        book.category = collection
		this.props.addBookToWishlist(book)
	}

	// onFormSubmit (data) {
	// 	const { bookImage, collection } = this.props;
     //    if (!data.book) {
     //        data.book = {}
	// 	}
     //    data.book.image = bookImage.url;
     //    this.handleClose()
	// 	this.props.onAddBookSubmit({
	// 		...data,
	// 		category: collection,
     //        match_type: 'keep'
	// 	})
	// }

    // handleOpen() {
		// this.setState({
    //         addBookModalOpen: true
		// })
    // }
    //
    // handleClose() {
		// this.props.removeUploadImage()
    //     this.setState({
    //         addBookModalOpen: false
    //     })
    // }

	render () {
		const { collection, books = {}, bookImage, userData, params, wishlist } = this.props
        const { userId } = params
		return (
			<Segment padded basic>
				<Button primary>{collection}</Button>
				{
                    get(books, `${collection}.data`, []).map((book) =>
                        <BookEntryItem
							key={book.id}
							book={book}
                            addBookToWishlist={this.addBookToWishlist}
							showWishList={userData.id !== userId}
                            userId={userData.id}
                            collection={collection}
                            removeFromWishlist={this.props.removeFromWishlist}
                            wishlist={get(wishlist, `data.${userData.id}`, [])}
                            updateBookStatus={this.props.updateBookStatus}
                            removeBookFromCollection={this.props.removeBookFromCollection}
						/>
					)
				}
				{
					(userData.id === userId) &&
						<Segment basic padded={'very'} textAlign={'center'}>
                    		<Button onClick={this.props.openSearchBar} circular color='twitter' icon={{ name: 'plus', size: 'big' }} />
						</Segment>
				}
			</Segment>
		)
	}
}

export default BookCollection
