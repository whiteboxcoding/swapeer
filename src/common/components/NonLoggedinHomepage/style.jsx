import styled from 'styled-components'
import { Grid, Header, Button, Segment } from 'semantic-ui-react'

export const PageWrapper = styled.div`
    padding: 1.25rem 3.125rem 0 3.125rem;
`;

export const TopWrapper = styled(Grid.Row)`
    padding-left: 9.375rem;
`;

export const BottomWrapper = styled(Grid.Row)`
    padding-left: 5.31rem;
`;

export const MiddleWrapper = styled(Grid)`
    padding: 0 5rem;
`;

export const LeftCol = styled(Grid.Column)`
    padding-right: 9.375rem;
`;

export const SwapText = styled.p`
    margin: 3.125rem 0;
    text-align: center;
    font-size: 1.5rem;
`;

export const Img = styled.img`
    margin: 1.25rem auto 0;
    display: block;
`;

export const BottomGrid = styled(Grid.Column)`
    padding: 0px 4rem;
`;

export const Btn = styled(Button)`
    background: #FB4B4E;
    border-radius: 6.25rem;
    font-size: 1.5rem;
`;

export const HowItWorkBtn = styled(Button)`
    background: #2CBAE5;
    border-radius: 6.25rem;
    font-size: 1.5rem;
`;

export const Footer = styled(Segment)`
    margin-top: 1.875rem;
    padding-top: 1.875rem;
    background-color: #251086;
`;

export const FooterText = styled.p`
    margin: 1.25rem;
    color: white;
    font-size: 2rem;
`;

export const Heading = styled(Header)`
    font-size: 2.2rem;
`;

export const Text = styled.p`
    font-size: 1.5rem;
`;
