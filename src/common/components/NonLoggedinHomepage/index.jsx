// @flow
import React, {PureComponent} from 'react'
import { Grid, Header, Button, Segment } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import get from 'lodash/get'
import {
    PageWrapper,
    LeftCol,
    SwapText,
    Img,
    BottomGrid,
    Btn,
    HowItWorkBtn,
    Footer,
    FooterText,
    Heading,
    Text,
    TopWrapper,
    MiddleWrapper,
    BottomWrapper
} from './style.jsx'


type Props = {
    readersStats: Object,
}

class NonLoggedinHomepage extends PureComponent<Props> {
    constructor() {
        super();
        this.state = {
            showFullView: false,
            exchange: false,
        };
        this.showFullView = this.showFullView.bind(this)
        this.showExchangeView = this.showExchangeView.bind(this)
        this.showDonateView = this.showDonateView.bind(this)
    }

    componentDidMount() {
        this.props.getReeadersStats()
    }

    showFullView() {
        this.setState({
            showFullView: true
        })
    }

    showExchangeView() {
        this.setState({
            exchange: true
        })
    }

    showDonateView() {
        this.setState({
            exchange: false
        })
    }

    render () {
        const { showFullView, exchange } = this.state;
        const { readersStats } = this.props;
        return (
            <div>
                <PageWrapper>
                    <Grid>
                        <TopWrapper only={'tablet computer'}>
                            <Grid.Column width={7}>
                                <Heading as={'h2'} style={{ paddingRight: '9.375rem' }}>
                                    Connect with like-minded readers by sharing books for free!
                                </Heading>
                                <Text style={{ paddingRight: '9.375rem' }}>Join our community of readers to mutually share libraries with anyone, anywhere in the world</Text>
                                <Btn onClick={this.props.goToLoginView} content={<b>Fullfill my wishlist now !</b>} />
                            </Grid.Column>
                            <Grid.Column width={8} textAlign={'right'}>
                                <Img width='88%' src={'/images/landing_page_v5.gif'} />
                            </Grid.Column>
                        </TopWrapper>
                        <Grid.Row only={'mobile'}>
                            <Grid.Column width={16} textAlign={'center'}>
                                <Img width='88%' src={'/images/landing_page_v5.gif'} />
                            </Grid.Column>
                            <Grid.Column width={16} textAlign={'center'}>
                                <Heading as={'h2'}>
                                    Connect with like-minded readers by sharing books for free!
                                </Heading>
                                <Text>Join our community of readers to mutually share libraries with anyone, anywhere in the world</Text>
                                <Btn onClick={this.props.goToLoginView} content={<b>Fullfill my wishlist now !</b>} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <Header as={'h1'} textAlign={'center'}>
                        <span style={{ color: '#FB4B4E' }}>{get(readersStats, 'readers', 0)}</span> readers with <span style={{ color: '#FB4B4E' }}>{get(readersStats, 'books', 0)}</span> books joined this month to
                    </Header>
                    <br />
                    <br />
                    <MiddleWrapper columns={3} textAlign={'center'}>
                        <Grid.Column tablet={5} computer={5} mobile={12}>
                            <Img width='40%' src={'/images/discover.png'} />
                            <Header as={'h2'}>Discover</Header>
                            <Text>Books nearby and beyond</Text>
                        </Grid.Column>
                        <Grid.Column tablet={6} computer={6} mobile={12}>
                            <Img width='40%' src={'/images/exchange.png'} />
                            <Header as={'h2'}>Exchange</Header>
                            <Text>books, cultures and experiences</Text>
                        </Grid.Column>
                        <Grid.Column tablet={5} computer={5} mobile={12}>
                            <Img width='40%' src={'/images/connect.png'} />
                            <Header as={'h2'}>Connect</Header>
                            <Text>with like-minded readers</Text>
                        </Grid.Column>
                        {
                            !showFullView &&
                            <Grid.Column width={16}>
                                <Segment basic padded>
                                    <HowItWorkBtn onClick={this.showFullView} content={<b>How it works ?</b>} />
                                </Segment>
                            </Grid.Column>
                        }
                    </MiddleWrapper>
                    {
                        showFullView &&
                            <div>
                                <br />
                                <br />
                                <Img style={{ width: '40%' }} src={'/images/exchangeDonate.jpg'} />
                                <SwapText>
                                    You can &nbsp;
                                    <Button.Group>
                                        <Button basic={!exchange} color={'twitter'} onClick={this.showExchangeView}>
                                            <b style={{ color: 'black' }}>Exchange</b>
                                        </Button>
                                        <Button basic={exchange} color={'twitter'} onClick={this.showDonateView}>
                                            <b style={{ color: 'black' }}>Donate</b>
                                        </Button>
                                    </Button.Group>&nbsp;
                                     with 3 easy steps
                                </SwapText>
                                {
                                    exchange
                                    ? (
                                        <div>
                                            <MiddleWrapper columns={3} textAlign={'center'}>
                                                <Grid.Column tablet={5} computer={5} mobile={12}>
                                                    <Img width='40%' src={'/images/give.png'} />
                                                    <Header as={'h2'}>Give</Header>
                                                    <Text>Add list of books to giveaway</Text>
                                                </Grid.Column>
                                                <Grid.Column tablet={6} computer={5} mobile={12}>
                                                    <Img width='27.33%' src={'/images/wishlist_v2.png'} />
                                                    <Header as={'h2'}>Wishlist</Header>
                                                    <Text>Add wishlist of books to read</Text>
                                                </Grid.Column>
                                                <Grid.Column tablet={5} computer={5} mobile={12}>
                                                    <Img width='50%' src={'/images/ExchangeConnect.png'} />
                                                    <Header as={'h2'}>Connect</Header>
                                                    <Text>Match and chat to exchange</Text>
                                                </Grid.Column>
                                            </MiddleWrapper>
                                            <br />
                                            <br />
                                            <br />
                                            <Grid>
                                                <BottomWrapper only={'tablet computer'} columns={2} verticalAlign={'middle'}>
                                                    <BottomGrid>
                                                        <Header as={'h1'}>Update your library with wishlist and save money</Header>
                                                        <Text>Have a dynamic library by participating in the
                                                            open library of sharing books!</Text>
                                                    </BottomGrid>
                                                    <BottomGrid>
                                                        <Img width='84%' src={'/images/updateLibrary.png'} />
                                                    </BottomGrid>
                                                    <BottomGrid>
                                                        <Img width='84%' src={'/images/getBackBook.png'} />
                                                    </BottomGrid>
                                                    <BottomGrid>
                                                        <Header as={'h1'}>Want your books back? We know who has it</Header>
                                                        <Text>All your exchanges and donations are tracked by
                                                            us. Simply with a click connect and chat to
                                                            request back your book!</Text>
                                                    </BottomGrid>
                                                </BottomWrapper>
                                                <Grid.Row only={'mobile'} columns={1} verticalAlign={'middle'}>
                                                    <BottomGrid>
                                                        <Img width='84%' src={'/images/updateLibrary.png'} />
                                                    </BottomGrid>
                                                    <BottomGrid style={{ marginTop: '2rem' }} textAlign={'center'}>
                                                        <Header as={'h1'}>Update your library with wishlist and save money</Header>
                                                        <Text>Have a dynamic library by participating in the
                                                            open library of sharing books!</Text>
                                                    </BottomGrid>
                                                    <BottomGrid>
                                                        <Img width='84%' src={'/images/getBackBook.png'} />
                                                    </BottomGrid>
                                                    <BottomGrid style={{ marginTop: '2rem' }} textAlign={'center'}>
                                                        <Header as={'h1'}>Want your books back? We know who has it</Header>
                                                        <Text>All your exchanges and donations are tracked by
                                                            us. Simply with a click connect and chat to
                                                            request back your book!</Text>
                                                    </BottomGrid>
                                                </Grid.Row>
                                            </Grid>
                                        </div>
                                    ) : (
                                        <div>
                                            <MiddleWrapper columns={3} textAlign={'center'}>
                                                <Grid.Column tablet={5} computer={5} mobile={12}>
                                                    <Img width='40%' src={'/images/give.png'} />
                                                    <Header as={'h2'}>Give</Header>
                                                    <Text>Add list of books to <br />donate</Text>
                                                </Grid.Column>
                                                <Grid.Column tablet={5} computer={5} mobile={12}>
                                                    <Img width='49%' src={'/images/donateConnect.jpg'} />
                                                    <Header as={'h2'}>Connect</Header>
                                                    <Text>Get connected to readers who want your book</Text>
                                                </Grid.Column>
                                                <Grid.Column tablet={5} computer={5} mobile={12}>
                                                    <Img width='33.33%' src={'/images/send2.png'} />
                                                    <Header as={'h2'}>Send</Header>
                                                    <Text>Meet offline or connect with delivery services</Text>
                                                </Grid.Column>
                                            </MiddleWrapper>
                                            <br />
                                            <br />
                                            <br />
                                            <Grid>
                                                <BottomWrapper only={'tablet computer'} columns={2} verticalAlign={'middle'}>
                                                    <BottomGrid>
                                                        <Header as={'h1'}>Donate to readers who value your books instead of throwing away.</Header>
                                                        <Text>Don't want to swap? - simply do a giveaway to
                                                            declutter your shelves! Readers, grab those
                                                            freebies now before anyone else!</Text>
                                                    </BottomGrid>
                                                    <BottomGrid>
                                                        <Img width='84%' src={'/images/kids.png'} />
                                                    </BottomGrid>
                                                    <BottomGrid>
                                                        <Img width='84%' src={'/images/getBackBook.png'} />
                                                    </BottomGrid>
                                                    <BottomGrid>
                                                        <Header as={'h1'}>Want your books back? We know who has it.</Header>
                                                        <Text>All your swaps and giveaways are tracked by us.
                                                            Simply with a click connect and chat to request
                                                            back your book!</Text>
                                                    </BottomGrid>
                                                </BottomWrapper>
                                                <Grid.Row only={'mobile'} columns={1} verticalAlign={'middle'}>
                                                    <BottomGrid>
                                                        <Img width='84%' src={'/images/kids.png'} />
                                                    </BottomGrid>
                                                    <BottomGrid style={{ marginTop: '2rem' }} textAlign={'center'}>
                                                        <Header as={'h1'}>Donate to readers who value your books instead of throwing away.</Header>
                                                        <Text>Don't want to swap? - simply do a giveaway to
                                                            declutter your shelves! Readers, grab those
                                                            freebies now before anyone else!</Text>
                                                    </BottomGrid>
                                                    <BottomGrid>
                                                        <Img width='84%' src={'/images/getBackBook.png'} />
                                                    </BottomGrid>
                                                    <BottomGrid style={{ marginTop: '2rem' }} textAlign={'center'}>
                                                        <Header as={'h1'}>Want your books back? We know who has it.</Header>
                                                        <Text>All your exchanges and donations are tracked by
                                                            us. Simply with a click connect and chat to
                                                            request back your book!</Text>
                                                    </BottomGrid>
                                                </Grid.Row>
                                            </Grid>
                                        </div>
                                        )
                                }
                            </div>
                    }
                </PageWrapper>
                {
                    showFullView &&
                    <div>
                        <Footer textAlign={'center'}>
                            <FooterText>Join the worldwide open library to share books!</FooterText>
                            <br />
                            <Btn onClick={this.props.goToLoginView} content={<b>Fullfill my wishlist now!</b>} />
                            <br />
                            <div style={{ textAlign: 'left', marginTop: '5rem', marginBottom: '1rem' }}>
                                <div style={{ display: 'inline-block' }}>
                                    <a style={{ color: 'white', padding: '0 1rem' }} href={'https://medium.com/readtreats-by-readsnet'} target='_blank'>About us</a>
                                    <a style={{ color: 'white', padding: '0 1rem' }} href={'/authors-corner'}>Authors Corner</a>
                                    <a style={{ color: 'white', padding: '0 1rem' }} href={'/privacy/policy'}>Terms and Policy</a>
                                    <a style={{ color: 'white', padding: '0 1rem' }} href={'/contact-us'}>Contact Us</a>
                                    <a style={{ color: 'white', padding: '0 1rem' }} href={'/feature/request'}>Feature Request</a>
                                </div>
                                <div style={{ display: 'inline-block', float: 'right' }}>
                                    <Grid>
                                        <Grid.Column only={'tablet computer'} width={16}>
                                            <a href={'https://www.facebook.com/readsnet/'} target='_blank'>
                                                <img style={{ width: '2rem', marginRight: '2rem' }} src={'/images/facebookWhite.svg'} />
                                            </a>
                                            <a href={'https://instagram.com/readsnet.app'} target='_blank'>
                                                <img style={{ width: '2rem', marginRight: '2rem' }} src={'/images/instagramWhite.svg'} />
                                            </a>
                                            <a href={'https://www.linkedin.com/company/readsnet/'} target='_blank'>
                                                <img style={{ width: '2rem' }} src={'/images/linkedinWhite.svg'} />
                                            </a>
                                        </Grid.Column>
                                    </Grid>
                                </div>
                            </div>
                        </Footer>
                    </div>
                }
            </div>
        )
    }
}

export default NonLoggedinHomepage
