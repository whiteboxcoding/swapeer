// @flow
import React from 'react'
import { Input } from 'semantic-ui-react'
import { ErrorText, FieldWrapper } from './style'

const FormField = ({
	input,
	meta: { touched, error },
	as,
	...rest
}: OptionsFieldProps) => {
	const Element = as || Input
	return (
		<FieldWrapper>
			<Element className='inputField' style={{ margin: 0 }} {...input} {...rest} error={!!(touched && error)} />
			{touched && error && <ErrorText><small>{error}</small></ErrorText>}
		</FieldWrapper>
	)
}
export default FormField
