import styled from 'styled-components'
import {brandError} from '../../styles/variables'

export const ErrorText = styled.p`
    color: ${brandError};
    position: absolute;
    bottom: -1.25rem;
`;

export const FieldWrapper = styled.div`
    position: relative;
    margin-bottom: 2rem;
`;
