// @flow
import React, {PureComponent} from 'react'
import { Grid, Header, Image } from 'semantic-ui-react'

type Props = {
    text: String,
}

class DataLoadingScreen extends PureComponent<Props> {
    render () {
        return (
            <Grid columns={1} verticalAlign={'middle'} style={{ height: '60vh' }}>
                <Grid.Column textAlign={'center'}>
                    <Image style={{ width: '40rem' }}centered src={'/images/loadingscreen.gif'} />
                </Grid.Column>
                <Grid.Column>
                    <Header as={'h3'} textAlign={'center'}>{this.props.text}</Header>
                </Grid.Column>
            </Grid>
        )
    }
}

export default DataLoadingScreen
