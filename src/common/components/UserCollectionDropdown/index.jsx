// @flow
import React, {PureComponent} from 'react'
import {Form, Input} from "semantic-ui-react";
import { Field } from 'redux-form'
import FormField from "../FormField";
import { required } from 'common/validators'
import { Wrapper, AutocompleteItems } from './style'

type Props = {
    getUserCollection: Function,
    setCollectionInputVal: Function,
    collections: Array,
    userId: String,
    form: String,
}

class UserCollectionDropdown extends PureComponent<Props> {
    constructor() {
        super();
        this.state = {
            filteredData: []
        };
        this.onInputFieldChange = this.onInputFieldChange.bind(this)
        this.filterResultClick = this.filterResultClick.bind(this)
    }

    componentDidMount() {
        const { userId } = this.props;
        this.props.getUserCollection({ user_id: userId, count: 0 })
    }

    filterResultClick(category) {
        this.setState({
            filteredData: []
        });
        this.props.setCollectionInputVal(category, this.props.form)
    }

    onInputFieldChange(e, data) {
        const { collections } = this.props;
        if (data.length === 0) {
            this.setState({
                filteredData: []
            })
        } else if (collections.length > 0) {
            const filteredData = collections.filter((collection) => (collection.category.toLowerCase().startsWith(data.toLowerCase())))
            this.setState({
                filteredData
            })
        }
    }

    render() {
        const { filteredData } = this.state
        return (
            <Wrapper>
                <Field
                    component={FormField}
                    validate={required}
                    as={Form.Field}
                    control={Input}
                    onChange={this.onInputFieldChange}
                    label='Collection Name'
                    name='collection'
                />
                <AutocompleteItems>
                    {
                        filteredData.map((obj) =>
                            <div key={obj.id} onClick={() => this.filterResultClick(obj.category)}>{obj.category}</div>
                        )
                    }
                </AutocompleteItems>
            </Wrapper>
        )
    }
}

export default UserCollectionDropdown
