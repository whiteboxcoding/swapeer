import styled from "styled-components";

export const Wrapper = styled.div`
    position: relative;
`;

export const AutocompleteItems = styled.div`
    position: absolute;
    border: 1px solid #d4d4d4;
    border-bottom: none;
    border-top: none;
    z-index: 99;
    top: 100%;
    right: 0;
    width: 97%;
    left: 0.8125rem;
    div {
        padding: 0.625rem;
        cursor: pointer;
        background-color: #fff;
        border-bottom: 1px solid #d4d4d4;
        &&:hover {
            background-color: #e9e9e9;
        }
    }
    div:hover {
        background-color: #e9e9e9 !important;
    }

`
