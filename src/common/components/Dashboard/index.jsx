// @flow
import React, {PureComponent} from 'react'
import { Button, Image as StyledImage, Menu, Card, Grid, Header, Modal, Divider } from 'semantic-ui-react'
import {NavLink} from 'react-router-dom'
import get from 'lodash/get'
import AddCollectionForm from '../AddCollectionForm'
import { data, otherUserData } from './data'
import StarRating from '../StarRating'
import BookTracking from '../BookTracking';
import UserFollowers from '../UserFollowers';
import UserProfileDetails from '../UserProfileDetails';
import EditProfileForm from '../EditProfileForm';
import AddBookForm from '../AddBookForm'
import AddCategoryDemo from '../AddCategoryDemo'
import {
    ImageMenu,
    NoDataFound,
    AddCollection,
    TopBanner,
    BookDescription,
    StyledMenu,
    BookTitle,
    WishlistBtn,
    EditProfileView,
    ChangePhotoBtn,
    EditCoverBtn,
    ChangeCoverText,
    EditModeBtn,
    AddWishListBtn
} from './style';
import { CloseBtn } from '../../styles/common'
import {dataURItoBlob, getModalMountNode} from "../../helpers/functions";

type Props = {
	getUserCollection: Function,
	onCollectionFormSubmit: Function,
    handleMenuChange: Function,
    getUserWishlist: Function,
    setCollectionInputVal: Function,
    fetchFollowersUsers: Function,
    fetchFollowingUsers: Function,
    getUserDetails: Function,
    getUserBooksList: Function,
    updateFollowStatus: Function,
    getUsersList: Function,
    removeFromWishlist: Function,
    addBookToWishlist: Function,
	collections: Array,
    params: Object,
    queryObj: Object,
    userData: Object,
    wishlist: Object,
    followers: Object,
    followings: Object,
    userBooksList: Object,
    searchedUser: Array,
    saveUploadedFile: Function,
    removeUploadImage: Function,
    searchedBooks: Function,
    requestUserBookSwap: Function,
    requestForGiveaway: Function,
    getUserBookHistory: Function,
    userBookHistory: Array,
    submitEditForm: Function,
    updateNewUserData: Function,
    uploadProfileImage: Function,
    uploadCoverImage: Function,
    updateUserProfile: Function,
    getUserCollection: Function,
    showMessagePopup: Function,
    getUserRequestCount: Function,
    coverImage: String,
    profileImage: String,
}

class Dashboard extends PureComponent<Props> {
	constructor() {
		super()
        this.state = {
            addBookModalOpen: false,
            addCollection: false,
            mode: 'read',
            profileUrl: '',
            profileObj: null,
            coverUrl: '',
            coverObj: null
        };
		this.getTabView = this.getTabView.bind(this);
        this.onMenuClick = this.onMenuClick.bind(this);
		this.getOtherUserTabView = this.getOtherUserTabView.bind(this)
        this.handleOpen = this.handleOpen.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.onAddWishlistFormSubmit = this.onAddWishlistFormSubmit.bind(this)
        this.updateViewMode = this.updateViewMode.bind(this)
        this.onFileInputChange = this.onFileInputChange.bind(this)
        this.onCoverInputChange = this.onCoverInputChange.bind(this)
        this.onEditFormSubmit = this.onEditFormSubmit.bind(this)
        this.openAddCollectionBtn = this.openAddCollectionBtn.bind(this)
        this.closeAddCollection = this.closeAddCollection.bind(this)
        this.getCoverPicUrl = this.getCoverPicUrl.bind(this)
        this.followUser = this.followUser.bind(this)
        this.unFollowUser = this.unFollowUser.bind(this)
	}
	componentDidMount() {
        const { params, userData } = this.props;
        const { userId, viewType } = params;
        if (viewType === 'collection') {
            this.props.getUserCollection({ user_id: userId, count: 0 })
        } else if (viewType === 'wishlist') {
            if (userData.id !== userId) {
                this.props.getUserWishlist({ user_id: userData.id, count: 0 })
            }
            this.props.getUserWishlist({ user_id: userId, count: 0 })
        }
	}

    followUser() {
        const { userDetails } = this.props
	    this.props.updateFollowStatus({
            following_id: get(userDetails, 'data.id', ''),
            follow: true,
        })
        this.props.getUserDetails({
            user_id: get(userDetails, 'data.id', '')
        })
    }

    unFollowUser() {
	    const { userDetails } = this.props
        this.props.updateFollowStatus({
            following_id: get(userDetails, 'data.id', ''),
            follow: false,
        })
        this.props.getUserDetails({
            user_id: get(userDetails, 'data.id', '')
        })
    }

    openAddCollectionBtn() {
	    this.setState({
            addCollection: true
        })
    }

    closeAddCollection() {
        this.setState({
            addCollection: false
        })
    }

	onMenuClick(e, data) {
        const { params, userData } = this.props
        const { userId } = params
        if (data.viewType === 'collection') {
            this.props.getUserCollection({ user_id: userId, count: 0 })
        } else if (data.viewType === 'wishlist') {
            this.props.getUserWishlist({ user_id: userData.id, count: 0 })
            this.props.getUserWishlist({ user_id: userId, count: 0 })
        }
        this.props.handleMenuChange(e, data)
    }

    handleOpen() {
	    this.setState({
            addBookModalOpen: true
        })
    }

    updateViewMode(mode) {
	    this.setState({
            mode
        })
    }

    handleClose() {
        this.props.removeUploadImage()
        this.setState({
            addBookModalOpen: false
        })
    }

    componentDidUpdate(prevProps) {
        const { params } = this.props
        const { userId } = params
        const prevParam = prevProps.params
        if (prevParam.userId !== userId) {
            this.props.getUserCollection({ user_id: userId, count: 0 })
        }
        // const { params, userData } = this.props
        // const { viewType, userId } = params
        // console.log(viewType, userId);
        // if (prevProps.params.viewType !== viewType) {
        //     if (viewType === 'collection') {
        //         this.props.getUserCollection({ user_id: userId, count: 0 })
        //     } else if (viewType === 'wishlist') {
        //         this.props.getUserWishlist({ user_id: userData.id, count: 0 })
        //         this.props.getUserWishlist({ user_id: userId, count: 0 })
        //     }
        // }
    }

    onAddWishlistFormSubmit(data) {
	    const {bookImage, params} = this.props;

        const { userId } = params

        data.book.image = bookImage.url;
        data.user_id = userId
        this.handleClose()
        this.props.addBookToWishlist(data)
    }

    addOtherUserBookToWishlist(book) {
	    const { userData } = this.props;
	    const data = {};
        data.book = book;
        data.user_id = userData.id;
        this.props.addBookToWishlist(data)
    }

    onEditFormSubmit(data) {
        const { profileObj, coverObj } = this.state;
	    const profilePromise = new Promise(((resolve, reject) => {
	        if (profileObj) {
                this.props.uploadProfileImage([profileObj])
                    .then(() => {
                        resolve(true)
                    })
            } else {
                resolve(true)
            }
        }));

        const coverPromise = new Promise(((resolve, reject) => {
            coverObj ? this.props.uploadCoverImage([coverObj], resolve, reject) : resolve(true)
        }));
        Promise.all([profilePromise, coverPromise]).then(() => {
            const { profileImage, coverImage } = this.props
            this.props.updateUserProfile({
                ...data,
                cover_pic: coverImage,
                profile_pic: profileImage
            })
            this.setState({
                mode: 'read',
                profileUrl: '',
                profileObj: null,
                coverUrl: '',
                coverObj: null
            })
        })
    }

    getOtherUserTabView (viewType) {
        const { collections = [], queryObj = {}, params, wishlist, followers, followings, userDetails, userData } = this.props
        const { userId } = params
        const currentUserWishlist = get(wishlist, `data.${userData.id}`, [])
        switch (viewType) {
            case 'collection':
                return (
                    <Grid>
						<Grid.Column only={'tablet computer'} tablet={4} computer={4} style={{ padding: '0 3rem 3rem' }}>
                            <UserProfileDetails
                                userDetails={userDetails}
                                userId={userId}
                                getUserDetails={this.props.getUserDetails}
                            />
						</Grid.Column>
						<Grid.Column tablet={12} computer={12} mobile={16}>
							<Grid columns={3} style={{ padding: '0 1rem' }}>
                                {
                                    collections.map((collection, key) =>[
                                            <Grid.Column mobile={8} tablet={3} computer={3} key={key}>
                                                <div>
                                                    <NavLink to={`/home/collection/${userId}?collection=${collection.category}`}>
                                                        <span style={{ fontSize: '1.4rem' }}>{collection.count === 1 ? '1 book' : `${collection.count} books`}</span>
                                                        <StyledImage style={{ width: '100%' }} src={get(collection, 'book.image', '')} />
                                                        <BookTitle>{collection.category}</BookTitle>
                                                    </NavLink>
                                                </div>
                                            </Grid.Column>,
                                            <Grid.Column
                                                tablet={1}
                                                computer={1}
                                                only='tablet computer'
                                                key={key + 100}
                                            />
                                        ]
                                    )
                                }
                                {
                                    (collections.length === 0) &&
                                    <Grid.Column textAlign="center" tablet={16} computer={16}>
                                        <NoDataFound as="h3" >No books collection found</NoDataFound>
                                    </Grid.Column>
                                }
							</Grid>
						</Grid.Column>
                    </Grid>
                );
            case 'wishlist':
                return (
                    <Grid>
                        <Grid.Column only={'tablet computer'} tablet={4} computer={4} style={{ padding: '0 3rem 3rem' }}>
                            <UserProfileDetails
                                userDetails={userDetails}
                                userId={userId}
                                getUserDetails={this.props.getUserDetails}
                            />
                        </Grid.Column>
                        <Grid.Column tablet={12} computer={12} mobile={16}>
                            <Grid style={{ padding: '0 1rem' }}>
                                {
                                    get(wishlist, `data.${userId}`, []).map((wl) => {
                                        const cuwlObj = currentUserWishlist.find((cuwl) => (cuwl.book.id === get(wl, 'book.id', '')))
                                        return (
                                            <Grid.Column tablet={4} computer={4} mobile={8} key={wl.id} textAlign={'center'}>
                                                <StyledImage src={get(wl, 'book.image', '')}/>
                                                <div style={{maxWidth: '8rem', padding: '10px'}}>
                                                    {
                                                        cuwlObj
                                                            ? <WishlistBtn onClick={() => this.props.removeFromWishlist({
                                                                user_id: userData.id,
                                                                bookId: get(cuwlObj, 'book.id', ''),
                                                                id: get(cuwlObj, 'id', '')
                                                            })}><img style={{width: '1.3rem'}}
                                                                     src={'/images/heartRed.svg'}/></WishlistBtn>
                                                            : <WishlistBtn onClick={() => this.addOtherUserBookToWishlist(wl.book)}><img style={{width: '1.3rem'}}
                                                                                src={'/images/heartOutline.svg'}/></WishlistBtn>
                                                    }
                                                </div>
                                            </Grid.Column>
                                        )
                                    })
                                }
                                {
                                    (get(wishlist, `data.${userId}`, []).length === 0) &&
                                    <Grid.Column textAlign="center" mobile={16} tablet={16} computer={16}>
                                        <NoDataFound as="h3" >No user wishlist found</NoDataFound>
                                    </Grid.Column>
                                }
                            </Grid>
                        </Grid.Column>
                    </Grid>
                );
            case 'followers':
                return (
                    <UserFollowers
                        fetchFollowersUsers={this.props.fetchFollowersUsers}
                        fetchFollowingUsers={this.props.fetchFollowingUsers}
                        followers={followers}
                        userId={userId}
                        followings={followings}
                    />
                );
            default:

        }
	}

	getTabView (viewType) {
		const { collections = [], queryObj, wishlist, params, followers,
            followings, userBooksList, searchedUser, bookImage, userData } = this.props;
        const { userId } = params;
		switch (viewType) {
			case 'collection':
				return (
				    <div>
                        <Grid>
                            <Grid.Column only={'tablet computer'} tablet={4} computer={4} style={{ padding: '0 3rem 3rem' }}>
                                <UserProfileDetails
                                    userDetails={{ data: userData }}
                                    currentUser
                                />
                            </Grid.Column>
                            <Grid.Column tablet={12} computer={12} mobile={16}>
                                <Grid style={{ padding: '0 1rem' }}>
                                    {
                                        collections.map((collection, key) =>[
                                                <Grid.Column mobile={8} tablet={3} computer={3} key={key}>
                                                    <div>
                                                        <NavLink to={`/home/collection/${userId}?collection=${collection.category}`}>
                                                            <span style={{ fontSize: '1.4rem' }}>{collection.count === 1 ? '1 book' : `${collection.count} books`}</span>
                                                            <StyledImage style={{ width: '100%' }} src={get(collection, 'book.image', '')} />
                                                            <BookTitle>{collection.category}</BookTitle>
                                                        </NavLink>
                                                    </div>
                                                </Grid.Column>,
                                                <Grid.Column
                                                    tablet={1}
                                                    computer={1}
                                                    only='tablet computer'
                                                    key={key + 100}
                                                />
                                            ]
                                        )
                                    }
                                    {
                                        (collections.length === 0) &&
                                        <Grid.Column textAlign="center" tablet={16} computer={16}>
                                            <NoDataFound as="h3" >No books collection found</NoDataFound>
                                        </Grid.Column>
                                    }
                                    {
                                        get(userData, 'firstTimeUser', true) &&
                                        <AddCategoryDemo updateNewUserData={this.props.updateNewUserData} />
                                    }
                                </Grid>
                            </Grid.Column>
                        </Grid>
                        <AddCollection onClick={this.props.openSearchBar} circular color='twitter' icon={{ name: 'plus', size: 'big' }} />
                        <Modal
                            closeIcon={<CloseBtn><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>}
                            mountNode={getModalMountNode()}
                            onClose={this.closeAddCollection}
                            open={this.state.addCollection}
                        >
                            <Modal.Content image>
                                <Modal.Description>
                                    <AddCollectionForm
                                        onSubmit={this.props.onCollectionFormSubmit}
                                        userId={userId}
                                        setCollectionInputVal={this.props.setCollectionInputVal}
                                        collections={collections}
                                        getUserCollection={this.props.getUserCollection}
                                    />
                                </Modal.Description>
                            </Modal.Content>
                        </Modal>
                    </div>
				)
			case 'wishlist':
				return (
				    <div>
                        <Grid container>
                            {
                                get(wishlist, `data.${userId}`, []).map((wl, key) =>
                                    <Grid.Column padded tablet={8} computer={8} mobile={16} key={key}>
                                        <Grid columns={2}>
                                            <Grid.Column width={7}>
                                                <StyledImage style={{ width: '80%' }} src={get(wl, 'book.image', '')} />
                                            </Grid.Column>
                                            <Grid.Column width={9} style={{ padding: '2rem 0' }}>
                                                <p style={{ fontSize: '1.4rem' }}>{get(wl, 'book.name', '')}</p>
                                                <BookDescription>{get(wl, 'book.authors', []).join(', ')}</BookDescription>
                                                <Divider hidden />
                                                <Divider hidden />
                                                <StarRating rating={parseInt(get(wl, 'book.averageRating', 0))} />
                                                <Divider hidden />
                                                {/*<Button size='big' style={{ color: 'black' }} secondary>Buy</Button>*/}
                                                <Button size='big' style={{ color: 'black' }} color="red" onClick={() => this.props.removeFromWishlist({ user_id: userId, bookId: get(wl, 'book.id', ''), id: get(wl, 'id', '') })}>Remove</Button>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                )
                            }
                            {
                                (get(wishlist, `data.${userId}`, []).length === 0) &&
                                <Grid.Column textAlign="center" tablet={16} computer={16}>
                                    <NoDataFound as="h3">No user wishlist found</NoDataFound>
                                </Grid.Column>
                            }
                        </Grid>
                        <AddWishListBtn onClick={this.props.openSearchBar} circular color='twitter' icon={{ name: 'plus', size: 'big' }} />
                        <Modal
                            mountNode={getModalMountNode()}
                            open={this.state.addBookModalOpen}
                            onClose={this.handleClose}
                            closeIcon={<CloseBtn><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>}
                        >
                            <Modal.Content image>
                                <Modal.Description>
                                    <AddBookForm
                                        onSubmit={this.onAddWishlistFormSubmit}
                                        saveUploadedFile={this.props.saveUploadedFile}
                                        bookImage={bookImage}
                                        viewType={'wishlist'}
                                    />
                                </Modal.Description>
                            </Modal.Content>
                        </Modal>
                    </div>
				);
			case 'bookTracking':
				return (
                    <BookTracking
                        queryObj={queryObj}
                        userId={userId}
                        getUserBooksList={this.props.getUserBooksList}
                        userBooksList={userBooksList}
                        searchedUser={searchedUser}
                        searchedBooks={this.props.searchedBooks}
                        getUsersList={this.props.getUsersList}
                        requestUserBookSwap={this.props.requestUserBookSwap}
                        requestForGiveaway={this.props.requestForGiveaway}
                        getUserBookHistory={this.props.getUserBookHistory}
                        userBookHistory={this.props.userBookHistory}
                    />
				);
            case 'followers':
                return (
                    <UserFollowers
                        fetchFollowersUsers={this.props.fetchFollowersUsers}
                        fetchFollowingUsers={this.props.fetchFollowingUsers}
                        followers={followers}
                        updateFollowStatus={this.props.updateFollowStatus}
                        userId={userId}
                        followings={followings}
                    />
                );
			default:

		}
	}

	getProfileImage(profilePic) {
	    if (profilePic) {
	       return <StyledImage style={{ width: '11rem' }} src={profilePic} circular />
        }
        return <StyledImage style={{ width: '11rem' }} src={'/images/profileImage.svg'} />
    }

    getMobileProfileImage(profilePic) {
        if (profilePic) {
            return <StyledImage style={{ width: '11rem', margin: '0 auto' }} src={profilePic} circular />
        }
        return <StyledImage style={{ width: '11rem', margin: '0 auto' }} src={'/images/profileImage.svg'} />
    }

    triggerFileUpload(e) {
        e.preventDefault();
        e.currentTarget.nextElementSibling.click();
    }

    onFileInputChange(e) {
        const { files } = e.currentTarget;
        const file = files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        const self = this
        reader.onloadend = () => {
            const img = new Image();
            img.onload = function() {
                const cvs = document.createElement('canvas');
                const minSize = Math.min(img.width, img.height);
                const left = (img.width - minSize) / 2;
                const top = (img.height - minSize) / 2;
                const ctx = cvs.getContext('2d');
                ctx.canvas.width = 250;
                ctx.canvas.height = 250;
                ctx.drawImage(img, left, top, minSize, minSize, 0, 0, ctx.canvas.width, ctx.canvas.height);
                const newImageData = cvs.toDataURL('image/jpeg', 0.7);
                const blob = dataURItoBlob(newImageData);
                const fileObj = new File([blob], file.name);
                self.setState({
                    profileUrl: newImageData,
                    profileObj: fileObj
                });
                self.props.uploadProfileImage([fileObj])
            };
            img.src = reader.result;
        };
    }

    onCoverInputChange(e) {
	    const { files } = e.currentTarget;
        const file = files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        const self = this
        reader.onloadend = () => {
            const img = new Image();
            img.onload = function() {
                const cvs = document.createElement('canvas');
                const ctx = cvs.getContext('2d');
                ctx.canvas.width = 1428;
                ctx.canvas.height = 277;
                ctx.drawImage(img, (ctx.canvas.width - img.width) / 2, (ctx.canvas.height - img.height) / 2);
                const newImageData = cvs.toDataURL('image/jpeg', 0.7);
                const blob = dataURItoBlob(newImageData);
                const fileObj = new File([blob], file.name);
                self.setState({
                    coverUrl: newImageData,
                    coverObj: fileObj
                });
                self.props.uploadProfileImage([fileObj])
            };
            img.src = reader.result;
        };
    }

    getCoverPicUrl(coverUrl, userData, userId, userDetails) {
	    if (userId === userData.id) {
	        return (coverUrl || userData.cover_pic)
        }
        return (coverUrl || get(userDetails, 'data.cover_pic', ''))
    }

	render () {
		const { params, userData, userDetails } = this.props;
		const { viewType, userId } = params;
        const { mode, profileUrl, coverUrl } = this.state;
		return (
			<div>
                {
                    (mode === 'read')
                        ? <TopBanner url={this.getCoverPicUrl(coverUrl, userData, userId, userDetails)}></TopBanner>
                        : <div>
                            <EditCoverBtn onClick={this.triggerFileUpload}>
                                <TopBanner url={this.getCoverPicUrl(coverUrl, userData, userId, userDetails)}></TopBanner>
                                <ChangeCoverText>Change Cover Photo</ChangeCoverText>
                            </EditCoverBtn>
                            <input style={{ display: 'none' }} onChange={this.onCoverInputChange} type={'file'} />
                        </div>
                }
                <Grid columns={1}>
                    <Grid.Column only={'tablet computer'}>
                        {
                            userId === userData.id
                                ? (
                                    <div>
                                        <StyledMenu pointing size={'massive'} secondary style={{ borderColor: '#251086' }}>
                                            <ImageMenu>
                                                {
                                                    (mode === 'read')
                                                        ? <div style={{ position: 'relative', top: '-4.3rem' }}>{this.getProfileImage(userData.profile_pic)}</div>
                                                        : (
                                                            <div>
                                                                <EditProfileView style={{ position: 'relative', top: '-4.3rem' }} onClick={this.triggerFileUpload}>
                                                                    {this.getProfileImage(profileUrl || userData.profile_pic)}
                                                                    {
                                                                        userData.profile_pic
                                                                            ? <ChangePhotoBtn>Change Photo</ChangePhotoBtn>
                                                                            : <ChangePhotoBtn>Add Photo</ChangePhotoBtn>
                                                                    }
                                                                </EditProfileView>
                                                                <input style={{ display: 'none' }} onChange={this.onFileInputChange} type={'file'} />
                                                            </div>
                                                        )
                                                }
                                            </ImageMenu>
                                            {
                                                data.tabs(userData.id).map((menuObj, key) =>
                                                    <Menu.Item
                                                        index={key}
                                                        onClick={this.onMenuClick}
                                                        key={key}
                                                        disabled={mode === 'edit'}
                                                        viewType={menuObj.viewType}
                                                        name={menuObj.name}
                                                        link={menuObj.link}
                                                        active={viewType === menuObj.viewType}
                                                    />
                                                )
                                            }
                                            {
                                                (mode === 'read')
                                                    ? (
                                                        <div style={{ display: 'contents' }}>
                                                            {
                                                                data.menuRightButton.map((buttonObj, key) =>
                                                                    <Menu.Item key={key} position='right'>
                                                                        <Button size='tiny' basic style={{ borderRadius: '6.25rem', fontSize: '1rem' }} onClick={() => this.updateViewMode('edit')}>{buttonObj.name}</Button>
                                                                    </Menu.Item>
                                                                )
                                                            }
                                                        </div>
                                                    ) : (
                                                        <Menu.Item position='right'>
                                                            <EditModeBtn onClick={() => this.updateViewMode('read')} color={'twitter'} size='tiny'>
                                                                <b style={{ color: 'black' }}>Cancle</b>
                                                            </EditModeBtn>
                                                            <EditModeBtn size='tiny' secondary onClick={this.props.submitEditForm}>
                                                                <b style={{ color: 'black' }}>Save</b>
                                                            </EditModeBtn>
                                                        </Menu.Item>
                                                    )
                                            }
                                        </StyledMenu>
                                        {
                                            (mode === 'edit')
                                            ? (
                                                <Grid>
                                                    <Grid.Column only={'tablet computer'} tablet={4} computer={4} style={{ padding: '0 3rem 3rem' }}>
                                                        <UserProfileDetails
                                                            userDetails={{ data: userData }}
                                                            currentUser
                                                        />
                                                    </Grid.Column>
                                                    <Grid.Column tablet={12} computer={12} mobile={16}>
                                                        <div style={{ padding: '0 15%', maxWidth: '70%' }}><EditProfileForm showMessagePopup={this.props.showMessagePopup} initialValues={userData} onSubmit={this.onEditFormSubmit} /></div>
                                                    </Grid.Column>
                                                </Grid>
                                            ) : this.getTabView(viewType)
                                        }
                                    </div>
                                ) : (
                                    <div>
                                        <StyledMenu pointing size={'massive'} secondary style={{ borderColor: '#251086' }}>
                                            <ImageMenu>
                                                <div style={{ position: 'relative', top: '-4.3rem' }}>
                                                    <StyledImage style={{ width: '11rem' }} src={get(userDetails, 'data.profile_pic', '')} circular />
                                                </div>
                                            </ImageMenu>
                                            {
                                                otherUserData.tabs(userId).map((menuObj, key) =>
                                                    <Menu.Item
                                                        index={key}
                                                        onClick={this.onMenuClick}
                                                        key={key}
                                                        name={menuObj.name}
                                                        link={menuObj.link}
                                                        viewType={menuObj.viewType}
                                                        active={viewType === menuObj.viewType}
                                                    />
                                                )
                                            }
                                            {
                                                otherUserData.menuRightButton.map((buttonObj, key) =>
                                                    <Menu.Item key={key} position='right'>
                                                        {
                                                            get(userDetails, 'data.following_him', '')
                                                                ? <Button size='tiny' secondary style={{ borderRadius: '6.25rem', fontSize: '1rem' }} onClick={this.unFollowUser}>
                                                                    <b style={{ color: 'black' }}>Following</b>
                                                                </Button>
                                                                : <Button size='tiny' basic style={{ borderRadius: '6.25rem', fontSize: '1rem' }} onClick={this.followUser}>{buttonObj.name}</Button>
                                                        }
                                                    </Menu.Item>
                                                )
                                            }
                                        </StyledMenu>
                                        {this.getOtherUserTabView(viewType)}
                                    </div>
                                )
                        }
                    </Grid.Column>
                    <Grid.Column only={'mobile'}>
                        {
                            userId === userData.id
                                ? (
                                    <div>
                                        <div style={{ position: 'relative', top: '-4.3rem', textAlign: 'center' }}>
                                            {
                                                (mode === 'read')
                                                    ? this.getMobileProfileImage(userData.profile_pic)
                                                    : (
                                                        <div>
                                                            <EditProfileView onClick={this.triggerFileUpload}>
                                                                {this.getMobileProfileImage(profileUrl || userData.profile_pic)}
                                                                {
                                                                    userData.profile_pic
                                                                        ? <ChangePhotoBtn>Change Photo</ChangePhotoBtn>
                                                                        : <ChangePhotoBtn>Add Photo</ChangePhotoBtn>
                                                                }
                                                            </EditProfileView>
                                                            <input style={{ display: 'none' }} onChange={this.onFileInputChange} type={'file'} />
                                                        </div>
                                                    )
                                            }
                                            <UserProfileDetails
                                                userDetails={{ data: userData }}
                                                currentUser
                                            />
                                            <div style={{ position: 'absolute', right: 0, bottom: '-3rem' }}>
                                                {
                                                    (mode === 'read')
                                                        ? (
                                                            <div>
                                                                {
                                                                    data.menuRightButton.map((buttonObj, key) =>
                                                                        <Button key={key} size='tiny' basic style={{ borderRadius: '6.25rem', fontSize: '1rem' }} onClick={() => this.updateViewMode('edit')}>{buttonObj.name}</Button>
                                                                    )
                                                                }
                                                            </div>
                                                        ) : (
                                                            <div>
                                                                <EditModeBtn onClick={() => this.updateViewMode('read')} color={'twitter'} size='tiny'>
                                                                    <b style={{ color: 'black' }}>Cancle</b>
                                                                </EditModeBtn>
                                                                <EditModeBtn size='tiny' secondary onClick={this.props.submitEditForm}>
                                                                    <b style={{ color: 'black' }}>Save</b>
                                                                </EditModeBtn>
                                                            </div>
                                                        )
                                                }
                                            </div>
                                        </div>
                                        <StyledMenu pointing size={'huge'} secondary style={{ borderColor: '#251086' }}>
                                            {
                                                data.tabs(userData.id).map((menuObj, key) =>
                                                    <Menu.Item
                                                        index={key}
                                                        onClick={this.onMenuClick}
                                                        key={key}
                                                        disabled={mode === 'edit'}
                                                        viewType={menuObj.viewType}
                                                        name={menuObj.name}
                                                        link={menuObj.link}
                                                        active={viewType === menuObj.viewType}
                                                    />
                                                )
                                            }
                                        </StyledMenu>
                                        {
                                            (mode === 'edit')
                                                ? (
                                                    <Grid>
                                                        <Grid.Column only={'tablet computer'} tablet={4} computer={4} style={{ padding: '0 3rem 3rem' }}>
                                                            <UserProfileDetails
                                                                userDetails={{ data: userData }}
                                                                currentUser
                                                            />
                                                        </Grid.Column>
                                                        <Grid.Column tablet={12} computer={12} mobile={16}>
                                                            <div style={{ padding: '0 15%' }}><EditProfileForm showMessagePopup={this.props.showMessagePopup} initialValues={userData} onSubmit={this.onEditFormSubmit} /></div>
                                                        </Grid.Column>
                                                    </Grid>
                                                ) : this.getTabView(viewType)
                                        }
                                    </div>
                                ) : (
                                    <div>
                                        <div style={{ position: 'relative', top: '-4.3rem', textAlign: 'center' }}>
                                            {this.getMobileProfileImage(get(userDetails, 'data.profile_pic', ''))}
                                            <UserProfileDetails
                                                userDetails={{ data: userData }}
                                                currentUser
                                            />
                                            <div style={{ position: 'absolute', right: 0, bottom: '-3rem' }}>
                                                {
                                                    otherUserData.menuRightButton.map((buttonObj, key) =>
                                                        <Menu.Item key={key} position='right'>
                                                            {
                                                                get(userDetails, 'data.following_him', '')
                                                                    ? <Button size='tiny' secondary style={{ borderRadius: '6.25rem', fontSize: '1rem' }} onClick={this.unFollowUser}>
                                                                        <b style={{ color: 'black' }}>Following</b>
                                                                    </Button>
                                                                    : <Button size='tiny' basic style={{ borderRadius: '6.25rem', fontSize: '1rem' }} onClick={this.followUser}>{buttonObj.name}</Button>
                                                            }
                                                        </Menu.Item>
                                                    )
                                                }
                                            </div>
                                        </div>
                                        <StyledMenu pointing size={'huge'} secondary style={{ borderColor: '#251086' }}>
                                            {
                                                otherUserData.tabs(userId).map((menuObj, key) =>
                                                    <Menu.Item
                                                        index={key}
                                                        onClick={this.onMenuClick}
                                                        key={key}
                                                        name={menuObj.name}
                                                        link={menuObj.link}
                                                        viewType={menuObj.viewType}
                                                        active={viewType === menuObj.viewType}
                                                    />
                                                )
                                            }
                                        </StyledMenu>
                                        {this.getOtherUserTabView(viewType)}
                                    </div>
                                )
                        }
                    </Grid.Column>
                </Grid>
			</div>
		)
	}
}

export default Dashboard
