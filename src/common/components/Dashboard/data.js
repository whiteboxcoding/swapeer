export const data = {
	tabs: (userId) => ([
		{
			name: 'Collection',
			link: `/home/collection/${userId}`,
            viewType: 'collection',
		},
		{
			name: 'Wishlist',
            link: `/home/wishlist/${userId}`,
			viewType: 'wishlist',
		},
		{
			name: 'Where is my book ?',
            link: `/home/bookTracking/${userId}`,
			viewType: 'bookTracking',
		},
		{
			name: 'Follows',
            link: `/home/followers/${userId}`,
			viewType: 'followers'
		}
	]),
	menuRightButton: [
		{
			name: 'Edit Profile',
			link: '/'
		}
	],
}

export const otherUserData = {
    tabs: (id) => ([
        {
            name: 'Collection',
            link: `/home/collection/${id}`,
            viewType: 'collection',
        },
        {
            name: 'Wishlist',
            link: `/home/wishlist/${id}`,
            viewType: 'wishlist',
        },
        {
            name: 'Follows',
            link: `/home/followers/${id}`,
            viewType: 'followers'
        }
    ]),
    menuRightButton: [
        {
            name: 'Follow',
            link: '/'
        }
    ],
}
