import { Button, Menu, Header } from 'semantic-ui-react'
import styled from 'styled-components'

export const ImageMenu  = styled(Menu.Item)`
    height: 2.5rem;
    img {
        width: 11rem;
    }
`;

export const NoDataFound = styled(Header)`
    margin: 8% 0;
`;

export const AddCollection = styled(Button)`
    position: sticky;
    bottom: 10%;
    float: right;
    margin-right: 7%;
`;

export const AddWishListBtn = styled(Button)`
    position: sticky;
    bottom: 10%;
    left: 90%;
`;

export const TopBanner = styled.div`
    background-image: ${(props) => (
        props.url 
            ? `url('${props.url}')` 
            : ''
    )};
    
    background-color: ${(props) => (
        props.url
            ? ''
            : '#938ac4'
    )};
    height: 17.3rem;
    background-size: 100%;
    background-position-y: center;
`;

export const BookDescription = styled.p`
    font-size: 1.3rem;
`;

export const StyledMenu = styled(Menu)`
    border-color: #251086;
    margin-bottom: 1rem;
`;

export const BookTitle = styled.p`
    background: #251086;
    color: white;
    text-align: center;
    font-size: 1.4rem;
    padding: 1rem 0;
`;

export const WishlistBtn = styled.button`
    outline: none;
    border: none;
    cursor: pointer;
`;

export const EditProfileView = styled.button`
    background: white;
    border-radius: 100px;
    padding: 0;
    border: none;
    cursor: pointer;
    opacity: 0.5;
    outline: none;
`;

export const EditCoverBtn = styled.button`
    background: white;
    padding: 0;
    border: none;
    cursor: pointer;
    opacity: 0.5;
    width: 100%;
    position: relative;
    outline: none;
`;

export const ChangePhotoBtn = styled.span`
    position: absolute;
    top: 40%;
    width: 100%;
    left: 0;
    color: white;
`;

export const ChangeCoverText = styled.span`
    font-size: 2rem;
    position: absolute;
    bottom: 30%;
    width: 100%;
    left: 0;
    color: white;
`;

export const EditModeBtn = styled(Button)`
    font-size: 1rem;
    margin-right: 0.5rem;
    border-radius: 100px;
    padding: 0.5rem 1rem;
`;
