// @flow
import React, {PureComponent} from 'react'
import { Form, Input, Segment, Button } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { required } from 'common/validators'
import FormField from '../FormField'
import UserCollectionDropdown from '../UserCollectionDropdown'
import { Wrapper, AutocompleteItems } from './style'

type Props = {
	handleSubmit: Function,
	userId: String,
    getUserCollection: Function,
    setCollectionInputVal: Function,
    collections: Array,
}

class AddCollectionForm extends PureComponent<Props> {
	render () {
		return (
			<Form onSubmit={this.props.handleSubmit}>
				<UserCollectionDropdown
                    getUserCollection={this.props.getUserCollection}
                	setCollectionInputVal={this.props.setCollectionInputVal}
                    collections={this.props.collections}
                    userId={this.props.userId}
					form={'addCollectionForm'}
				/>
			</Form>
		)
	}
}

export default reduxForm({
	form: 'addCollectionForm'
})(AddCollectionForm)
