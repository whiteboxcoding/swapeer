import styled from 'styled-components'
import { Grid, Message, Form, Button } from 'semantic-ui-react'

export const Wrapper = styled(Grid)`
    height: calc(100vh - 10rem);
`;

export const ScrollCol = styled(Grid.Column)`
    overflow-y: scroll;
    height: 82vh;
`;

export const MobileScrollCol = styled(Grid.Column)`
    overflow-x: scroll;
`;

export const StyledMessage = styled(Message)`
    border-radius: 6.25rem !important;
    width: 67%;
    margin-top: 2.19rem !important;
    margin-bottom: 2.19rem !important;
`;

export const RightMessage = styled(StyledMessage)`
    margin-left: auto !important;
`;

export const RoundedButton = styled(Button)`
    border-radius: 6.25rem !important;
`;

export const UserDataText = styled.p`
    color: black;
    font-size: 1.125rem;
`;

export const MessageWrapper = styled.div`
    padding-bottom: 5rem;
`;

export const ArrowIcon = styled(Form.Button)`
    padding: 0 !important;
    box-shadow: none !important;
    font-size: 2rem !important;
`;

export const ChatForm = styled(Form)`
    position: fixed !important;
    width: 62%;
    bottom: 0;
    background: white;
    padding-top: 1rem;
    padding-bottom: 0;
    padding-right: 40px;
    margin: 0;
`;

export const MobileChatForm = styled(Form)`
    position: fixed !important;
    width: 100%;
    bottom: 10px;
    background: white;
    padding-top: 1rem;
    padding-bottom: 0;
    margin: 0;
`;
