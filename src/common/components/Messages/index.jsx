// @flow
import React, {PureComponent} from 'react'
import uniqBy from 'lodash/uniqBy'
import UserProfileImage from '../UserProfileImage'
import NoDataFoundScreen from '../NoDataFoundScreen'
import get from 'lodash/get'
import { NavLink } from 'react-router-dom'
import { Grid, Image, Segment, Message, Form, Button, Header, List } from 'semantic-ui-react'
import {
    Wrapper,
    ScrollCol,
    StyledMessage,
    RightMessage,
    RoundedButton,
    UserDataText,
    MessageWrapper,
    ArrowIcon,
    ChatForm,
    MobileScrollCol,
    MobileChatForm
} from './style'

type Props = {
    joinChatRoom: Function,
    getChatMessages: Function,
    userChat: Object,
    sendChatMessage: Function,
    getUserDonatesList: Function,
    getUserDetails: Function,
    updatePageUrl: Function,
    saveNewMessages: Function,
    getUserRequestedSwaps: Function,
    userDetails: Object,
    params: Object,
    exchanges: Object,
    donates: Object,
    userId: Object,
    requestedSwaps: Object,
}

class Messages extends PureComponent<Props> {
	constructor() {
		super()
		this.state = {
			text: ''
		}
        this.activeUserId = ''
		this.onChatFormSubmit = this.onChatFormSubmit.bind(this)
		this.onInputChange = this.onInputChange.bind(this)
    this.onChatScroll = this.onChatScroll.bind(this)
	}
	componentDidMount() {
		const { params } = this.props;
        this.props.getUserDonatesList({
            user_id: params.userId,
            count: 0
        });
        this.props.getUserMatchesList({
            user_id: params.userId,
            count: 0
		});
	}

	componentDidUpdate(prevProps) {
        const { userChat } = this.props;
        const prevChat = prevProps.userChat || { data: [] }
        if (prevChat.data.length !== userChat.data.length) {
            const el = document.getElementById('messageWrapper')
            el.scrollTop = el.scrollHeight;
		}
	}

  onChatScroll(e) {
    const { params, userChat } = this.props
    const el = e.currentTarget;
    if (el.scrollTop < 1) {
      this.props.getChatMessages({
          user_id: params.userId,
          other_user_id : this.activeUserId,
          count: get(userChat, 'data', []).length
      });
    }
  }

    onInputChange(e, data) {
		this.setState({
            text: data.value
		})
	}

    onChatFormSubmit(e) {
		const { text } = this.state
        const { params } = this.props;
        this.setState({
            text: ''
        });

		this.props.sendChatMessage({
            message: text,
            from: params.userId,
            to: this.activeUserId
        })
	}

	isActiveUser(activeUserId, userId, index) {
		if (activeUserId) {
			if (activeUserId === userId) {
				return true
			}
			return false
		} else {
			if (index === 0) {
                return true
			}
            return false
		}
	}

    getUserBooksCount(exchanges, donatesData, activeUserId) {
	    let count = 0
        const exchange = exchanges.data.find((exchng) => exchng.id === activeUserId)
        const donates = donatesData.data.find((donateObj) => donateObj.id === activeUserId)
        if (exchange) {
            count += exchange.collection.length
        }
        if (donates) {
	        count += donates.books.length
        }
        return count
    }

    getMobileUserBooksList(exchanges, donatesData, activeUserId, userId, requestedSwaps) {
        let arr = []
        const exchange = exchanges.data.find((exchng) => exchng.id === activeUserId)
        const request = requestedSwaps.data.find((swap) => (
            get(swap, 'user_to.book.id', '') === userId && get(swap, 'user_from.book.id', '') === activeUserId
        ));
        if (exchange) {
            const exchangeArr = exchange.collection.map((book) =>
                <div key={book.id} style={{ width: '38.9%', whiteSpace: 'initial', marginRight: '5%', display: 'inline-block' }}>
                    <Image src={book.image} />
                    <UserDataText style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>{book.name}</UserDataText>
                    <p style={{ color: 'black', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>{book.authors.join(', ')}</p>
                    {
                        request
                        ? (
                            <RoundedButton size='tiny' color={'red'}>
                                <b style={{ color: 'black' }}>Requested</b>
                            </RoundedButton>
                        ) : (
                            <NavLink to={`/swaps/${userId}?tab=exchange`}>
                                <RoundedButton size='tiny' secondary>
                                    <b style={{ color: 'black' }}>Request Swap</b>
                                </RoundedButton>
                            </NavLink>
                        )
                    }
                </div>
            )
            arr = arr.concat(exchangeArr)
        }
        const donates = donatesData.data.find((donateObj) => donateObj.id === activeUserId)
        if (donates) {
            const donatesArr = donates.books.map((book) =>
                <div key={book.user_book_id} style={{ width: '38.9%', whiteSpace: 'initial', marginRight: '5%', display: 'inline-block' }}>
                    <Image src={get(book, 'book.image', '')} />
                    <UserDataText style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>{get(book, 'book.name', '')}</UserDataText>
                    <p style={{ color: 'black', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>{get(book, 'book.authors', []).join(', ')}</p>
                    {
                        book.request
                        ? (
                            <RoundedButton size='tiny' color={'red'}>
                                <b style={{ color: 'black' }}>Requested</b>
                            </RoundedButton>
                        ) : (
                            <NavLink to={`/swaps/${userId}?tab=donate`}>
                                <RoundedButton size='tiny' secondary>
                                    <b style={{ color: 'black' }}>Request Book</b>
                                </RoundedButton>
                            </NavLink>
                        )
                    }
                </div>
            );
            arr = arr.concat(donatesArr)
        }
        return arr
    }

    getUserBooksList(exchanges, donatesData, activeUserId, userId, requestedSwaps) {
	    let arr = []
	    const exchange = exchanges.data.find((exchng) => exchng.id === activeUserId)
        if (exchange) {
            const exchangeArr = exchange.collection.map((book) => {
                const request = requestedSwaps.data.find((swap) => (
                    get(swap, 'user_from.book.id', '') === book.id
                ));
                return (
                    <Grid key={book.id} columns={2} padded>
                        <Grid.Column width={6}>
                            <Image src={book.image} />
                        </Grid.Column>
                        <Grid.Column width={10}>
                            <UserDataText>{book.name}</UserDataText>
                            <p style={{ color: 'black' }}>{book.authors.join(', ')}</p>
                            {
                                request
                                    ? (
                                        <RoundedButton size='tiny' color={'red'}>
                                            <b style={{ color: 'black' }}>Requested</b>
                                        </RoundedButton>
                                    ) : (
                                        <NavLink to={`/swaps/${userId}?tab=exchange`}>
                                            <RoundedButton size='tiny' secondary>
                                                <b style={{ color: 'black' }}>Request Swap</b>
                                            </RoundedButton>
                                        </NavLink>
                                    )
                            }
                        </Grid.Column>
                    </Grid>
                )
            })
            arr = arr.concat(exchangeArr)
        }
        const donates = donatesData.data.find((donateObj) => donateObj.id === activeUserId)
        if (donates) {
            const donatesArr = donates.books.map((book) =>
                <Grid key={book.user_book_id} columns={2} padded>
                    <Grid.Column width={6}>
                        <Image src={get(book, 'book.image', '')} />
                    </Grid.Column>
                    <Grid.Column width={10}>
                        <UserDataText>{get(book, 'book.name', '')}</UserDataText>
                        <p style={{ color: 'black' }}>{get(book, 'book.authors', []).join(', ')}</p>
                        {
                            book.request
                                ? (
                                    <RoundedButton size='tiny' color={'red'}>
                                        <b style={{ color: 'black' }}>Requested</b>
                                    </RoundedButton>
                                ) : (
                                    <NavLink to={`/swaps/${userId}?tab=donate`}>
                                        <RoundedButton size='tiny' secondary>
                                            <b style={{ color: 'black' }}>Request Book</b>
                                        </RoundedButton>
                                    </NavLink>
                                )
                        }
                    </Grid.Column>
                </Grid>
            );
            arr = arr.concat(donatesArr)
        }
        return arr
    }

	render () {
		const { userChat, userId, exchanges, userDetails, params, donates, requestedSwaps } = this.props;
		const { text } = this.state;
		const allUsers = uniqBy(exchanges.data.concat(donates.data), 'id')
        this.activeUserId = userId || get(allUsers, '0.id', '');
		const count = this.getUserBooksCount(exchanges, donates, this.activeUserId)
		return (
            this.activeUserId
			? (
                    <Wrapper>
                        <Grid.Row only={'tablet computer'}>
                            <ScrollCol style={{ background: '#FB4B4E' }} width={2}>
                                <Grid columns={1} padded textAlign='center'>
                                    {
                                        allUsers.map((user, i) => (
                                            <UserProfileImage
                                                key={user.id}
                                                getChatMessages={this.props.getChatMessages}
                                                getUserRequestedSwaps={this.props.getUserRequestedSwaps}
                                                saveNewMessages={this.props.saveNewMessages}
                                                userId={params.userId}
                                                updatePageUrl={this.props.updatePageUrl}
                                                getUserDetails={this.props.getUserDetails}
                                                user={user}
                                                isActiveUser={this.isActiveUser(userId, user.id, i)}
                                            />
                                        ))
                                    }
                                </Grid>
                            </ScrollCol>
                            <ScrollCol id={'messageWrapper'} onScroll={this.onChatScroll} width={10}>
                                <MessageWrapper>
                                    {
                                        userChat.data.map((obj) => {
                                            if (obj.from === params.userId) {
                                                return (
                                                    <StyledMessage
                                                        key={obj.id}
                                                        info
                                                    >
                                                        {obj.message}
                                                    </StyledMessage>
                                                )
                                            }
                                            if (obj.from === this.activeUserId) {
                                                return (
                                                    <RightMessage
                                                        key={obj.id}
                                                        info
                                                    >
                                                        {obj.message}
                                                    </RightMessage>
                                                )
                                            }
                                        })
                                    }
                                </MessageWrapper>
                                <ChatForm onSubmit={this.onChatFormSubmit}>
                                    <Form.Group>
                                        <Form.Input value={text} onChange={this.onInputChange} placeholder='Just a few messages away from your next favourite book!' width={15} />
                                        <ArrowIcon id='arrowIcon' type={'submit'} width={1} size='huge' color='teal' basic icon='paper plane' />
                                    </Form.Group>
                                </ChatForm>
                            </ScrollCol>
                            <ScrollCol color='teal' width={4}>
                                <Grid columns={2} padded textAlign='center'>
                                    <Grid.Column width={7}>
                                        <Image centered size='small' circular src={get(userDetails, 'data.profile_pic')} />
                                    </Grid.Column>
                                    <Grid.Column width={9} textAlign='left'>
                                        <UserDataText>{get(userDetails, 'data.name')}</UserDataText>
                                        <UserDataText>{get(userDetails, 'data.bookCount') === 1 ? '1 Book' : `${get(userDetails, 'data.bookCount')} Books`}</UserDataText>
                                        <UserDataText>{get(userDetails, 'data.address')}</UserDataText>
                                        <NavLink to={`/home/collection/${get(userDetails, 'data.id')}`}>
                                            <RoundedButton
                                                fluid
                                                secondary
                                                size='mini'
                                            >
                                                <b style={{ color: 'black' }}>Explore Library</b>
                                            </RoundedButton>
                                        </NavLink>
                                    </Grid.Column>
                                </Grid>
                                <br />
                                <UserDataText>
                                    {count === 1 ? '1 Match' : `${count} Matches`}
                                </UserDataText>
                                {this.getUserBooksList(exchanges, donates, this.activeUserId, params.userId, requestedSwaps)}
                            </ScrollCol>
                        </Grid.Row>
                        <Grid.Row only={'mobile'}>
                            <Grid.Column width={16}>
                                <Grid verticalAlign={'middle'} style={{ background: '#FB4B4E' }} columns={3} padded>
                                    {
                                        allUsers.map((user, i) => (
                                            <UserProfileImage
                                                key={user.id}
                                                getChatMessages={this.props.getChatMessages}
                                                saveNewMessages={this.props.saveNewMessages}
                                                getUserRequestedSwaps={this.props.getUserRequestedSwaps}
                                                userId={params.userId}
                                                updatePageUrl={this.props.updatePageUrl}
                                                getUserDetails={this.props.getUserDetails}
                                                user={user}
                                                isActiveUser={this.isActiveUser(userId, user.id, i)}
                                            />
                                        ))
                                    }
                                </Grid>
                                <Grid style={{ display: 'none' }} color={'teal'}>
                                    <Grid.Column width={5}>
                                        <Grid columns={1} padded textAlign='center'>
                                            <Grid.Column>
                                                <Image centered size='small' circular src={get(userDetails, 'data.profile_pic')} />
                                            </Grid.Column>
                                            <Grid.Column textAlign='left'>
                                                <UserDataText>{get(userDetails, 'data.name')}</UserDataText>
                                                <UserDataText>{get(userDetails, 'data.bookCount') === 1 ? '1 Book' : `${get(userDetails, 'data.bookCount')} Books`}</UserDataText>
                                                <UserDataText>{get(userDetails, 'data.address')}</UserDataText>
                                                <NavLink to={`/home/collection/${get(userDetails, 'data.id')}`}>
                                                    <RoundedButton
                                                        fluid
                                                        secondary
                                                        size='mini'
                                                    >
                                                        <b style={{ color: 'black' }}>Explore Library</b>
                                                    </RoundedButton>
                                                </NavLink>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                    <Grid.Column width={11}>
                                        <div style={{ overflowX: 'auto', width: '100%', whiteSpace: 'nowrap' }}>
                                            {this.getMobileUserBooksList(exchanges, donates, this.activeUserId, params.userId, requestedSwaps)}
                                        </div>
                                    </Grid.Column>
                                </Grid>
                                <div style={{ overflowY: 'scroll', height: '66vh', padding: '1rem' }} id={'messageWrapper'} onScroll={this.onChatScroll}>
                                    <div>
                                        {
                                            userChat.data.map((obj) => {
                                                if (obj.from === params.userId) {
                                                    return (
                                                        <StyledMessage
                                                            key={obj.id}
                                                            info
                                                        >
                                                            {obj.message}
                                                        </StyledMessage>
                                                    )
                                                }
                                                if (obj.from === this.activeUserId) {
                                                    return (
                                                        <RightMessage
                                                            key={obj.id}
                                                            info
                                                        >
                                                            {obj.message}
                                                        </RightMessage>
                                                    )
                                                }
                                            })
                                        }
                                    </div>
                                    <MobileChatForm onSubmit={this.onChatFormSubmit}>
                                        <Grid>
                                            <Grid.Column width={13}>
                                                <Form.Input value={text} onChange={this.onInputChange} placeholder='Just a few messages away from your next favourite book!' />
                                            </Grid.Column>
                                            <Grid.Column width={2}>
                                                <ArrowIcon id='arrowIcon' type={'submit'} size='huge' color='teal' basic icon='paper plane' />
                                            </Grid.Column>
                                        </Grid>
                                    </MobileChatForm>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                    </Wrapper>
			) : <NoDataFoundScreen
      text='Looks like you do not have any friend to chat :('
      buttonText='Invite friends to join for more books!'
      />
		)
	}
}

export default Messages
