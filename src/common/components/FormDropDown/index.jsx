// @flow
import React from 'react'
import { Dropdown } from 'semantic-ui-react'
import { ErrorText, FieldWrapper } from './style'

const FormDropDown = ({
   input,
   required,
   options,
   search,
   as,
   meta: { touched, error },
   ...rest
}: OptionsFieldProps) => {
	const Element = as || Dropdown
	return (
		<FieldWrapper>
			<Element
				style={{ margin: 0 }}
                search={search}
                value={input.value}
                required={required}
                options={options}
                error={!!(touched && error)}
                onChange={(event, data) => input.onChange(data.value)}
                {...rest}
			/>
			{touched && error && <ErrorText><small>{error}</small></ErrorText>}
		</FieldWrapper>
	)
}
export default FormDropDown
