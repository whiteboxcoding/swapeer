import styled from 'styled-components'
import {brandError} from '../../styles/variables'

export const ErrorText = styled.p`
    color: ${brandError}
`;

export const FieldWrapper = styled.div`
    margin-bottom: 2rem;
`;
