import { Grid } from 'semantic-ui-react'
import styled from 'styled-components'

export const ItemWrapper = styled(Grid)`
    margin: 0.625rem 0;
`;

export const WishlistBtn = styled.button`
    outline: none;
    border: none;
    cursor: pointer;
    padding: 4px;
    background: white;
`;
