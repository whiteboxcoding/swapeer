// @flow
import React, {PureComponent} from 'react'
import { Grid, Image, Segment, Dropdown } from 'semantic-ui-react'
import get from 'lodash/get'
import { ItemWrapper, WishlistBtn } from './style'
import { CloseBtn, MobileCloseBtn } from '../../styles/common'

type Props = {
    book: Object,
    showWishList: Boolean,
    addBookToWishlist: Function,
    removeFromWishlist: Function,
    removeBookFromCollection: Function,
    updateBookStatus: Function,
    wishlist: Array,
    userId: String,
    collection: String,
}

class BookEntryItem extends PureComponent<Props> {
    constructor() {
        super()
        this.addToWishlist = this.addToWishlist.bind(this)
        this.removeFromWishlist = this.removeFromWishlist.bind(this)
        this.onBookStatusChange = this.onBookStatusChange.bind(this)
        this.removeBookFromCollection = this.removeBookFromCollection.bind(this)
    }

    addToWishlist () {
        const { book } = this.props;
        this.props.addBookToWishlist(book)
    }

    removeFromWishlist (id, bookId) {
        const { userId } = this.props;
        this.props.removeFromWishlist({ bookId, id, user_id: userId })
    }

    removeBookFromCollection() {
        const { book, userId, collection } = this.props
        this.props.removeBookFromCollection({ id: book.id }, { user_id: userId, count: 0, category: collection })
    }

    onBookStatusChange(e, data) {
        const { book } = this.props;
        this.props.updateBookStatus({
            user_books_id: book.id,
            match_type: data.value
        })
    }

	render () {
		const { book, showWishList, wishlist } = this.props;
		const isWishlisted = wishlist.find((b) => (get(b, 'book.name') === get(book, 'book.name') ))
		return (
            <ItemWrapper verticalAlign='middle'>
                <Grid.Row only={'mobile'} verticalAlign='top'>
                    <Grid.Column width={6}>
                        <Image src={get(book, 'book.image')} />
                    </Grid.Column>
                    <Grid.Column width={10} style={{ fontSize: '1.5rem', position: 'relative' }}>
                        {
                            showWishList ?
                                <Grid.Column style={{ position: 'absolute', right: 0, top: '-2rem' }}>
                                    {
                                        isWishlisted
                                            ? <WishlistBtn onClick={() => this.removeFromWishlist(isWishlisted.id, get(isWishlisted, 'book.id', ''))}><img style={{ width: '1.3rem' }} src={'/images/heartRed.svg'} /></WishlistBtn>
                                            : <WishlistBtn onClick={this.addToWishlist}><img style={{ width: '1.3rem' }} src={'/images/heartOutline.svg'} /></WishlistBtn>
                                    }
                                </Grid.Column> :
                                <Grid.Column width={1} style={{ position: 'relative' }}>
                                    <MobileCloseBtn onClick={this.removeBookFromCollection}><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></MobileCloseBtn>
                                </Grid.Column>
                        }
                        <p style={{ marginBottom: '0.5rem' }}>{get(book, 'book.name')}</p>
                        <p style={{ marginBottom: '0.5rem' }}>{get(book, 'book.authors', []).join(', ')}</p>
                        <p style={{ marginBottom: '0.5rem' }}>Non-fiction</p>
                        <br />
                        <div>
                            {
                                showWishList
                                    ? <p>{get(book, 'match_type')}</p>
                                    : (
                                        <Dropdown
                                            onChange={this.onBookStatusChange}
                                            options={[
                                                {
                                                    key: 'keep',
                                                    text: 'Keep',
                                                    value: 'keep'
                                                },
                                                {
                                                    key: 'exchange',
                                                    text: 'Exchange',
                                                    value: 'swap'
                                                },
                                                {
                                                    key: 'giveaway',
                                                    text: 'Giveaway',
                                                    value: 'giveaway'
                                                },
                                            ]}
                                            defaultValue={get(book, 'match_type')}
                                        />
                                    )
                            }
                        </div>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row only={'tablet computer'}>
                    <Grid.Column width={2}>
                        <Image style={{ width: '6.25rem' }} src={get(book, 'book.image')} />
                    </Grid.Column>
                    <Grid.Column width={14}>
                        <Grid columns={'equal'}>
                            <Grid.Column><p>{get(book, 'book.name')}</p></Grid.Column>
                            <Grid.Column><p>{get(book, 'book.authors', []).join(', ')}</p></Grid.Column>
                            <Grid.Column>
                                {
                                    showWishList
                                    ? <p>{get(book, 'match_type')}</p>
                                        : (
                                            <Dropdown
                                                onChange={this.onBookStatusChange}
                                                options={[
                                                    {
                                                        key: 'keep',
                                                        text: 'Keep',
                                                        value: 'keep'
                                                    },
                                                    {
                                                        key: 'exchange',
                                                        text: 'Exchange',
                                                        value: 'swap'
                                                    },
                                                    {
                                                        key: 'giveaway',
                                                        text: 'Giveaway',
                                                        value: 'giveaway'
                                                    },
                                                ]}
                                                defaultValue={get(book, 'match_type')}
                                            />
                                        )
                                }
                            </Grid.Column>
                            <Grid.Column><p>Non-fiction</p></Grid.Column>
                            {
                                showWishList ?
                                <Grid.Column>
                                    {
                                        isWishlisted
                                        ? <WishlistBtn onClick={() => this.removeFromWishlist(isWishlisted.id, get(isWishlisted, 'book.id', ''))}><img style={{ width: '1.3rem' }} src={'/images/heartRed.svg'} /></WishlistBtn>
                                            : <WishlistBtn onClick={this.addToWishlist}><img style={{ width: '1.3rem' }} src={'/images/heartOutline.svg'} /></WishlistBtn>
                                    }
                                </Grid.Column> :
                                    <Grid.Column width={1}>
                                        <CloseBtn onClick={this.removeBookFromCollection}><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>
                                    </Grid.Column>
                            }
                        </Grid>
                    </Grid.Column>
                </Grid.Row>
            </ItemWrapper>
		)
	}
}

export default BookEntryItem
