import styled from 'styled-components'
import { Segment } from 'semantic-ui-react'
import FormField from 'components/FormField'

export const FormTitle = styled.h3`
    font-size: 1.875rem;
    margin-top: 2rem;
    text-align: center;
`;

export const StyledFormInput = styled(FormField)`
    width: 100%;
`;

export const BtnWrapper = styled(Segment)`
    padding-left: 2.625rem;
`;
