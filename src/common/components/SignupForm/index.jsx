// @flow
import React, {PureComponent} from 'react'
import { Form, Divider, Grid, Segment, Button } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { required, validEmail } from 'common/validators'
import { FormTitle, StyledFormInput, BtnWrapper } from './style';

type Props = {
  handleSubmit: Function,
  loading: Boolean
};

class SignupForm extends PureComponent<Props> {
	render () {
		return (
			<Form onSubmit={this.props.handleSubmit}>
				<FormTitle>Sign up</FormTitle>
				<br />
				<Grid>
					<Grid.Column tablet={5} computer={5} only={'tablet computer'}></Grid.Column>
					<Grid.Column tablet={6} computer={6} mobile={16}>
						<Segment basic padded='very' textAlign={'left'}>
                            <p>Name</p>
                            <Field component={StyledFormInput} validate={required} name='name' />
                            <Divider hidden />
							<p>Email Id</p>
							<Field component={StyledFormInput} validate={[required, validEmail]} name='email' />
							<Divider hidden />
							<p>Password</p>
							<Field type='password' component={StyledFormInput} validate={required} name='password' />
						</Segment>
                        <BtnWrapper basic>
                        	<Button disabled={this.props.loading} primary type='submit'>Next</Button>
						</BtnWrapper>
					</Grid.Column>
                    <Grid.Column tablet={5} computer={5} only={'tablet computer'}></Grid.Column>
				</Grid>
			</Form>
		)
	}
}

export default reduxForm({
	form: 'signupForm'
})(SignupForm)
