// @flow
import React, {PureComponent} from 'react'
import { Form, Header, Input, Divider, Grid, Segment, Button } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { required, validEmail } from 'common/validators'
import {
    FormTitle,
    StyledFormInput,
    SubmitBtn,
    BackButton,
} from './style';

type Props = {
    handleSubmit: Function,
    forgotPassword: Function,
    goBackToLogin: Function,
    loading: Boolean,
}

class ResetPasswordForm extends PureComponent<Props> {
    render () {
        return (
            <Form onSubmit={this.props.handleSubmit}>
                <FormTitle>Reset Password</FormTitle>
                <br />
                <Grid>
                    <Grid.Column tablet={5} computer={5} only={'tablet computer'}></Grid.Column>
                    <Grid.Column tablet={6} computer={6} mobile={16}>
                        <Segment basic padded='very'>
                            <p>OTP</p>
                            <Field component={StyledFormInput} type={'number'} validate={required} name='otp' />
                            <Divider hidden />
                            <p>Password</p>
                            <Field component={StyledFormInput}  type='password' validate={required} name='password' />
                            <Divider hidden />
                            <p>Retry Password</p>
                            <Field component={StyledFormInput}  type='password' validate={required} name='retryPassword' />
                        </Segment>
                        <SubmitBtn disabled={this.props.loading} primary type='submit'>Reset Password</SubmitBtn>
                        <BackButton onClick={this.props.goBackToLogin}>
                            Back&nbsp;<img style={{ width: '0.5rem' }} src={'/images/rightArrow.svg'} />
                        </BackButton>
                    </Grid.Column>
                    <Grid.Column tablet={5} computer={5} only={'tablet computer'}></Grid.Column>
                </Grid>
            </Form>
        )
    }
}

export default reduxForm({
    form: 'resetPasswordForm'
})(ResetPasswordForm)
