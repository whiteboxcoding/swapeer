// @flow
import React, {PureComponent} from 'react'

type Props = {}
class AuthorsCorner extends PureComponent<Props> {
    render () {
        return (
            <iframe
                src="https://docs.google.com/forms/d/16m1hmjqjNavtxE7DA0v3iP4QQQXaAGIggMWdwS5fUW0/viewform?embedded=true"
                width="44.4%"
                height="100%"
                frameBorder="0"
                marginHeight="0"
                marginWidth="0"
                style={{
                    margin: '3% auto',
                    display: 'block',
                }}
            >
                Loading...
            </iframe>
        )
    }
}

export default AuthorsCorner
