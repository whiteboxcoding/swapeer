// @flow
import React, {PureComponent} from 'react'
import { Grid, Button, Dropdown } from 'semantic-ui-react'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import get from 'lodash/get'
import cloneDeep from 'lodash/cloneDeep'
import { CustomeDot, CustomSlider, BookImage } from './style';

function NextArrow(props) {
    const { className, style, onClick } = props;
    const isDisabled = className.includes('slick-disabled');
    isDisabled ? style.opacity = 0.5 : '';
    return (
        <img
            className={className}
            style={{ ...style, width: '5rem', cursor: 'pointer', height: '5rem', right: '0', zIndex: 10 }}
            onClick={onClick}
            src={'/images/rightArrow.svg'}
        />
    );
}

function PrevArrow(props) {
    const { className, style, onClick } = props;
    const isDisabled = className.includes('slick-disabled');
    isDisabled ? style.opacity = 0.5 : '';
    return (
        <img
            className={className}
            style={{ ...style, width: '5rem', cursor: 'pointer', height: '5rem', left: '0', zIndex: 10 }}
            onClick={onClick}
            src={'/images/leftArrow.svg'}
        />
    );
}

type Props = {
    data: Object,
    requestedSwaps: Object,
};

class BookSwapRequestModal extends PureComponent<Props> {
    constructor() {
        super()
        this.state = {
            index: 0,
            matches: []
        };
        this.onSlideChange = this.onSlideChange.bind(this)
        this.requestUserBookSwap = this.requestUserBookSwap.bind(this)
        this.getButtons = this.getButtons.bind(this)
        this.onDropDownChange = this.onDropDownChange.bind(this)
    }
    componentDidMount() {
        const { data } = this.props;
        const { collection = [], wishlist = [] } = data
        const matches = []
        wishlist.forEach((wlObj) => {
            if (collection.length > 0) {
              matches.push({
                  0: collection[0],
                  1: wlObj
              })
            }
        })
        this.setState({
            matches
        })
    }

    onDropDownChange(index, { collection }) {
      const cloneData = cloneDeep(this.state.matches)
      cloneData[index][0] = collection
      this.setState({
        matches: cloneData
      })
    }

    onSlideChange(index) {
        this.setState({
            index
        })
    }

    requestUserBookSwap(obj) {
        const { data } = this.props;
        this.props.requestUserBookSwap({
            user_to: {
                user_book_id: obj[1].user_book_id,
                book: {
                    id: obj[1].id,
                    name: obj[1].name,
                    image: obj[1].image,
                }
            },
            user_from: {
                id: data.id,
                name: data.name,
                profile_pic: data.profile_pic || '',
                user_book_id: obj[0].user_book_id,
                book: {
                    id: obj[0].id,
                    name: obj[0].name,
                    image: obj[0].image,
                }
            }
        })
    }

    getButtons(requestedSwaps, obj) {
        const toBookId = get(obj, `${1}.id`, '')
        const fromBookId = get(obj, `${0}.id`, '')
        const request = requestedSwaps.data.find((swap) => (
            get(swap, 'user_to.book.id', '') === toBookId && get(swap, 'user_from.book.id', '') === fromBookId
        ));
        if (request) {
            if (request.status === 'pending') {
                return (
                    <Button color={'red'} size={'tiny'} style={{ borderRadius: '6.25rem' }}>
                        <b style={{ color: 'black' }}>Request Pending</b>
                    </Button>
                );
            }
            return null
        }
        return (
            <Button onClick={() => this.requestUserBookSwap(obj)} secondary size={'tiny'} style={{ borderRadius: '6.25rem' }}>
                <b style={{ color: 'black' }}>Request Exchange</b>
            </Button>
        )
    }

    render () {
        const settings = {
            dots: true,
            customPaging: function(i) {
                return (
                    <a>
                        <CustomeDot></CustomeDot>
                    </a>
                );
            },
            infinite: false,
            speed: 500,
            draggable: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: this.onSlideChange,
            nextArrow: <NextArrow />,
            prevArrow: <PrevArrow />
        };
        const { data, requestedSwaps } = this.props;
        const { collection = [], wishlist = [] } = data
        const { index, matches } = this.state
        return (
            <div>
                <p style={{ fontSize: '1.25rem', textAlign: 'center', padding: '0 4rem' }}>Swap {data.name}'s {get(matches, `${index}.${0}.name`, '')} with your {get(matches, `${index}.${1}.name`, '')}</p>
                <CustomSlider {...settings}>
                    {
                        matches.map((obj, i) => (
                            <Grid>
                                <Grid.Column mobile={16} tablet={8} computer={8} textAlign={'center'}>
                                    <BookImage style={{ margin: '1rem auto' }} src={obj[0].image} />
                                    <p style={{ fontSize: '1.25rem', marginBottom: 0 }}>{data.name}</p>
                                    <Dropdown scrolling text={<p style={{ fontSize: '1.5rem', textAlign: 'center', marginBottom: 0, textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '22rem' }}>{obj[0].name}</p>}>
                                      <Dropdown.Menu style={{ width: '100%', height: '8rem' }}>
                                        {
                                          collection.map((colObj) =>
                                            <Dropdown.Item onClick={(e, data) => this.onDropDownChange(i, data)} key={colObj.id} collection={colObj} text={colObj.name} />
                                          )
                                        }
                                      </Dropdown.Menu>
                                    </Dropdown>
                                    <p style={{fontSize: '1.25rem'}}>by {get(obj[0], 'authors', []).join(', ')}</p>
                                </Grid.Column>
                                <Grid.Column mobile={16} tablet={8} computer={8} textAlign={'center'}>
                                    <BookImage style={{ margin: '1rem auto' }} src={obj[1].image} />
                                    <p style={{ fontSize: '1.25rem', marginBottom: 0 }}>You</p>
                                    <p style={{ fontSize: '1.5rem', marginBottom: 0, textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden' }}>{obj[1].name}</p>
                                    <p style={{fontSize: '1.25rem'}}>by {get(obj[1], 'authors', []).join(', ')}</p>
                                </Grid.Column>
                                <Grid.Column width={16} textAlign={'center'}>
                                    {this.getButtons(requestedSwaps, obj)}
                                </Grid.Column>
                            </Grid>
                        ))
                    }
                </CustomSlider>
            </div>
        );
    }
}

export default BookSwapRequestModal
