import styled from 'styled-components'
import Slider from "react-slick";

export const CustomeDot = styled.button`
    &:before {
        color: #2CBAE5 !important;
        font-size: 2rem !important;
        opacity: 1;
    }
`;

export const BookImage = styled.img`
  height: 12rem;
`

export const CustomSlider = styled(Slider)`
    padding-bottom: 1rem;
    .slick-slide {
      height: 85% !important;
      margin-bottom: 4rem;
    }
    .slick-dots {
        padding: 2rem 0;
        li.slick-active {
            button:before {
                color: #251086 !important;
                opacity: 1;
            }
        }
        li {
            width: 2.5rem !important;
            button:before {
                opacity: 1;
            }
            button:hover:before, button:active:before, button:focus:before,
            button:hover:after, button:active:after, button:focus:after {
                color: #251086 !important;
                opacity: 1;
            }
        }
    }
`;
