// @flow
import React, {PureComponent} from 'react'
import { Grid, Segment, Header, Button } from 'semantic-ui-react'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { StyledImage, Text, CustomeDot, LetsGoBtn, CustomSlider } from './style';

function NextArrow(props) {
    const { className, style, onClick } = props;
    const isDisabled = className.includes('slick-disabled');
    isDisabled ? style.opacity = 0.5 : '';
    return (
        <img
            className={className}
            style={{ ...style, width: '5rem', cursor: 'pointer', height: '5rem', right: '-4.06rem' }}
            onClick={onClick}
            src={'/images/rightArrow.svg'}
        />
    );
}

function PrevArrow(props) {
    const { className, style, onClick } = props;
    const isDisabled = className.includes('slick-disabled');
    isDisabled ? style.opacity = 0.5 : '';
    return (
        <img
            className={className}
            style={{ ...style, width: '5rem', cursor: 'pointer', height: '5rem', left: '-4.06rem' }}
            onClick={onClick}
            src={'/images/leftArrow.svg'}
        />
    );
}

type Props = {
    closeDemoModal: Function,
};

class DemoCarousel extends PureComponent<Props> {
    constructor() {
        super();
        this.state = {
            showLetsGo: false,
        };
        this.onSlideChange = this.onSlideChange.bind(this)
    }

    onSlideChange(index) {
        if (index === 2) {
            this.setState({
                showLetsGo: true
            })
        } else {
            this.setState({
                showLetsGo: false
            })
        }
    }
    render () {
        const { showLetsGo } = this.state;
        const settings = {
            dots: true,
            customPaging: function(i) {
                return (
                    <a>
                        <CustomeDot></CustomeDot>
                    </a>
                );
            },
            infinite: false,
            speed: 500,
            draggable: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: this.onSlideChange,
            nextArrow: <NextArrow />,
            prevArrow: <PrevArrow />
        };
        return (
            <div style={{ position: 'relative' }}>
                <CustomSlider {...settings}>
                    <div>
                        <Segment basic padded={'very'}>
                            <Header as={'h1'} textAlign={'center'}>How it works?</Header>
                        </Segment>
                        <br />
                        <Grid columns={3}>
                            <Grid.Row only={'tablet computer'}>
                                <Grid.Column>
                                    <StyledImage src={'/images/Collection.gif'} />
                                    <Text>Add books you want<br />to exchange/donate</Text>
                                </Grid.Column>
                                <Grid.Column>
                                    <StyledImage src={'/images/wishlist.gif'} />
                                    <Text>Add books on your<br />wishlist</Text>
                                </Grid.Column>
                                <Grid.Column>
                                    <StyledImage src={'/images/chat.gif'} />
                                    <Text>Fix up a place to<br />meet!</Text>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row only={'mobile'}>
                                <Grid.Column mobile={16}>
                                    <StyledImage src={'/images/Collection.gif'} />
                                    <p style={{ textAlign: 'center', fontSize: '1.5rem', marginTop: '1rem' }}>Add books you want<br />to giveaway</p>
                                </Grid.Column>
                                <Grid.Column mobile={16}>
                                    <StyledImage src={'/images/wishlist.gif'} />
                                    <p style={{ textAlign: 'center', fontSize: '1.5rem', marginTop: '1rem' }}>Add books on your<br /> wishlist</p>
                                </Grid.Column>
                                <Grid.Column mobile={16}>
                                    <StyledImage src={'/images/chat.gif'} />
                                    <p style={{ textAlign: 'center', fontSize: '1.5rem', marginTop: '1rem' }}>Fix up a place to<br />swap!</p>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                    <div>
                        <Segment basic padded={'very'}>
                            <Header as={'h1'} textAlign={'center'}>But, what if I want my book back?</Header>
                        </Segment>
                        <br />
                        <Grid columns={2}>
                            <Grid.Row only={'tablet computer'}>
                                <Grid.Column>
                                    <StyledImage src={'/images/getBackBook.png'} />
                                    <Text>Know the travel journey<br /> of your books!</Text>
                                </Grid.Column>
                                <Grid.Column>
                                    <StyledImage src={'/images/connect.png'} />
                                    <Text>Connect with the one who<br /> has it instantly!</Text>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row only={'mobile'}>
                                <Grid.Column mobile={16}>
                                    <StyledImage src={'/images/getBackBook.png'} />
                                    <p style={{ textAlign: 'center', fontSize: '1.5rem', marginTop: '1rem' }}>Know the travel journey<br /> of your books!</p>
                                </Grid.Column>
                                <Grid.Column mobile={16}>
                                    <StyledImage src={'/images/connect.png'} />
                                    <p style={{ textAlign: 'center', fontSize: '1.5rem', marginTop: '1rem' }}>Connect with the one who<br /> has it instantly!</p>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                    <div>
                        <Segment basic padded={'very'}>
                            <Header as={'h1'} textAlign={'center'}>
                                ReadTreats to bring out the reader in you!
                            </Header>
                        </Segment>
                        <br />
                        <Grid columns={3}>
                            <Grid.Row only={'tablet computer'}>
                                <Grid.Column>
                                    <StyledImage src={'/images/collections.png'} />
                                    <Text>Collections</Text>
                                </Grid.Column>
                                <Grid.Column>
                                    <StyledImage src={'/images/Reviews.png'} />
                                    <Text>Recommendations</Text>
                                </Grid.Column>
                                <Grid.Column>
                                    <StyledImage src={'/images/Habit.png'} />
                                    <Text>Fan Fiction</Text>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row only={'mobile'}>
                                <Grid.Column mobile={16}>
                                    <StyledImage src={'/images/collections.png'} />
                                    <p style={{ textAlign: 'center', fontSize: '1.5rem', marginTop: '1rem' }}>Collections</p>
                                </Grid.Column>
                                <Grid.Column mobile={16}>
                                    <StyledImage src={'/images/Reviews.png'} />
                                    <p style={{ textAlign: 'center', fontSize: '1.5rem', marginTop: '1rem' }}>Recommendations</p>
                                </Grid.Column>
                                <Grid.Column mobile={16}>
                                    <StyledImage src={'/images/Habit.png'} />
                                    <p style={{ textAlign: 'center', fontSize: '1.5rem', marginTop: '1rem' }}>Fan Fiction</p>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                </CustomSlider>
                {
                    showLetsGo &&
                    <LetsGoBtn primary onClick={this.props.closeDemoModal}>Let's go!</LetsGoBtn>
                }
            </div>
        );
    }
}

export default DemoCarousel
