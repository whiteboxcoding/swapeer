import { Grid, Segment, Header, Button } from 'semantic-ui-react'
import styled from 'styled-components'
import Slider from "react-slick";

export const StyledImage = styled.img`
    max-width: 17rem;
    max-height: 28vh;
    margin: 0 auto;
`;

export const Text = styled.p`
    font-size: 1.5rem;
    text-align: center;
    margin-top: 8vh;
    padding-bottom: 8vh;
`;

export const CustomeDot = styled.button`
    &:before {
        color: #2CBAE5 !important;
        font-size: 2rem !important;
        opacity: 1;
    }
`;

export const LetsGoBtn = styled(Button)`
    position: absolute;
    right: 0;
    bottom: 4vh;
`;

export const CustomSlider = styled(Slider)`
    .slick-slide {
      height: auto !important;
    }
    .slick-dots {
        padding: 2rem 0;
        position: inherit;
        li.slick-active {
            button:before {
                color: #251086 !important;
                opacity: 1;
            }
        }
        li {
            width: 2.5rem !important;
            button:before {
                opacity: 1;
            }
            button:hover:before, button:active:before, button:focus:before,
            button:hover:after, button:active:after, button:focus:after {
                color: #251086 !important;
                opacity: 1;
            }
        }
    }
`;
