export const data = {
	headerImage: 'http://localhost:4000/images/1560583188699imagegovind.jpeg',
	tabs: [
		{
			name: 'Collection'
		},
		{
			name: 'Wishlist'
		},
		{
			name: 'Where is my book ?'
		},
		{
			name: 'Testimonials'
		},
		{
			name: 'Follows'
		}
	],
	menuRightButton: [
		{
			name: 'Edit Profile',
			link: '/'
		}
	],
}
