// @flow
import React, {PureComponent} from 'react'
import { Button, Image, Menu, Card, Grid } from 'semantic-ui-react'
import {NavLink} from 'react-router-dom'
import './Dashboard.scss'
import { data } from './data'

type Props = {
}

class Dashboard extends PureComponent<Props> {
	render () {
		return (
			<div>
				<img src='http://localhost:4000/images/1560583188699imagegovind.jpeg' />
				<Image src={data.headerImage} circular />
				<Menu pointing secondary>
					{
						data.tabs.map((menuObj, key) =>
							<Menu.Item key={key} name={menuObj.name} active />
						)
					}
					{
						data.menuRightButton.map((buttonObj, key) =>
							<Menu.Item key={key} position='right'>
								<Button size='tiny' basic>{buttonObj.name}</Button>
							</Menu.Item>
						)
					}
				</Menu>
				<Grid centered container>
					{
						data.collections.map((collection, key) =>
							<Grid.Column tablet={4} computer={4} mobile={8} key={key}>
								<NavLink to={collection.link}>
									<Card>
										<Image src={collection.image} />
										<Card.Content>
											<Card.Description>{collection.text}</Card.Description>
										</Card.Content>
									</Card>
								</NavLink>
							</Grid.Column>
						)
					}
				</Grid>
			</div>
		)
	}
}

export default Dashboard
