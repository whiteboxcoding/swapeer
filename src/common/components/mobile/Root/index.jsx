// @flow
import React, {Component} from 'react'
import {Provider} from 'react-redux'
import Helmet from 'react-helmet'
import RoutingWrapper from 'components/RoutingWrapper'
import App from 'containers/mobile/App'

type Props = {
	store: Object,
	SSR: {
		location?: Object,
		context?: Object
	},
	history: any
}

const Router = process.env.BROWSER
	? require('react-router-redux').ConnectedRouter
	: require('react-router').StaticRouter

class MobileRoot extends Component<Props> {
	static defaultProps = {
		SSR: {}
	}

	render () {
		if (this.context.asyncBootstrapPhase) {
			return null
		}
		const {SSR, store, history} = this.props
		const routerProps = process.env.BROWSER ? {history} : {location: SSR.location, context: SSR.context}

		return (
			<Provider store={store} key={Date.now()}>
				<Router {...routerProps}>
					<div className={'moreSpecific'}>
						<div className={'reallySpecific'}>
							<div className={'extraSpecific'}>
								<div className={'moreExtraSpecific'}>
									<App>
										<Helmet>
											<html lang='en' />
											<meta charSet="utf-8" />
											<title>Readsnet</title>
											<meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
											<meta
												name="description"
												content="Ultimate universal starter with lazy-loading, SSR and i18n"
											/>
											<meta name="theme-color" content="#1b1e2f" />
											<meta name="viewport" content="width=device-width, initial-scale=1.0" />
											<base href="/" />

											<meta name="msapplication-tap-highlight" content="no" />
											<link rel="manifest" href="manifest.json" />
											<noscript
												dangerouslySetInnerHTML={{
													__html: `You are using outdated browser. You can install modern browser here:
													<a href="http://outdatedbrowser.com/">http://outdatedbrowser.com</a>.`
												}}
											/>
											<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnzrOlBkxCfK2ih4JQGzQ-YrgrH8ut1jY&libraries=places"></script>
										</Helmet>
										<RoutingWrapper />
									</App>
								</div>
							</div>
						</div>
					</div>
				</Router>
			</Provider>
		)
	}
}

export default MobileRoot
