import styled from 'styled-components'

export const BookTitle = styled.p`
    font-size: 2.25rem;
`;

export const Description = styled.p`
    font-size: 1.8rem;
`;

export const BookImage = styled.img`
    max-width: 13.75rem;
    max-height: 21.25rem;
`;
