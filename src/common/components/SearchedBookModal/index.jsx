// @flow
import React, {PureComponent} from 'react'
import get from 'lodash/get'
import { Grid, Button, Radio } from 'semantic-ui-react'
import { BookTitle, Description, BookImage } from './style'
import StarRating from "../StarRating";
import AddCollectionForm from '../AddCollectionForm';

type Props = {
    data: Object,
    userId: String,
    onCollectionFormSubmit: Function,
    getUserCollection: Function,
    setCollectionInputVal: Function,
    collections: Array,
    onBookTypeChange: Function,
    type: String,
}

class SearchedBookModal extends PureComponent<Props> {
    render () {
        const { data } = this.props;
        return (
            <Grid columns={2} verticalAlign={'middle'}>
                <Grid.Column>
                    <BookTitle>{get(data, 'name')}</BookTitle>
                    <p><b>Authors:</b> {get(data, 'authors', []).join(', ')}</p>
                    <StarRating rating={get(data, 'averageRating', 0)} />
                    <p><b>Description:</b> {get(data, 'description', '')}</p>
                    <p><b>Category:</b> {get(data, 'categories', []).join(', ')}</p>
                    <br />
                    <Radio
                        label='Collection'
                        value='collection'
                        checked={this.props.type === 'collection'}
                        onChange={this.props.onBookTypeChange}
                    />&nbsp;&nbsp;
                    <Radio
                        label='Wishlist'
                        value='wishlist'
                        checked={this.props.type === 'wishlist'}
                        onChange={this.props.onBookTypeChange}
                    />
                    <br />
                    <br />
                    {
                        (this.props.type === 'collection') &&
                        <AddCollectionForm
                            onSubmit={this.props.onCollectionFormSubmit}
                            userId={this.props.userId}
                            getUserCollection={this.props.getUserCollection}
                            setCollectionInputVal={this.props.setCollectionInputVal}
                            collections={this.props.collections}
                        />
                    }
                </Grid.Column>
                <Grid.Column textAlign={'center'}>
                    <BookImage src={get(data, 'images.thumbnail', '')} />
                </Grid.Column>
            </Grid>
        )
    }
}

export default SearchedBookModal
