import { Segment, Input, Button, Grid } from 'semantic-ui-react'
import styled from 'styled-components'

export const Text = styled.p`
    font-size: 1.5rem;
`;

export const ActionBtn = styled(Button)`
    border-radius: 6.25rem;
    min-width: 10rem;
    background: #2CBAE5;
`;

export const Wrapper = styled(Grid.Row)`
    border: 1px solid #707070;
    padding: 1.5rem;
`;
