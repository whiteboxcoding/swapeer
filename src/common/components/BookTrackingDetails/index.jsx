// @flow
import React, {PureComponent} from 'react'
import { Grid, Header, Divider, Button, Image } from 'semantic-ui-react'
import { Text, ActionBtn, Wrapper } from './style'
import config from '../../../../config/environments'

type Props = {
  bookHistory: Array,
}

class BookTrackingDetails extends PureComponent<Props> {
    render () {
      const { bookHistory } = this.props
        return (
            <Grid>
                <Grid.Column width={2}></Grid.Column>
                <Grid.Column width={11}>
                    <Grid centered columns={2}>
                        {
                          bookHistory.map((obj) =>
                            <Wrapper key={Math.random()} verticalAlign={'middle'}>
                                <Grid.Column width={10}>
                                    <Text>{get(obj, 'user_from.name', '')} has swapped his '{get(obj, 'user_from.book.name', '')}'
                                        with your book '{get(obj, 'user_to.book.name', '')}' that {get(obj, 'user_to.name', '')} had.</Text>
                                </Grid.Column>
                                <Grid.Column width={2}></Grid.Column>
                                <Grid.Column width={4} textAlign={'center'}>
                                    <ActionBtn><b>Message {get(obj, 'user_from.name', '')}</b></ActionBtn>
                                    <br />
                                    <br />
                                    <ActionBtn><b>View Profile</b></ActionBtn>
                                </Grid.Column>
                            </Wrapper>
                          )
                        }
                    </Grid>
                </Grid.Column>
                <Grid.Column width={3} textAlign={'right'}>
                    <Image style={{ margin: '-1.5rem 2rem 1rem auto', width: '10rem' }} src={`${config.frontendBase}/images/156058415791610920906101529838879574222704025005198393863n.jpg`} circular />
                </Grid.Column>
            </Grid>
        )
    }
}

export default BookTrackingDetails
