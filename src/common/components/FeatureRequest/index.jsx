import React, {PureComponent} from 'react'

class FeatureRequest extends PureComponent<Props> {
    render () {
        return (
            <iframe
                src="https://docs.google.com/forms/d/e/1FAIpQLSc73gsFOCJ092_AwPbjfp3cmJ638gZGfRKztWAtArl5bGcgdw/viewform?embedded=true"
                width="44.4%"
                height="1241"
                frameBorder="0"
                marginHeight="0"
                marginWidth="0"
                style={{
                    margin: '3% auto',
                    display: 'block',
                }}
            >
                Loading...
            </iframe>
        )
    }
}

export default FeatureRequest
