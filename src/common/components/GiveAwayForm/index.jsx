// @flow
import React, {PureComponent} from 'react'
import { Form, Button } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import get from 'lodash/get'
import { required } from '../../validators'
import FormField from '../FormField'
import FormDropDown from '../FormDropDown'

type Props = {
    handleSubmit: Function,
    getUserGivAwayList: Function,
    getUsersList: Function,
    userId: String,
    userBooksList: Object,
};

class GiveAwayForm extends PureComponent<Props> {
    constructor() {
        super()
        this.onSwapBookSearch = this.onSwapBookSearch.bind(this)
        this.onUserSearch = this.onUserSearch.bind(this)
    }

    onSwapBookSearch(e, data) {
        this.props.getUserBooksList({
            user_id: this.props.userId,
            count: 0,
            type: 'giveaway',
            text: data.searchQuery
        })
    }

    onUserSearch(e, data) {
        this.props.getUsersList({
            count: 0,
            text: data.searchQuery
        })
    }

    render () {
        const { userBooksList, searchedUser, userId } = this.props;
        return (
            <Form onSubmit={this.props.handleSubmit}>
                <p style={{ fontSize: '1.5rem' }}>Reader's name:</p>
                <Field
                    className="formInput"
                    size='large'
                    component={FormDropDown}
                    validate={required}
                    name='user_to.id'
                    search
                    fluid
                    selection
                    onSearchChange={this.onUserSearch}
                    options={searchedUser.map(user => ({
                        key: user.name,
                        text: user.name,
                        value: user.id
                    }))}
                />
                <p style={{ fontSize: '1.5rem' }}>Name of the book:</p>
                <Field
                    className="formInput"
                    size='large'
                    component={FormDropDown}
                    validate={required}
                    name='user_from.book.id'
                    search
                    fluid
                    selection
                    onSearchChange={this.onSwapBookSearch}
                    options={get(userBooksList, `data.${userId}`, []).map(book => ({
                        key: get(book, 'book.id'),
                        text: get(book, 'book.name'),
                        value: get(book, 'book.id')
                    }))}
                />
                <br />
                <Button size='large' secondary style={{ float: 'right' }}>
                    <b style={{ color: 'black' }}>Submit</b>
                </Button>
                <br />
            </Form>
        )
    }
}

export default reduxForm({
    form: 'giveAwayForm'
})(GiveAwayForm)
