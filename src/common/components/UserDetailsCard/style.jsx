import styled from 'styled-components'
import { Image, Button } from 'semantic-ui-react'

export const BackgroundImg = styled.div`
    height: 7.5rem;
    background-image: ${(props) => (
        props.url
            ? `url('${props.url}')`
            : ''
    )};
    
    background-color: ${(props) => (
        props.url
            ? ''
            : '#938ac4'
    )};
    background-size: 100%;
    background-position-y: center;
`;

export const ProfileImage = styled(Image)`
    width: 5.5rem;
    position: relative;
    top: 4.5rem;
    left: 0.5rem;
`;

export const Wrapper = styled.div`
    max-width: 22rem;
    border: 1px solid #c1c1c1;
`;

export const FollowButton = styled(Button)`
    color: black;
    border-radius: 6.25rem;
`
