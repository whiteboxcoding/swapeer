// @flow
import React, {PureComponent} from 'react'
import { Image, Grid, Button } from 'semantic-ui-react'
import get from 'lodash/get'
import { BackgroundImg, ProfileImage, Wrapper, FollowButton } from './style'

type Props = {
    userData: Object,
    unFollowUser: Function,
}

class UserDetailsCard extends PureComponent<Props> {
    constructor() {
        super()
        this.unFollowUser = this.unFollowUser.bind(this)
    }

    unFollowUser(id) {
        this.props.unFollowUser({
            following_id: id,
            follow: false
        })
    }

    render () {
        const { userData = {}, unFollowUser } = this.props
        return (
            <Wrapper>
                <BackgroundImg coverPic={userData.cover_pic}>
                    <ProfileImage circular src={userData.profile_pic} />
                </BackgroundImg>
                <div style={{ marginTop: '1rem' }}>
                    <Grid>
                        <Grid.Column width={10} style={{ padding: '3rem 0rem 1rem 2rem' }}>
                            <p>{userData.name}</p>
                            <small>{get(userData, 'user_profile.blog_link', '')}</small><br />
                            <small>{get(userData, 'user_profile.book_read_story', '')}</small><br />
                            <small>{get(userData, 'user_profile.favourite_genres', '')}</small>
                        </Grid.Column>
                        <Grid.Column width={6}>
                            {
                                unFollowUser &&
                                <FollowButton onClick={() => this.unFollowUser(userData.id)} color={'twitter'} size={'mini'}>Following</FollowButton>
                            }
                        </Grid.Column>
                    </Grid>
                </div>
            </Wrapper>
        )
    }
}

export default UserDetailsCard
