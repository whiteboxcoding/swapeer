import { Header } from 'semantic-ui-react'
import styled from 'styled-components'

export const TopHeader  = styled(Header)`
    padding: 0 4rem 4rem;
    button {
        float: right;
        color: black;
    }
`;
