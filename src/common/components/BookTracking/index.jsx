// @flow
import React, {PureComponent} from 'react'
import { Grid, Header, Divider, Button, Modal, Segment } from 'semantic-ui-react'
import BookTrackingDetails from '../BookTrackingDetails';
import SwapRequestModal from '../SwapRequestModal';
import { TopHeader } from './style'
import { getModalMountNode } from '../../helpers/functions'
import { CloseBtn } from '../../styles/common'
import { NavLink } from 'react-router-dom'
import get from 'lodash/get'

type Props = {
    queryObj: Object,
    userId: String,
    getUserBooksList: Function,
    getUsersList: Function,
    userBooksList: Object,
    searchedUser: Array,
    requestUserBookSwap: Function,
    requestForGiveaway: Function,
    getUserBookHistory: Function,
    searchedBooks: Object,
    userBookHistory: Array
}

class BookTracking extends PureComponent<Props> {
    constructor() {
        super()
        this.state = {
            openSwapModal: false
        }
        this.openSwapModal = this.openSwapModal.bind(this)
        this.closeSwapModal = this.closeSwapModal.bind(this)
        this.onBookSwapRequest = this.onBookSwapRequest.bind(this)
        this.onGiveAwayRequest = this.onGiveAwayRequest.bind(this)
    }

    componentDidMount() {
        const { userId } = this.props;
        this.props.getUserBookHistory({
            user_id: userId,
            count: 0
        })
    }

    onBookSwapRequest(data) {
        this.closeSwapModal()
        this.props.requestUserBookSwap(data)
    }

    onGiveAwayRequest(data) {
        this.closeSwapModal()
        this.props.requestForGiveaway(data)
    }

    openSwapModal() {
        this.setState({
            openSwapModal: true
        })
    }

    closeSwapModal() {
        this.setState({
            openSwapModal: false
        })
    }

    render () {
        const { queryObj, userId, userBooksList, searchedUser, userBookHistory } = this.props;
        const { openSwapModal } = this.state
        const currentBookData = (queryObj.book) ? get(userBookHistory, 'data', []).find((obj) => (obj.book.id === queryObj.book)) : {}
        return (
            <div>
                {
                    queryObj.book
                        ? <div>
                            <TopHeader as={'h2'} textAlign={'center'}>
                                Track where your book is travelling across the globe!
                                <Modal
                                    size={'tiny'}
                                    centered
                                    mountNode={getModalMountNode()}
                                    open={openSwapModal}
                                    onClose={this.closeSwapModal}
                                    closeIcon={<CloseBtn><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>}
                                    trigger={<Button secondary onClick={this.openSwapModal}><b>Add swap</b></Button>}
                                    content={
                                        <SwapRequestModal
                                            getUsersList={this.props.getUsersList}
                                            userBooksList={userBooksList}
                                            userId={userId}
                                            searchedUser={searchedUser}
                                            searchedBooks={this.props.searchedBooks}
                                            getUserBooksList={this.props.getUserBooksList}
                                            requestUserBookSwap={this.onBookSwapRequest}
                                            requestForGiveaway={this.onGiveAwayRequest}
                                        />
                                    }
                                />
                            </TopHeader>
                            <Divider hidden section />
                            <BookTrackingDetails bookHistory={currentBookData.history || []} />
                        </div>
                        : (
                            <div>
                              {
                                (get(userBookHistory, 'data', []).length > 0)
                                 ? (
                                   <div>
                                     <Header as={'h2'} textAlign={'center'}>
                                         Know where your book is travelling across the globe!
                                     </Header>
                                     <Divider hidden section />
                                     <Grid centered container columns={5}>
                                       {
                                         userBookHistory.data.map((obj) =>
                                               <Grid.Column padded={'vertically'} key={obj.id}>
                                                   <NavLink to={`/home/bookTracking/${obj.user_id}?book=${obj.book.id}`}>
                                                       <img src={obj.book.image} />
                                                   </NavLink>
                                               </Grid.Column>
                                         )
                                       }
                                     </Grid>
                                   </div>
                                 ) : (
                                   <Segment basic padded='very'>
                                       <Segment textAlign={'right'} basic>
                                           <Modal
                                               size={'tiny'}
                                               centered
                                               mountNode={getModalMountNode()}
                                               open={openSwapModal}
                                               onClose={this.closeSwapModal}
                                               closeIcon={<CloseBtn><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>}
                                               trigger={<Button secondary onClick={this.openSwapModal}><b>Add swap</b></Button>}
                                               content={
                                                   <SwapRequestModal
                                                       getUsersList={this.props.getUsersList}
                                                       userBooksList={userBooksList}
                                                       userId={userId}
                                                       searchedUser={searchedUser}
                                                       searchedBooks={this.props.searchedBooks}
                                                       getUserBooksList={this.props.getUserBooksList}
                                                       requestUserBookSwap={this.onBookSwapRequest}
                                                       requestForGiveaway={this.onGiveAwayRequest}
                                                   />
                                               }
                                           />
                                       </Segment>
                                     <Header as={'h3'} textAlign={'center'}>
                                         Looks like you do not have any book to track. Please exchange or donate any book in your collection with your friends :)
                                     </Header>
                                   </Segment>
                                 )
                              }
                            </div>
                        )
                }
            </div>
        )
    }
}

export default BookTracking
