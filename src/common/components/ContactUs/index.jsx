// @flow
import React, {PureComponent} from 'react'

class ContactUs extends PureComponent<Props> {
    render () {
        return (
            <iframe
                src="https://docs.google.com/forms/d/e/1FAIpQLSf9P2tX-lWd2uLUugw4GA6pcrYzEzBbjdyw2Gcwi-p7YIN6cg/viewform?embedded=true"
                width="44.4%"
                height="100%"
                frameBorder="0"
                marginHeight="0"
                marginWidth="0"
                style={{
                    margin: '3% auto',
                    display: 'block',
                }}
            >
                Loading...
            </iframe>
        )
    }
}

export default ContactUs
