import styled from 'styled-components'
import { Button } from 'semantic-ui-react'

export const UploadButton = styled(Button)`
    width: 8.18rem;
    height: 10.69rem;
`;

export const HiddenInput = styled.input`
    display: none;
`;
