// @flow
import React, {PureComponent} from 'react'
import { Form, Grid, Input, Segment, Button, Icon } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { required } from 'common/validators'
import FormField from '../FormField'
import FormDropDown from '../FormDropDown'
import UserCollectionDropdown from '../UserCollectionDropdown'
import { dataURItoBlob } from '../../helpers/functions';
import { UploadButton, HiddenInput } from './style';
import {genresList} from "../../constants";

type Props = {
	handleSubmit: Function,
    saveUploadedFile: Function,
    bookImage: Object,
    getUserCollection: Function,
    collections: Array,
    setCollectionInputVal: Function,
    userId: String,
    viewType: String,
}

class AddBookForm extends PureComponent<Props> {
	constructor () {
		super()
		this.state = {
			url: ''
		};
		this.readImageFile = this.readImageFile.bind(this)
		this.imageView = this.imageView.bind(this);
	}
	handleImageUpload (e) {
		e.preventDefault()
		document.getElementById('fileInput').click()
	}
	readImageFile (e, el) {
		const { files } = e.currentTarget;
        const file = files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        const self = this;
        reader.onloadend = () => {
            const img = new Image;
            img.onload = function() {
                const cvs = document.createElement('canvas');
                const ratio = img.width / img.height;
                cvs.width = Math.min(img.width, 500);
                cvs.height = cvs.width / ratio;
                const ctx = cvs.getContext('2d');
                ctx.drawImage(img, 0, 0, cvs.width, cvs.height);
                const newImageData = cvs.toDataURL('image/jpeg', 0.7);
                self.setState({
                    url: newImageData,
                });
                const blob = dataURItoBlob(newImageData);
                const fileObj = new File([blob], file.name);
                self.props.saveUploadedFile({ 0: fileObj });
            };
            img.src = reader.result;
        };
	}

	imageView(localImage, uploadedImage) {
		if (uploadedImage && uploadedImage.url) {
			return <img src={uploadedImage.url} style={{ width: '8.18rem' }} />
		}
		if (localImage) {
			return <img style={{ opacity: 0.5 }} src={localImage} style={{ width: '8.18rem' }} />
		}
        return (
            <UploadButton size='large' color='black' basic onClick={this.handleImageUpload}>
                Add Image
            </UploadButton>
		)
	}

	render () {
		const { bookImage, viewType } = this.props;
		return (
			<Form onSubmit={this.props.handleSubmit}>
				<Grid columns={2}>
					<Grid.Column>
						<Field
							component={FormField}
							validate={required}
							as={Form.Field}
							control={Input}
							label='Book Name'
							name='book.name'
						/>
                        <Field
                            component={FormField}
                            validate={required}
                            as={Form.Field}
                            control={Input}
                            label='Description'
                            name='book.description'
                        />
						<Field
							component={FormField}
							validate={required}
							as={Form.Field}
							control={Input}
							label='Author Name'
							name='book.authors.0'
						/>
						<label style={{ fontSize: '0.92857143em;', fontWeight: 'bold' }}>Genre</label>
                        <Field
							fluid
                            component={FormDropDown}
                            name='book.genres.0'
                            multiple
                            search
                            selection
                            options={genresList}
                        />
						<Field
							component={FormField}
							as={Form.Field}
							control={Input}
							label='ISBN No.'
							name='book.isbn10'
						/>
						{
							(viewType !== 'wishlist') &&
                            <UserCollectionDropdown
                                getUserCollection={this.props.getUserCollection}
                                setCollectionInputVal={this.props.setCollectionInputVal}
                                collections={this.props.collections}
                                userId={this.props.userId}
								form={'addBookForm'}
                            />
						}
					</Grid.Column>
					<Grid.Column verticalAlign='middle' textAlign='center'>
						{this.imageView(this.state.url, bookImage)}
						<HiddenInput id='fileInput' type='file' onChange={this.readImageFile} />
					</Grid.Column>
				</Grid>
				<Segment textAlign='right' basic >
					{/*<Button type='submit' secondary>Buy</Button>*/}
					<Button type='submit' primary>Add</Button>
				</Segment>
			</Form>
		)
	}
}

export default reduxForm({
	form: 'addBookForm'
})(AddBookForm)
