// @flow
import React, {PureComponent} from 'react'
import { Form, Header, Input, Divider, Grid, Segment, Button } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { required, validEmail } from 'common/validators'
import {
    FormTitle,
    StyledFormInput,
    SubmitBtn,
    ForgotPassword,
} from './style';

type Props = {
  handleSubmit: Function,
  forgotPassword: Function,
  loading: Boolean,
}

class SigninForm extends PureComponent<Props> {
	render () {
		return (
			<Form onSubmit={this.props.handleSubmit}>
				<FormTitle>Sign in</FormTitle>
				<br />
				<Grid>
					<Grid.Column tablet={5} computer={5} only={'tablet computer'}></Grid.Column>
					<Grid.Column tablet={6} computer={6} mobile={16}>
						<Segment basic padded='very'>
							<p>Email Id</p>
							<Field component={StyledFormInput} validate={[required, validEmail]} name='email' />
							<Divider hidden />
							<p>Password</p>
							<Field component={StyledFormInput}  type='password' validate={required} name='password' />
						</Segment>
                        <SubmitBtn disabled={this.props.loading} primary type='submit'>Next</SubmitBtn>
						<ForgotPassword onClick={this.props.forgotPassword}>Forgot Password</ForgotPassword>
					</Grid.Column>
                    <Grid.Column tablet={5} computer={5} only={'tablet computer'}></Grid.Column>
				</Grid>
			</Form>
		)
	}
}

export default reduxForm({
	form: 'signinForm'
})(SigninForm)
