import styled from 'styled-components'
import FormField from 'components/FormField'
import { Button } from 'semantic-ui-react'

export const FormTitle = styled.h3`
    font-size: 1.875rem;
    margin-top: 2rem;
    text-align: center;
`;

export const StyledFormInput = styled(FormField)`
    width: 100%;
`;

export const SubmitBtn = styled(Button)`
    margin-left: 2.625rem;
`;

export const ForgotPassword = styled.button`
    float: right;
    background: white;
    border: none;
    color: #251086;
    text-decoration: underline;
    font-size: 0.75rem;
    /* font-weight: 600; */
    margin-right: 3rem;
    cursor: pointer;
    outline: none;
`;
