import styled from 'styled-components'
import { Grid, Dropdown } from 'semantic-ui-react'

export const TextWrapper = styled(Grid.Column)`
    border: 1px solid #707070;
`;

export const ItemWrapper = styled(Grid)`
    margin: 2rem 0 !important;
`;

export const StyledDropdown = styled(Dropdown)`
    font-size: 1.3rem;
    .visible.menu {
        background: #251086;
        .item {
            font-size: 1.5rem;
            color: white;
        }
        .item.selected {
            color: #2CBAE5;
        }
    }
`;
