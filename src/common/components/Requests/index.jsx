// @flow
import React, {PureComponent} from 'react'
import { List, Image, Segment, Grid, Icon, Button } from 'semantic-ui-react'
import { TextWrapper, ItemWrapper, StyledDropdown } from './style'
import NoDataFoundScreen from '../NoDataFoundScreen'
import DataLoadingScreen from '../DataLoadingScreen'
import get from 'lodash/get'

type Props = {
    params: Object,
	getUserRequestsList: Function,
    updateRequestStatus: Function,
	updateBookStatus: Function,
    requests: Object,
}

class Requests extends PureComponent<Props> {
	constructor() {
		super()
		this.getRequestText = this.getRequestText.bind(this)
		this.acceptRequest = this.acceptRequest.bind(this)
		this.rejectRequest = this.rejectRequest.bind(this)
		this.getActionButtons = this.getActionButtons.bind(this)
		this.getComponentView = this.getComponentView.bind(this)
	}
	componentDidMount() {
        const { params } = this.props;
		    this.props.getUserRequestsList({
                user_id: params.userId,
                count: 0
            });
	}

    getRequestText(request) {
		if (request.request_type === 'swap') {
			return <span style={{ fontSize: '1.5rem' }}>
				has requested your <span style={{ color: '#2CBAE5' }}>{get(request, 'user_from.book.name', '')}</span> in return of his&nbsp;
				<span style={{ color: '#2CBAE5' }}>{get(request, 'user_to.book.name', '')}</span>
			</span>
		}else if (request.request_type === 'giveaway') {
            return <span style={{ fontSize: '1.5rem' }}>
				has shown interest in your donation of the book <span style={{ color: '#2CBAE5' }}>{get(request, 'user_from.book.name', '')}</span>
			</span>
		}
	}

    getActionButtons(request) {
		const options = [
            {
                text: 'Book received',
                value: 'Book received',
                key: '1',
				onClick: () => this.props.updateBookStatus({
                    request_id: request.id,
                    book_status: 'Book received'
                })
            }
        ];
		if (request.request_type === 'giveaway') {
            options.push({
                text: 'Book not received',
                value: 'Book not received',
                key: '2',
                onClick: () => this.props.updateBookStatus({
                    request_id: request.id,
                    book_status: 'Book not received'
                })
            })
		} else {
            options.push({
                text: 'Book not exchanged',
                value: 'Book not exchanged',
                key: '2',
                onClick: () => this.props.updateBookStatus({
                    request_id: request.id,
                    book_status: 'Book not exchanged'
                })
            })
		}
		switch (request.status) {
			case 'accepted':
				return (
					<Grid.Column mobile={15} tablet={4} computer={4} textAlign={'right'}>
                        {
                            request.book_status
                            ? (
                                    <StyledDropdown
                                        inline
                                        options={options}
                                        defaultValue={request.book_status || 'Book not received'}
                                    />
                            ) : null
                        }

					</Grid.Column>
				);
			case 'rejected':
					return <Grid.Column mobile={15} tablet={4} computer={4} textAlign={'right'}></Grid.Column>;
			default:
				return (
                    <Grid.Column mobile={15} tablet={4} computer={4} textAlign={'right'}>
                        <Button size='tiny' secondary onClick={() => this.acceptRequest(request.id)}>
                            <b style={{ color: 'black' }}>Accept</b>
                        </Button>
                        <Button size='tiny' color={'red'} onClick={() => this.rejectRequest(request.id)}>
                            <b style={{ color: 'black' }}>Reject</b>
                        </Button>
                    </Grid.Column>
				)
		}
	}

    acceptRequest(requestId) {
      const { params } = this.props;
  		this.props.updateRequestStatus({
  				request_id: requestId,
  				status: "accepted",
          user_id: params.userId,
      })
  	}

    rejectRequest(requestId) {
      const { params } = this.props;
        this.props.updateRequestStatus({
          request_id: requestId,
          status: "rejected",
          user_id: params.userId,
        })
	}

	getComponentView(requests) {
        if (get(requests, 'data', []).length === 0 && requests.status !== 'fetched') {
            return (
                <DataLoadingScreen text={'Checking for request for your collection'} />
            )
        }
        if (get(requests, 'data', []).length === 0) {
            return (
                <NoDataFoundScreen
                    text={'Looks like you didn\'t get any request for exchange or donate :('}
                    buttonText={'Invite friends to join for more books!'}
                />
            )
        }
		return (
				  <Segment padded basic>
					  {
						  get(requests, 'data', []).map((request) =>
							  <ItemWrapper key={request.id} verticalAlign='middle'>
								  <Grid.Column mobile={4} tablet={2} computer={2}>
									  <Image circular src={get(request, 'user_to.profile_pic', '')} />
								  </Grid.Column>
								  <TextWrapper mobile={12} tablet={14} computer={14}>
									  <p style={{ margin: 0, display: 'inline-block', fontSize: '1.5rem', color: '#251086' }}>{get(request, 'user_to.name', '')}</p>
									  &nbsp;&nbsp;{this.getRequestText(request)}
									  <Grid padded verticalAlign={'bottom'}>
										  <Grid.Column mobile={16} tablet={12} computer={12}>
											  {
												  request.message &&
												  <p style={{ padding: '1rem', border: '1px solid grey' }}>{request.message}</p>
											  }
											  {
												  request.request_type === 'swap' &&
												  <p></p>
											  }
										  </Grid.Column>
										  {
											  this.getActionButtons(request)
										  }
									  </Grid>
								  </TextWrapper>
							  </ItemWrapper>
						  )
					  }
				  </Segment>
		)
	}

	render () {
		const { requests } = this.props;
		return (
			this.getComponentView(requests)
		)
	}
}

export default Requests
