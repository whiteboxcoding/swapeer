export const required = value => value ? undefined : '* Required field'
export const validEmail = value => {
    if (value && value.length >= 50) {
        return 'Must be less than 50 characters';
    }
    if (value && /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
        return undefined;
    } else {
        return 'Invalid email address';
    }
};

export const length = size => value =>
    value && value.length && value.length !== size
        ? `Must be ${size} characters`
        : undefined;
export const length10 = length(10);
