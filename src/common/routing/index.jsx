// @flow
import React from 'react'
import {createBrowserHistory, createMemoryHistory} from 'history'
import {asyncComponent} from 'react-async-component'
import {Loader, Dimmer, Header, Icon} from 'semantic-ui-react'
import App from 'containers/App'
import LoginPage from 'containers/LoginPage'
import HomePage from 'containers/HomePage'
import Swaps from 'containers/Swaps'
import PrivacyPolicy from '../components/PrivacyPolicy'
import ContactUs from '../components/ContactUs'
import AuthorsCorner from '../components/AuthorsCorner'
import FeatureRequest from '../components/FeatureRequest'
import pick from 'lodash/pick'

function asyncComponentCreator (url) {
	return asyncComponent({
		resolve: () => {
			if (!process.env.BROWSER) {
				// flow-disable-next-line
				return require(`containers/${url}/index.jsx`).default
			}
			// flow-disable-next-line: The parameter passed to import() must be a literal string
			return import(/* webpackChunkName:"[index].[request]" */ `containers/${url}/index.jsx`)
		},
		LoadingComponent () {
			return (
				<Dimmer active>
					<Loader size="large" active>
						Loading page...
					</Loader>
				</Dimmer>
			)
		},
		ErrorComponent () {
			return (
				<Dimmer active>
					<Header inverted as="h2" icon textAlign="center">
						<Icon name="refresh" />
						Refresh
						<Header.Subheader>Got error while loading page.</Header.Subheader>
					</Header>
				</Dimmer>
			)
		},
		autoResolveES2015Default: true,
		env: process.env.BROWSER ? 'browser' : 'node',
		serverMode: 'resolve'
	})
}

function routingFnCreator (useFor) {
	// const AsyncNotFound = asyncComponentCreator('NotFound')
	// Dashboard and Links included in build
	// NotFound(404) is lazy
	const routes: any[] = [
		{
			path: '/login',
			exact: true,
			component: LoginPage,
			name: 'LoginPage'
		},
		{
			path: '/signup',
			exact: true,
			component: LoginPage,
			name: 'LoginPage'
		},
		{
			path: '/home/:viewType/:userId',
			exact: true,
			component: HomePage,
			name: 'HomePage'
		},
        {
            path: '/home',
            exact: true,
            component: HomePage,
            name: 'HomePage'
        },
		{
			path: '/swaps/:userId',
			exact: true,
			component: Swaps,
			name: 'Swaps'
		},
		{
            path: '/authors-corner',
            exact: true,
            component: AuthorsCorner,
            name: 'AuthorsCorner'
        },
		{
			path: '/privacy/policy',
            exact: true,
            component: PrivacyPolicy,
            name: 'PrivacyPolicy'
		},
		{
			path: '/contact-us',
            exact: true,
            component: ContactUs,
            name: 'Contact us'
		},
		{
			path: '/feature/request',
            exact: true,
            component: FeatureRequest,
            name: 'Feature Request'
		},
		{
			component: asyncComponentCreator('NotFound'),
			name: '404'
		}
	]

	const fns = {
		// Returns routing for React-Router
		routing () {
			return routes.map(a => pick(a, ['path', 'strict', 'exact', 'component', 'lazy']))
		},
		// Returns `name` + `path`. used in Header
		meta () {
			return routes.map(a => pick(a, ['path', 'name', 'exact', 'strict']))
		}
	}

	return fns[useFor]
}

const createRequiredHistory = process.env.BROWSER ? createBrowserHistory : createMemoryHistory

export const getMetaRoutes = routingFnCreator('meta')
export const getRouterRoutes = routingFnCreator('routing')
export const history = createRequiredHistory()
