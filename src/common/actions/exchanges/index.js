import {
    GETTING_USER_MATCHES,
    GET_USER_MATCHES_SUCCESS,
    GETTING_USER_REQUESTS_LIST,
    GET_USER_REQUESTS_SUCCESS,
    GETTING_USER_DONATES,
    GET_USER_DONATES_SUCCESS,
    REQUESTING_USER_BOOK_SWAP,
    BOOK_SWAP_REQUEST_SUCCESS,
    REQUESTING_BOOK_GIVEAWAY,
    REQUEST_BOOK_GIVEAWAY_SUCCESS,
    GETTING_USER_REQUESTED_SWAPS,
    GET_USER_REQUESTED_SWAPS_SUCCESS,
    UPDATE_REQUEST_STATUS_SUCCESS,
    UPDATE_REQUEST_BOOK_STATUS_SUCCESS,
    GET_USER_REQUESTS_COUNT_SUCCESS
} from '../../actionTypes'
import {
    requestingBookGiveaway,
    gettingUserMatches,
    gettingUserRequestsList,
    gettingUserDonates,
    requestingUserBookSwap,
    gettingUserRequestedSwaps,
    updatingRequestStatus,
    updatingRequestBookStatus,
    gettingUserRequestCount
} from '../../services/exchanges'
import {showMessagePopup} from "../app";

export const getUserMatchesList = (data) => async (dispatch) => {
    dispatch({
        type: GETTING_USER_MATCHES,
    });
    const { body } = await gettingUserMatches(data)
    if (!body.error) {
        dispatch({
            type: GET_USER_MATCHES_SUCCESS,
            exchanges: body,
            count: data.count
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const getUserDonatesList = (data) => async (dispatch) => {
    dispatch({
        type: GETTING_USER_DONATES,
    });
    const { body } = await gettingUserDonates(data)
    if (!body.error) {
        dispatch({
            type: GET_USER_DONATES_SUCCESS,
            donates: body,
            count: data.count
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const getUserRequestsList = (data) => async (dispatch) => {
    dispatch({
        type: GETTING_USER_REQUESTS_LIST
    });
    const { body } = await gettingUserRequestsList(data);
    if (!body.error) {
        dispatch({
            type: GET_USER_REQUESTS_SUCCESS,
            requests: body,
            count: data.count
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const getUserRequestCount = () => async (dispatch) => {
    const { body } = await gettingUserRequestCount();
    if (!body.error) {
        dispatch({
            type: GET_USER_REQUESTS_COUNT_SUCCESS,
            count: body.count
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const requestUserBookSwap = (data) => async (dispatch) => {
    dispatch({
        type: REQUESTING_USER_BOOK_SWAP,
        count: data.count
    });
    const { body } = await requestingUserBookSwap(data);
    if (!body.error) {
        if (body.id) {
            dispatch(showMessagePopup('Book swap request has been raised', 'success'))
            dispatch({
                type: BOOK_SWAP_REQUEST_SUCCESS,
                requests: body
            })
        }
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}

export const requestForGiveaway = (data) => async (dispatch) => {
    dispatch({
        type: REQUESTING_BOOK_GIVEAWAY
    });
    const { body } = await requestingBookGiveaway(data);
    if (!body.error) {
        if (body.id) {
            dispatch(showMessagePopup('Giveaway request has been raised', 'success'))
            dispatch({
                type: REQUEST_BOOK_GIVEAWAY_SUCCESS,
                requests: body
            })
        }
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}

export const getUserRequestedSwaps = (data) => async (dispatch) => {
    dispatch({
        type: GETTING_USER_REQUESTED_SWAPS
    });
    const { body } = await gettingUserRequestedSwaps(data);
    if (!body.error) {
        dispatch({
            type: GET_USER_REQUESTED_SWAPS_SUCCESS,
            data: body
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}

export const updateRequestStatus = (data) => async (dispatch) => {
    // dispatch({
    //     type: GETTING_USER_REQUESTED_SWAPS
    // });
    const { body } = await updatingRequestStatus(data);
    if (!body.error) {
      dispatch({
          type: UPDATE_REQUEST_STATUS_SUCCESS,
          data
      })
      getUserRequestsList({
          user_id: data.user_id,
          count: 0
      })(dispatch)
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const updateRequestBookStatus = (data) => async (dispatch) => {
    // dispatch({
    //     type: GETTING_USER_REQUESTED_SWAPS
    // });
    const { body } = await updatingRequestBookStatus(data);
    if (!body.error) {
        dispatch({
            type: UPDATE_REQUEST_BOOK_STATUS_SUCCESS,
            data
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};
