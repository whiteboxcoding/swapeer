import {
    LOGIN_SUCCESS,
    UPDATE_VIEW_TYPE,
    START_LOGIN_WITH_FACEBOOK,
    PROCESSING_MANUAL_SIGNUP,
    PROCESSING_MANUAL_LOGIN,
    MANUAL_SIGNUP_SUCCESS,
    MANUAL_SIGNUP_FAILURE,
    MANUAL_LOGIN_SUCCESS,
    GET_USER_DATA_SUCCESS,
    LOGOUT,
    PROCESSING_FINAL_LOGIN,
    FETCHING_USER_FOLLOWERS,
    FETCHED_USER_FOLLOWERS,
    FETCHING_USER_FOLLOWINGS,
    FETCHED_USER_FOLLOWINGS,
    GETTING_USER_DETAILS,
    GET_USER_DETAILS_SUCCESS,
    UPDATING_FOLLOW_STATUS,
    UPDATE_FOLLOW_STATUS_SUCCESS,
    UPLOAD_PROFILE_IMAGE_SUCCESS,
    UPLOAD_COVER_IMAGE_SUCCESS,
    INIT_REDUCER,
    RESET_USER_FOLLOWERS,
    RESET_USER_FOLLOWINGS,
} from '../../actionTypes'
import { push } from 'react-router-redux'
import get from 'lodash/get'
import {
    startLoginWithFacebook,
    processingManualSignup,
    processingManualLogin,
    gettingUserData,
    loggingoutUser,
    processingProfileUpdate,
    fetchingUserFollowers,
    fetchingUserFollowings,
    gettingUserDetails,
    updatingFollowStatus,
    submittingForgotPassword,
    resetingUserPassword
} from '../../services/login'
import { showMessagePopup } from '../app'
import { messages } from '../../constants'
import { setUserTokenInCookie, deleteUserTokenInCookie } from '../../helpers/cookie';

import imageUpload from '../../services/imageUpload'
import config from '../../../../config/environments'

export const loginWithFacebook = () => async (dispatch) => {
	dispatch({
		type: START_LOGIN_WITH_FACEBOOK,
	});
	const { body } = await startLoginWithFacebook()
	if (body) {
		// dispatch({
		// 	type: UPDATE_VIEW_TYPE,
		// 	viewType: 'readingSpace'
		// })
        dispatch(push({
            search: '?readingSpace=true'
        }));
	}
}
export const loginWithGoodReads = () => (dispatch) => {
	// dispatch({
	// 	type: UPDATE_VIEW_TYPE,
	// 	viewType: 'readingSpace'
	// })
    dispatch(push({
        search: '?readingSpace=true'
    }));
};

export const loginWithEmail = () => (dispatch) => {
	dispatch({
		type: UPDATE_VIEW_TYPE,
		viewType: 'signinWithEmail'
	})
};

export const signupWithEmail = () => (dispatch) => {
    dispatch({
        type: UPDATE_VIEW_TYPE,
        viewType: 'signupWithEmail'
    })
};

export const processFinalLogin = (data) => async (dispatch) => {
    dispatch({
        type: PROCESSING_FINAL_LOGIN,
    });
    const { body } = await processingProfileUpdate(data);
    if (body.id) {
        dispatch({
            type: LOGIN_SUCCESS
        });
        dispatch({
            type: GET_USER_DATA_SUCCESS,
            data
        });
        dispatch(push(`/home/collection/${body.id}`))
    } else {
        const message = body.error || 'Something went wrong while updating profile';
        dispatch(showMessagePopup(message, 'error'))
    }
};

export const updateViewType = (viewType) => (dispatch) => {
	dispatch({
		type: UPDATE_VIEW_TYPE,
		viewType
	})
};

export const processManualLogin = (data) => async (dispatch) => {
    dispatch({
        type: PROCESSING_MANUAL_LOGIN,
    });
    const { body } = await processingManualLogin(data);
    if (body.id) {
        setUserTokenInCookie(body.token);
        dispatch({
            type: MANUAL_LOGIN_SUCCESS,
        });
        dispatch({
            type: GET_USER_DATA_SUCCESS,
            data: body
        });
        dispatch(push(`/home/collection/${body.id}`))
    } else {
        const message = get(body, 'error.message') || 'Something went wrong while sign in';
        dispatch(showMessagePopup(message, 'error'))
    }
};

export const uploadProfileImage = (file) => async (dispatch) => {
    try {
        dispatch({
            type: UPLOAD_PROFILE_IMAGE_SUCCESS,
            data: '',
        });
        const body = await imageUpload(file, `${config.frontendBase}/upload/user/profile/pic`);
        dispatch({
            type: UPLOAD_PROFILE_IMAGE_SUCCESS,
            data: get(body, '0.url', ''),
        });
    } catch (e) {
        throw e;
    }
};

export const uploadCoverPicImage = (file, resolve) => async (dispatch) => {
    const body = await imageUpload(file, `${config.frontendBase}/upload/user/cover/pic`);
    dispatch({
        type: UPLOAD_COVER_IMAGE_SUCCESS,
        data: get(body, '0.url', ''),
    });
    if (resolve) {
        resolve(true)
    }
};

export const updateUserProfile = (data) => async (dispatch) => {
    const { body } = await processingProfileUpdate(data);
    if (body.id) {
        dispatch({
            type: GET_USER_DATA_SUCCESS,
            data
        });
    } else {
        const message = body.error || 'Something went wrong while updating profile';
        dispatch(showMessagePopup(message, 'error'))
    }
}

export const processManualSignup = (data) => async (dispatch) => {
    dispatch({
        type: PROCESSING_MANUAL_SIGNUP,
    });
    const { body } = await processingManualSignup(data);
    if (body.id) {
        const tokenData  = await processingManualLogin(data);
        setUserTokenInCookie(tokenData.body.token);
        // dispatch({
        //     type: UPDATE_VIEW_TYPE,
        //     viewType: 'readingSpace'
        // });
        dispatch(push({
            search: '?readingSpace=true'
        }));
        dispatch({
            type: MANUAL_SIGNUP_SUCCESS,
        });
        dispatch(showMessagePopup('User Signed up Successfully', 'success'))
	} else {
        dispatch({
            type: MANUAL_SIGNUP_FAILURE,
        });
        const message = get(body, 'error.message') || get(body, 'message')
        dispatch(showMessagePopup(message, 'error'))
    }
};

export const getUserData = () => async (dispatch) => {
    const { body } = await gettingUserData();
    if (!(body.userData && body.userData.id)) {
        dispatch({
            type: LOGOUT
        })
        deleteUserTokenInCookie();
    } else {
        dispatch({
            type: GET_USER_DATA_SUCCESS,
            data: body.userData,
        })
    }
};

export const logoutUser = () => async (dispatch) => {
    const { body } = await loggingoutUser();
    if (body) {
        dispatch({
            type: LOGOUT
        });
        dispatch(getUserData())
        deleteUserTokenInCookie();
        dispatch({
            type: INIT_REDUCER
        })
        dispatch(push('/home'))
    }
};

export const fetchFollowersUsers = (data) => async (dispatch) => {
    if(data.count === 0) {
        dispatch({
            type: RESET_USER_FOLLOWERS,
        })
    }
    dispatch({
        type: FETCHING_USER_FOLLOWERS,
    });
    const { body } = await fetchingUserFollowers(data)
    if (!body.error) {
        dispatch({
            type: FETCHED_USER_FOLLOWERS,
            data: body
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const fetchFollowingUsers = (data) => async (dispatch) => {
    if(data.count === 0) {
        dispatch({
            type: RESET_USER_FOLLOWINGS,
        })
    }
    dispatch({
        type: FETCHING_USER_FOLLOWINGS,
    });
    const { body } = await fetchingUserFollowings(data)
    if (!body.error) {
        dispatch({
            type: FETCHED_USER_FOLLOWINGS,
            data: body
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const getUserDetails = (data) => async (dispatch) => {
    dispatch({
        type: GETTING_USER_DETAILS
    })
    const { body } = await gettingUserDetails(data)
    if (!body.error) {
        dispatch({
            type: GET_USER_DETAILS_SUCCESS,
            data: body,
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}

export const submitForgotPassword = (data) => async (dispatch) => {
    const { body } = await submittingForgotPassword({ email: data })
    if (!body.error && body.message === messages.sendOtpSuccess) {
        dispatch(showMessagePopup(body.message, 'success'))
        dispatch(push({
            search: '?forgotpassword=true'
        }));
    } else {
        dispatch(showMessagePopup((body.error || body.message), 'error'))
    }
};

export const resetUserPassword = (data) => async (dispatch) => {
    const { body } = await resetingUserPassword(data)
    if (!body.error && body.message === messages.otpResetSuccess) {
        dispatch(showMessagePopup(body.message, 'success'))
        dispatch(push({
            search: ''
        }));
    } else {
        dispatch(showMessagePopup((body.error || body.message), 'error'))
    }
};

export const updateFollowStatus = (data, viewType) => async (dispatch) => {
    dispatch({
        type: UPDATING_FOLLOW_STATUS
    })
    const { body } = await updatingFollowStatus(data)
    if (!body.error) {
        dispatch({
            type: UPDATE_FOLLOW_STATUS_SUCCESS,
            data,
            viewType
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}
