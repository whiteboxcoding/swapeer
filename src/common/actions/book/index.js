import {
    searchBookByISBNApi,
    addingBookToCollection,
    addingBookToWishlist,
    gettingUserBooksList,
    removingBookFromWishlist,
    addingBookToDatabase,
    updatingBookStatus,
    gettingUserBookHistory
} from '../../services/book'
import {
    ADDING_BOOK_TO_COLLECTION,
    RECEIVED_BOOKS_DATA,
    RECEIVING_BOOKS_DATA,
	SAVE_UPLOAD_IMAGE_SUCCESS,
    ADDED_BOOK_TO_COLLECTION,
    OPEN_ADD_SEARCHED_BOOK_MODAL,
    CLOSE_ADD_SEARCHED_BOOK_MODAL,
    RESET_SEARCHED_BOOK_DATA,
    UPDATING_BOOK_IN_WISHLIST,
    UPDATE_BOOK_IN_WISHLIST,
    GETTING_USER_BOOKS_LIST,
    GET_USER_BOOKS_LIST_SUCCESS,
    UPDATE_BOOK_STATUS_SUCCESS,
    REMOVE_UPLOAD_IMAGE_SUCCESS,
    GET_USER_BOOK_HISTORY_SUCCESS,
} from "../../actionTypes";
import { getUserCollection } from '../collection'
import imageUpload from "../../services/imageUpload";
import {showMessagePopup} from "../app";
import config from '../../../../config/environments'

export const searchBookByName = (data, selectedBooks) => async (dispatch) => {
	dispatch({
		type: RECEIVING_BOOKS_DATA,
        searchQuery: data.text
	});
	const { body } = await searchBookByISBNApi(data)
    if (!body.error) {
	    let allBooks = body
	    if (selectedBooks && selectedBooks.length > 0) {
            allBooks = body.concat(selectedBooks)
        }
        dispatch({
            type: RECEIVED_BOOKS_DATA,
            booksList: allBooks,
            count: data.count
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const closeAddSearchedBookModal = () => (dispatch) => {
    dispatch({
        type: CLOSE_ADD_SEARCHED_BOOK_MODAL
    })
};

export const resetSearchedBookData = () => (dispatch) => {
    dispatch({
        type: RESET_SEARCHED_BOOK_DATA
    })
};

export const openAddSearchedBookModal = (data) => async (dispatch) => {
    dispatch({
        type: OPEN_ADD_SEARCHED_BOOK_MODAL,
        data
    });
};

export const removeUploadImage = () => (dispatch) => {
    dispatch({
        type: REMOVE_UPLOAD_IMAGE_SUCCESS
    })
};

export const addBookToCollection = (data) => async (dispatch) => {
    if (!data.book.id) {
        const bookResp = await addingBookToDatabase(data.book)
        if (bookResp.body.id) {
            if (bookResp.body.images && bookResp.body.images.thumbnail) {
                bookResp.body.image = bookResp.body.images.thumbnail
                delete bookResp.body.images
            }
            data.book = {
                ...data.book,
                ...bookResp.body
            }
        }
    }
    dispatch({
        type: ADDING_BOOK_TO_COLLECTION,
    });
    const { body } = await addingBookToCollection(data)
    getUserCollection(data.user_id)(dispatch)
    if (!body.error) {
        dispatch({
            type: ADDED_BOOK_TO_COLLECTION,
            book: body,
            category: data.category
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const saveUploadedFile = (file) => async (dispatch) => {
    const body = await imageUpload(file, `${config.frontendBase}/upload/book/image`);
    if (!body.error) {
        dispatch({
            type: SAVE_UPLOAD_IMAGE_SUCCESS,
            image: body[0],
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const addBookToWishlist = (data) => async (dispatch) => {
    dispatch({
        type: UPDATING_BOOK_IN_WISHLIST,
    });
    const { body } = await addingBookToWishlist(data)
    if (body.message) {
        dispatch(showMessagePopup(body.message, 'success'))
    } else if (!body.error) {
        dispatch({
            type: UPDATE_BOOK_IN_WISHLIST,
            book: body,
            bookId: body.book.id,
            actionType: 'add',
            userId: data.user_id
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}

export const removeFromWishlist = (data) => async (dispatch) => {
    dispatch({
        type: UPDATING_BOOK_IN_WISHLIST,
    });
    const { body } = await removingBookFromWishlist(data)
    if (!body.error) {
        dispatch({
            type: UPDATE_BOOK_IN_WISHLIST,
            book: data,
            bookId: data.bookId,
            actionType: 'remove',
            userId: data.user_id
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}

export const updateBookStatus = (data) => async (dispatch) => {
    const { body } = await updatingBookStatus(data)
    if (!body.error) {
        dispatch({
            type: UPDATE_BOOK_STATUS_SUCCESS,
            data,
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}

export const getUserBooksList = (data) => async (dispatch) => {
    dispatch({
        type: GETTING_USER_BOOKS_LIST,
    });
    const { body } = await gettingUserBooksList(data)
    if (!body.error) {
        dispatch({
            type: GET_USER_BOOKS_LIST_SUCCESS,
            data: body,
            userId: data.user_id
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const addBookToDatabase = (data, collection) => async (dispatch) => {
    const { body } = await addingBookToDatabase(data)
    if (!body.error) {
        if (body.id) {
            // dispatch(showMessagePopup('Book has been added to database', 'success'))
        }
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const getUserBookHistory = (data) => async (dispatch) => {
    const { body } = await gettingUserBookHistory(data)
    if (!body.error) {
        if (body.id) {
            dispatch({
              type: GET_USER_BOOK_HISTORY_SUCCESS,
              data: body
            })
        }
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};
