import {
    GETTING_USER_CHAT_MESSAGES,
    GET_USER_CHAT_SUCCESS,
    ADD_USER_CHAT, GET_USER_BOOKS_LIST_SUCCESS
} from "../../actionTypes";

import {
    gettingChatMessage,
    sendingChatMessage
} from '../../services/chat'
import {showMessagePopup} from "../app";

export const getChatMessages = (data) => async (dispatch, getState) => {
    const state = getState()
    if (state.chat.userMessages.status === 'fetching') {
        return;
    }
    dispatch({
        type: GETTING_USER_CHAT_MESSAGES,
        count: data.count
    })
    const { body } = await gettingChatMessage(data)
    if (!body.error) {
        dispatch({
            type: GET_USER_CHAT_SUCCESS,
            data: body,
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}

export const saveNewMessages = (data) => (dispatch) => {
  dispatch({
      type: GET_USER_CHAT_SUCCESS,
      data: [data]
  })
}

export const sendChatMessage = (data) => async (dispatch) => {
    const { body } = await sendingChatMessage(data)
    if (!body.error) {
        dispatch({
            type: ADD_USER_CHAT,
            data: {
                ...data,
                ...body,
                created: new Date().toISOString()
            }
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
}
