import { push } from 'react-router-redux'
import {
    GETTING_BOOKS_OF_COLLECTION,
    RECEIVED_BOOKS_OF_COLLECTION,
    GETTING_USER_COLLECTION,
    GET_USER_COLLECTION_SUCCESS,
    GETTING_USER_WISHLIST,
    GET_USER_WISHLIST_SUCCESS,
    RESET_BOOKS_OF_COLLECTION, ADD_USER_CHAT,
} from '../../actionTypes'
import { gettingBooksOfCollection, gettingUserCollection, gettingUserWishlist } from 'common/services/collection'
import { removingBookFromCollection } from '../../services/book'
import {showMessagePopup} from "../app";

export const getUserCollection = (data) => async (dispatch) => {
    if (data.user_id) {
        dispatch({
            type: GETTING_USER_COLLECTION,
        });
        const { body } = await gettingUserCollection(data)
        if (!body.error) {
            dispatch({
                type: GET_USER_COLLECTION_SUCCESS,
                book: body
            })
        } else {
            dispatch(showMessagePopup(body.error, 'error'))
        }
    }
};

export const removeBookFromCollection = (data, collection) => async (dispatch) => {
    const resp = await Promise.all([removingBookFromCollection(data), gettingBooksOfCollection(collection)])
    const { body } = resp[1];
    if (body.length === 0) {
        dispatch(push(`/home/collection/${collection.user_id}`))
    }
    dispatch({
        type: GET_USER_COLLECTION_SUCCESS,
        book: body
    })
    dispatch({
        type: RESET_BOOKS_OF_COLLECTION,
        category: collection.category
    })
    dispatch({
        type: RECEIVED_BOOKS_OF_COLLECTION,
        category: collection.category,
        books: body
    })
};

export const getUserWishlist = (data) => async (dispatch) => {
    dispatch({
        type: GETTING_USER_WISHLIST,
    });
    const { body } = await gettingUserWishlist(data)

    if (!body.error) {
        dispatch({
            type: GET_USER_WISHLIST_SUCCESS,
            data: body,
            userId: data.user_id
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const getCollectionBooks = (data) => async (dispatch) => {
    if (data.count === 0) {
        dispatch({
            type: RESET_BOOKS_OF_COLLECTION,
            category: data.category
        })
    }
	dispatch({
		type: GETTING_BOOKS_OF_COLLECTION,
	})
	const { body } = await gettingBooksOfCollection(data)
    if (!body.error) {
        dispatch({
            type: RECEIVED_BOOKS_OF_COLLECTION,
            category: data.category,
            books: body
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};
