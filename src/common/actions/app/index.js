import {
    HEADER_MENU_CHANGE,
    RECEIVED_NEW_USER_DATA,
    SHOW_MESSAGE_POPUP,
    HIDE_MESSAGE_POPUP,
    RECEIVED_USER_PROFILE_SEARCH,
    RECEIVING_PROFILES_DATA,
    RECEIVED_PROFILES_DATA,
    RESET_PROFILES_DATA,
    RECEIVING_NEW_USER_DATA,
    UPDATE_FIRST_TIME_USER,
    OPEN_SEARCH_BAR,
    SAVE_READERS_STATS
} from 'common/actionTypes'
import {
    gettingFirstTimeUserData,
    updatingNewUserData,
    gettingUsersList,
    searchingProfileByName,
    gettingReeadersStats
} from "../../services/app";
import {CLOSE_SEARCH_BAR} from "../../actionTypes";

// may have to remove
export const onHeaderMenuChange = (e, { name }) => (dispatch) => {
	dispatch({
		type: HEADER_MENU_CHANGE,
		name
	})
};

export const openSearchBar = () => (dispatch) => {
    document.getElementById('sideBarPushable').scrollTop = 0;
    dispatch({
        type: OPEN_SEARCH_BAR,
    })
};

export const getReeadersStats = () => async (dispatch) => {
    const { body } = await gettingReeadersStats();
    dispatch({
        type: SAVE_READERS_STATS,
        body
    })
};

export const closeSearchBar = () => (dispatch) => {
    dispatch({
        type: CLOSE_SEARCH_BAR,
    })
};

export const getFirstTimeUserData = () => async (dispatch) => {
    dispatch({
        type: RECEIVING_NEW_USER_DATA,
    })
    const { body } = await gettingFirstTimeUserData();
    if (!body.error) {
        dispatch({
            type: RECEIVED_NEW_USER_DATA,
            userData: body
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const updateNewUserData = () => async (dispatch) => {
    const { body } = await updatingNewUserData();
    dispatch({
        type: UPDATE_FIRST_TIME_USER,
    })
};

export const showMessagePopup = (message, messageType) => (dispatch) => {
    dispatch({
        type: SHOW_MESSAGE_POPUP,
        message,
        messageType
    })
};

export const hideMessagePopup = () => async (dispatch) => {
    dispatch({
        type: HIDE_MESSAGE_POPUP
    })
};

export const getUsersList = (data) => async (dispatch) => {
    const { body } = await gettingUsersList(data);
    if (!body.error) {
        dispatch({
            type: RECEIVED_USER_PROFILE_SEARCH,
            data: body
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const searchProfileByName = (data) => async (dispatch) => {
    if (data.count === 0) {
        dispatch({
            type: RESET_PROFILES_DATA,
        })
    }
    dispatch({
        type: RECEIVING_PROFILES_DATA,
        searchQuery: data.text
    });
    const { body } = await searchingProfileByName(data)
    if (!body.error) {
        dispatch({
            type: RECEIVED_PROFILES_DATA,
            profilesList: body,
        })
    } else {
        dispatch(showMessagePopup(body.error, 'error'))
    }
};

export const resetSearchedProfileData = () => (dispatch) => {
    dispatch({
        type: RESET_PROFILES_DATA,
    })
}
