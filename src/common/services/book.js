import {
  get,
  post,
  put,
  patch,
} from 'common/utils/api';

import { post as postFormData} from 'common/utils/formDataApi';

export const searchBookByISBNApi = (data) => post('/search/book').send({ data })

export const uploadingBookImageFile = (files) => {
  const data = new FormData()
  data.append( 'file', files );
  const xhr = new XMLHttpRequest();
  xhr.open( 'POST', '/api/book/image/upload', true );
  xhr.onreadystatechange = function ( response ) {
    console.log('response', response)
  };
  xhr.send( data );
};
export const addingBookToCollection = (data) => post('/book/collection/add').send({ data })

export const removingBookFromCollection = (data) => post('/book/collection/remove').send({ data })

export const addingBookToWishlist = (data) => post('/book/wishlist/add').send({ data })

export const removingBookFromWishlist = (data) => post('/book/wishlist/remove').send({ data })

export const updatingBookStatus = (data) => post('/update/user/book').send({ data })

export const addingBookToDatabase = (data) => post('/book/database/add').send({ data })

export const gettingUserBooksList = (data) => post('/get/user/books/list').send({ data })

export const gettingUserSwaps = (data) => post('/get/swaps/list').send({ data })

export const gettingUserBookHistory = (data) => post('/user/history/book').send({ data })
