import {
    get,
    post,
    put,
    patch,
} from 'common/utils/api';

export const gettingFirstTimeUserData = () => get('/firstTimeUser');

export const updatingNewUserData = () => post('/update/firstTimeUser').send();

export const gettingUsersList = (data) => post('/user/profile/search').send({ data });

export const searchingProfileByName = (data) => post('/search/user/profile').send({ data });

export const gettingReeadersStats = () => get('/readers/stats');
