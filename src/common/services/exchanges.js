import {
    post,
} from 'common/utils/api';

export const gettingUserMatches = (data) => post('/user/matches/list').send({ data });
export const gettingUserRequestsList = (data) => post('/user/requests/list').send({ data });
export const gettingUserDonates = (data) => post('/user/donates/list').send({ data });
export const requestingUserBookSwap = (data) => post('/request/user/book/swap').send({ data });
export const requestingBookGiveaway = (data) => post('/request/book/giveaway').send({ data });
export const gettingUserRequestedSwaps = (data) => post('/get/user/swaps').send({ data });
export const updatingRequestStatus = (data) => post('/update/request/status').send({ data });
export const updatingRequestBookStatus = (data) => post('/update/book/status').send({ data });
export const gettingUserRequestCount = () => post('/requests/count');
