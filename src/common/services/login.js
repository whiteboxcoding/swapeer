import {
  get,
  post,
  put,
  patch,
} from 'common/utils/api';

export const startLoginWithFacebook = () => get('/login/facebook')

export const processingManualSignup = (data) => post('/auth/signup').send({ data });

export const processingManualLogin = (data) => post('/auth/login').send({ data });

export const processingProfileUpdate = (data) => post('/profile/update').send({ data });

export const gettingUserData = () => get('/get/userData');

export const loggingoutUser = () => post('/auth/logout');

export const fetchingUserFollowers = (data) => post('/user/followers').send({ data });

export const fetchingUserFollowings = (data) => post('/user/followings').send({ data });

export const updatingFollowStatus = (data) => post('/update/follow/status').send({ data })

export const gettingUserDetails = (data) => post('/get/user/details').send({ data });

export const submittingForgotPassword = (data) => post('/user/forgot/password').send({ data });

export const resetingUserPassword = (data) => post('/reset/user/password').send({ data });
