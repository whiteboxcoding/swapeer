import {
  get,
  post,
  put,
  patch,
} from 'common/utils/api';

export const gettingBooksOfCollection = (data) => post('/user/book/collection').send({ data })
export const gettingUserCollection = (data) => post('/user/book/collection/count').send({ data });
export const gettingUserWishlist = (data) => post('/user/book/wishlist').send({ data });
