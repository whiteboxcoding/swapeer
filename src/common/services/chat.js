import {
    get,
    post,
    put,
    patch,
} from 'common/utils/api';

export const gettingChatMessage = (data) => post('/user/chat').send({ data });

export const sendingChatMessage = (data) => post('/send/chat/message').send({ data });
