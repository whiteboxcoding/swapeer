export default function imageUpload(files, url) {
    const data = new FormData();
    // const token = getUserTokenFromCookie() || '';
    Object.values(files).forEach(f => {
        data.append('files', f, f.name);
    });
    const xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    const promise = new Promise((resolve => {
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200) {
                resolve(JSON.parse(xhr.response));
            }
        };
    }));
    // xhr.setRequestHeader('x-auth-token', token);
    xhr.send(data);
    return promise.then((resp) => {
        return resp;
    });
}
