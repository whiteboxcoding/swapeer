export const tabNameToPathMap = (userId) => ({
	'Home': `/home/collection/${userId}`,
	'Explore': '/explore',
	'Books': `/swaps/${userId}?tab=exchange`,
	'Login': '/login',
	'Sign up': '/signup'
})

export const tabToPathMap = {
    'Home': '/home',
    'Explore': '/explore',
    'Login': '/login',
    'Sign up': '/signup'
};

export const messages = {
    sendOtpSuccess: 'Verification Email has been sent.',
    otpResetSuccess: 'Password has been updated Successfully',
};


export const genresList = [{
    "key": "Action and adventure",
    "text": "Action and adventure",
    "value": "Action and adventure"
}, {
    "key": "Alternate history",
    "text": "Alternate history",
    "value": "Alternate history"
}, {
    "key": "Anthology",
    "text": "Anthology",
    "value": "Anthology"
}, {
    "key": "Art",
    "text": "Art",
    "value": "Art"
}, {
    "key": "Autobiography",
    "text": "Autobiography",
    "value": "Autobiography"
}, {
    "key": "Biography",
    "text": "Biography",
    "value": "Biography"
}, {
    "key": "Book review",
    "text": "Book review",
    "value": "Book review"
}, {
    "key": "Chick lit",
    "text": "Chick lit",
    "value": "Chick lit"
}, {
    "key": "Children's",
    "text": "Children's",
    "value": "Children's"
}, {
    "key": "Comic book",
    "text": "Comic book",
    "value": "Comic book"
}, {
    "key": "Coming-of-age",
    "text": "Coming-of-age",
    "value": "Coming-of-age"
}, {
    "key": "Cookbook",
    "text": "Cookbook",
    "value": "Cookbook"
}, {
    "key": "Crime",
    "text": "Crime",
    "value": "Crime"
}, {
    "key": "Diary",
    "text": "Diary",
    "value": "Diary"
}, {
    "key": "Dictionary",
    "text": "Dictionary",
    "value": "Dictionary"
}, {
    "key": "Drama",
    "text": "Drama",
    "value": "Drama"
}, {
    "key": "Encyclopedia",
    "text": "Encyclopedia",
    "value": "Encyclopedia"
}, {
    "key": "Fairytale",
    "text": "Fairytale",
    "value": "Fairytale"
}, {
    "key": "Fantasy",
    "text": "Fantasy",
    "value": "Fantasy"
}, {
    "key": "Graphic novel",
    "text": "Graphic novel",
    "value": "Graphic novel"
}, {
    "key": "Guide",
    "text": "Guide",
    "value": "Guide"
}, {
    "key": "Health",
    "text": "Health",
    "value": "Health"
}, {
    "key": "Historical fiction",
    "text": "Historical fiction",
    "value": "Historical fiction"
}, {
    "key": "History",
    "text": "History",
    "value": "History"
}, {
    "key": "Horror",
    "text": "Horror",
    "value": "Horror"
}, {
    "key": "Journal",
    "text": "Journal",
    "value": "Journal"
}, {
    "key": "Math",
    "text": "Math",
    "value": "Math"
}, {
    "key": "Memoir",
    "text": "Memoir",
    "value": "Memoir"
}, {
    "key": "Mystery",
    "text": "Mystery",
    "value": "Mystery"
}, {
    "key": "Paranormal romance",
    "text": "Paranormal romance",
    "value": "Paranormal romance"
}, {
    "key": "Picture book",
    "text": "Picture book",
    "value": "Picture book"
}, {
    "key": "Poetry",
    "text": "Poetry",
    "value": "Poetry"
}, {
    "key": "Political thriller",
    "text": "Political thriller",
    "value": "Political thriller"
}, {
    "key": "Prayer",
    "text": "Prayer",
    "value": "Prayer"
}, {
    "key": "Religion, spirituality, and new age",
    "text": "Religion, spirituality, and new age",
    "value": "Religion, spirituality, and new age"
}, {
    "key": "Review",
    "text": "Review",
    "value": "Review"
}, {
    "key": "Romance",
    "text": "Romance",
    "value": "Romance"
}, {
    "key": "Satire",
    "text": "Satire",
    "value": "Satire"
}, {
    "key": "Science",
    "text": "Science",
    "value": "Science"
}, {
    "key": "Science fiction",
    "text": "Science fiction",
    "value": "Science fiction"
}, {
    "key": "Self help",
    "text": "Self help",
    "value": "Self help"
}, {
    "key": "Short story",
    "text": "Short story",
    "value": "Short story"
}, {
    "key": "Suspense",
    "text": "Suspense",
    "value": "Suspense"
}, {
    "key": "Textbook",
    "text": "Textbook",
    "value": "Textbook"
}, {
    "key": "Thriller",
    "text": "Thriller",
    "value": "Thriller"
}, {
    "key": "Travel",
    "text": "Travel",
    "value": "Travel"
}, {
    "key": "True crime",
    "text": "True crime",
    "value": "True crime"
}, {
    "key": "Young adult",
    "text": "Young adult",
    "value": "Young adult"
}];
