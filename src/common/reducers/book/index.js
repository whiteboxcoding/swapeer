/*
 *
 * Book reducer
 *
 */
import {
	RECEIVED_BOOKS_DATA,
	RECEIVING_BOOKS_DATA,
	RECEIVED_BOOKS_OF_COLLECTION,
    RESET_BOOKS_OF_COLLECTION,
    SAVE_UPLOAD_IMAGE_SUCCESS,
    GETTING_USER_WISHLIST,
    GET_USER_WISHLIST_SUCCESS,
    ADDED_BOOK_TO_COLLECTION,
    OPEN_ADD_SEARCHED_BOOK_MODAL,
    CLOSE_ADD_SEARCHED_BOOK_MODAL,
    RESET_SEARCHED_BOOK_DATA,
    GETTING_USER_BOOKS_LIST,
    GET_USER_BOOKS_LIST_SUCCESS,
    UPDATE_BOOK_IN_WISHLIST,
    REMOVE_UPLOAD_IMAGE_SUCCESS,
		GET_USER_BOOK_HISTORY_SUCCESS,
} from 'common/actionTypes'
import cloneDeep from 'lodash/cloneDeep'
import uniqBy from 'lodash/uniqBy'
import {INIT_REDUCER} from "../../actionTypes";

const initialState = {
	searchedBooks: {
    	status: false,
        searchQuery: '',
        fetchedAll: false,
    	data: [],
  	},
    books: {},
    userWishlist: {
		status: false,
		data: {}
	},
	addBookImage: {},
    searchedBookModal: {
		open: false,
		data: {}
	},
	userBooks: {
		status: false,
		data: {}
	},
	userBookHistory: {
		status: false,
		data: []
	}
};

const bookReducer = (state = initialState, action) => {
	switch (action.type) {
	case RECEIVED_BOOKS_DATA: {
		const cloneData = cloneDeep(state.searchedBooks.data)
        return {
            ...state,
            searchedBooks: {
                ...state.searchedBooks,
                data: (action.count === 0 ? action.booksList : cloneData.concat(action.booksList)),
                status: 'fetched',
				fetchedAll: (action.booksList.length === 0)
            }
        };
    }
	case RESET_SEARCHED_BOOK_DATA:
		return {
			...state,
			searchedBooks: {
                status: false,
                searchQuery: '',
                data: [],
			}
		};
	case RECEIVING_BOOKS_DATA:
		return {
			...state,
		  	searchedBooks: {
				...state.searchedBooks,
				status: 'fetching',
				searchQuery: action.searchQuery
		  	}
		};
	case SAVE_UPLOAD_IMAGE_SUCCESS:
        return {
            ...state,
            addBookImage: action.image
        };
	case REMOVE_UPLOAD_IMAGE_SUCCESS:
		return {
			...state,
			addBookImage: {}
		};
	case RECEIVED_BOOKS_OF_COLLECTION: {
        const cloneData = cloneDeep(state.books[action.category]) || {}
        if (!cloneData.data) {
            cloneData.data = []
        }
        if (action.books.length === 0) {
            cloneData.fetchedAll = true
		}
        cloneData.data = uniqBy(cloneData.data.concat(action.books), 'id')
        return {
            ...state,
            books: {
                ...state.books,
                [action.category]: cloneData
            }
        };
	}
	case RESET_BOOKS_OF_COLLECTION: {
		const cloneData = cloneDeep(state.books)
		delete cloneData[action.category]
        return {
            ...state,
            books: cloneData
        }
	}
	case GETTING_USER_WISHLIST:
		return {
			...state,
            userWishlist: {
				...state.userWishlist,
				status: 'fetching'
            }
		};
	case ADDED_BOOK_TO_COLLECTION: {
		const booksList = cloneDeep(state.books[action.category]) || { data: [] }
		const index = booksList.data.findIndex((obj) => (
			obj.id === action.book.id
		))
		if (index >= 0) {
            booksList.data[index] = action.book
		} else {
            booksList.data.push(action.book)
		}
        return {
            ...state,
            books: {
                [action.category]: booksList
            }
        };
    }
	case UPDATE_BOOK_IN_WISHLIST: {
		const booksList = cloneDeep(state.userWishlist.data[action.userId])
		const index = booksList.findIndex((obj) => (
			obj.book.id === action.bookId
		))
		if (action.actionType === 'add' && index < 0) {
            booksList.push(action.book)
		}
        if (action.actionType === 'remove' && index >= 0) {
            booksList.splice(index, 1)
        }
		return {
			...state,
            userWishlist: {
				...state.userWishlist,
                data: {
					...state.userWishlist.data,
					[action.userId]: booksList
				}
			}
		};
	}
	case GET_USER_WISHLIST_SUCCESS: {
		const cloneData = cloneDeep(state.userWishlist.data[action.userId]) || []
        return {
            ...state,
            userWishlist: {
                ...state.userWishlist,
                status: 'fetched',
                data: {
                    ...state.userWishlist.data,
                    [action.userId]: uniqBy(cloneData.concat(action.data), 'id')
                }
            }
        };
	}
	case OPEN_ADD_SEARCHED_BOOK_MODAL:
		return {
			...state,
			searchedBookModal: {
				open: true,
				data: action.data
			}
		};
	case CLOSE_ADD_SEARCHED_BOOK_MODAL:
		return {
			...state,
			searchedBookModal: {
				open: false,
				data: {}
			}
		};
	case GETTING_USER_BOOKS_LIST:
		return {
        ...state,
            userBooks: {
            ...state.userBooks,
            status: 'fetching',
        }
    };
	case GET_USER_BOOK_HISTORY_SUCCESS:
		return {
        ...state,
            userBookHistory: {
            ...state.userBookHistory,
            status: 'fetched',
        }
    }
	case GET_USER_BOOKS_LIST_SUCCESS: {
        return {
            ...state,
            userBooks: {
                ...state.userBooks,
                data: {
					...state.userBooks.data,
                	[action.userId]: action.data
				},
                status: 'fetched'
            }
        }
	}
        case INIT_REDUCER:
            return initialState;
	default:
		return state
	}
}

export default bookReducer
