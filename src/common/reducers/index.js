// @flow
import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'
import loginReducer from './loginReducer'
import collectionReducer from './collection'
import appReducer from './appReducer'
import bookReducer from './book'
import exchanges from './exchanges'
import chatReducer from './chat'

// Root reducer
export default combineReducers({
	routing: routerReducer,
	login: loginReducer,
	app: appReducer,
	form: formReducer,
	collection: collectionReducer,
	books: bookReducer,
    chat: chatReducer,
	exchanges,
})
