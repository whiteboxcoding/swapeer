/*
 *
 * Login reducer
 *
 */
import {
    HEADER_MENU_CHANGE,
    RECEIVED_NEW_USER_DATA,
    RECEIVING_NEW_USER_DATA,
    SHOW_MESSAGE_POPUP,
    HIDE_MESSAGE_POPUP,
    FETCHING_USER_FOLLOWERS,
    FETCHED_USER_FOLLOWERS,
    FETCHING_USER_FOLLOWINGS,
    FETCHED_USER_FOLLOWINGS,
    UPDATE_FOLLOW_STATUS_SUCCESS,
    RECEIVING_PROFILES_DATA,
    RECEIVED_PROFILES_DATA,
    RECEIVED_USER_PROFILE_SEARCH,
    RESET_PROFILES_DATA,
	INIT_REDUCER,
    RESET_USER_FOLLOWERS,
    RESET_USER_FOLLOWINGS,
    OPEN_SEARCH_BAR,
    CLOSE_SEARCH_BAR,
    SAVE_READERS_STATS
} from '../../actionTypes'
import cloneDeep from 'lodash/cloneDeep'
import uniqBy from 'lodash/uniqBy'

const initialState = {
	activeMenu: 'Sign up',
    searchedProfile: {
        status: false,
        searchQuery: '',
        fetchedAll: false,
        data: [],
    },
	searchedUser: [],
	newUserData: {
        status: false,
        readersStats: {},
        data: {}
    },
	followers: {
	    status: false,
        data: [],
        fetchedAll: false
    },
	followings: {
        status: false,
        data: [],
        fetchedAll: false
    },
	messagePopup: {
		text: '',
		type: 'success',
	},
    openSearchBar: false,
}

const loginReducer = (state = initialState, action) => {
	switch (action.type) {
	case HEADER_MENU_CHANGE:
		return {
			...state,
			activeMenu: action.name
		};
		case RECEIVED_USER_PROFILE_SEARCH:
		return {
			...state,
            searchedUser: action.data
		};
        case RECEIVED_PROFILES_DATA: {
            const cloneData = cloneDeep(state.searchedProfile.data)
            return {
                ...state,
                searchedProfile: {
                    ...state.searchedProfile,
                    data: cloneData.concat(action.profilesList),
                    status: 'fetched',
                    fetchedAll: (action.profilesList.length === 0)
                }
            };
        }
        case RESET_PROFILES_DATA:
            return {
                ...state,
                searchedProfile: {
                    status: false,
                    searchQuery: '',
                    data: [],
                }
            };
        case RECEIVING_PROFILES_DATA:
            return {
                ...state,
                searchedProfile: {
                    ...state.searchedProfile,
                    status: 'fetching',
                    searchQuery: action.searchQuery,
                }
            };
	case SHOW_MESSAGE_POPUP:
		return {
			...state,
			messagePopup: {
				text: action.message,
				type: action.messageType || 'success'
			}
		};
	case HIDE_MESSAGE_POPUP:
		return {
            ...state,
            messagePopup: {
                text: '',
                type: 'success'
            }
		};
	case RECEIVED_NEW_USER_DATA:
		return {
            ...state,
            newUserData: {
              ...state.newUserData,
              status: 'fetched',
              data: action.userData
            }
		};
  case RECEIVING_NEW_USER_DATA:
    return {
            ...state,
            newUserData: {
              ...state.newUserData,
              status: 'fetching',
            }
    };
    case RESET_USER_FOLLOWERS: return {
        ...state,
        followers: {
            status: false,
            data: [],
            fetchedAll: false
        }
    };
    case RESET_USER_FOLLOWINGS:
        return {
            ...state,
            followings: {
                ...state.followings,
                status: false,
                data: [],
                fetchedAll: false
            }
        };
	case FETCHING_USER_FOLLOWERS:
		return {
			...state,
            followers: {
                ...state.followers,
			    status: 'fetching'
            }
		};
	case FETCHED_USER_FOLLOWERS: {
	    const cloneData = cloneDeep(state.followers.data)
        return {
            ...state,
            followers: {
                status: 'fetched',
                data: uniqBy(cloneData.concat(action.data)),
                fetchedAll: action.data.length === 0
            }
        };
    }
    case FETCHING_USER_FOLLOWINGS:
        return {
            ...state,
            followings: {
                ...state.followings,
                status: 'fetching'
            }
        };
	case FETCHED_USER_FOLLOWINGS: {
        const cloneData = cloneDeep(state.followings.data)
        return {
            ...state,
            followings: {
                ...state.followings,
                status: 'fetched',
                data: uniqBy(cloneData.concat(action.data), 'id'),
                fetchedAll: (action.data.length === 0)
            },
        };
    }
	case UPDATE_FOLLOW_STATUS_SUCCESS: {
        const { data } = action
		if (!data.follow) {
            const cloneData = cloneDeep(state.followings.data)
            const index = cloneData.findIndex((obj) => (obj.id === data.following_id))
            if (index >= 0) {
                cloneData.splice(index, 1)
            }
            return {
				...state,
                followings: {
                  ...state.followings,
                  data: cloneData
                }
		    }
	    }
	    return state
    }
        case INIT_REDUCER:
            return initialState;
        case OPEN_SEARCH_BAR:
            return {
                ...state,
                openSearchBar: true
            };
        case CLOSE_SEARCH_BAR:
            return {
                ...state,
                openSearchBar: false
            };
        case SAVE_READERS_STATS:
            return {
                ...state,
                readersStats: action.body
            };
	default:
		return state
	}
}

export default loginReducer
