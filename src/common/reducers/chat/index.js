/*
 *
 * chat reducer
 *
 */
import cloneDeep from 'lodash/cloneDeep'

import {
    GETTING_USER_CHAT_MESSAGES,
    GET_USER_CHAT_SUCCESS,
    ADD_USER_CHAT, INIT_REDUCER
} from "../../actionTypes";

const initialState = {
    userMessages: {
        status: false,
        data: []
    }
}

const collectionReducer = (state = initialState, action) => {
    switch (action.type) {
        case GETTING_USER_CHAT_MESSAGES:
            return {
                ...state,
                userMessages: {
                    ...state.userMessages,
                    status: 'fetching',
                    data: (action.count === 0) ? [] : state.userMessages.data,
                }
            };
        case GET_USER_CHAT_SUCCESS: {
          const cloneData = cloneDeep(state.userMessages.data)
          return {
              ...state,
              userMessages: {
                  ...state.userMessages,
                  status: 'fetched',
                  data: cloneData.concat(action.data)
              }
          };
        }
        case ADD_USER_CHAT: {
            const cloneData = cloneDeep(state.userMessages.data)
            cloneData.push(action.data)
            return {
                ...state,
                userMessages: {
                    ...state.userMessages,
                    data: cloneData
                }
            }
        }
        case INIT_REDUCER:
            return initialState;
        default:
            return state
    }
};

export default collectionReducer
