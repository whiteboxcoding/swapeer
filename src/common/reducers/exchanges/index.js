/*
 *
 * exchanges reducer
 *
 */
import {
    GETTING_USER_MATCHES,
    GET_USER_MATCHES_SUCCESS,
    GETTING_USER_REQUESTS_LIST,
    GET_USER_REQUESTS_SUCCESS,
    GETTING_USER_DONATES,
    GET_USER_DONATES_SUCCESS,
    UPDATING_FOLLOW_STATUS,
    UPDATE_FOLLOW_STATUS_SUCCESS,
    GET_USER_REQUESTED_SWAPS_SUCCESS,
    GETTING_USER_REQUESTED_SWAPS,
    UPDATE_REQUEST_STATUS_SUCCESS,
    UPDATE_REQUEST_BOOK_STATUS_SUCCESS,
    GET_USER_REQUESTS_COUNT_SUCCESS
} from 'common/actionTypes'
import cloneDeep from 'lodash/cloneDeep'
import {INIT_REDUCER} from "../../actionTypes";

const initialState = {
    exchanges: {
        data: [],
        status: false,
        fetchedAll: false,
    },
    requests: {
        data: [],
        status: false,
        fetchedAll: false,
        count: 0,
    },
    donates: {
        data: [],
        status: false,
        fetchedAll: false,
    },
    followStatus: false,
    requestedSwaps: {
        status: false,
        data: []
    }
};

const collectionReducer = (state = initialState, action) => {
    switch (action.type) {
        case GETTING_USER_MATCHES:
            return {
                ...state,
                exchanges: {
                    ...state.exchanges,
                    status: 'fetching'
                }
            };
        case GET_USER_MATCHES_SUCCESS: {
          const cloneData = cloneDeep(state.exchanges.data)
            return {
                ...state,
                exchanges: {
                    ...state.exchanges,
                    data: (action.count === 0 ? action.exchanges : cloneData.concat(action.exchanges)),
                    status: 'fetched',
                    fetchedAll: (action.exchanges.length === 0)
                }
            }
        }
        case UPDATING_FOLLOW_STATUS: {
            return {
                ...state,
                followStatus: 'fetching'
            }
        }

        case UPDATE_FOLLOW_STATUS_SUCCESS: {
            const { data, viewType } = action;
            if (viewType === 'exchange') {
                const cloneData = cloneDeep(state.exchanges.data)
                const index = cloneData.findIndex((obj) => (obj.id === data.following_id))
                if (index >= 0) {
                    cloneData[index].following = data.follow
                }
                return {
                    ...state,
                    exchanges: {
                        ...state.exchanges,
                        data: cloneData
                    }
                }
            } else if (viewType === 'donate') {
                const cloneData = cloneDeep(state.donates.data)
                const index = cloneData.findIndex((obj) => (obj.id === data.following_id))
                if (index >= 0) {
                    cloneData[index].following = data.follow
                }
                return {
                    ...state,
                    donates: {
                        ...state.donates,
                        data: cloneData
                    }
                }
            }
            return state
        }
        case GETTING_USER_REQUESTS_LIST:
            return {
                ...state,
                requests: {
                    ...state.requests,
                    status: 'fetching'
                }
            };
        case GET_USER_REQUESTS_SUCCESS: {
          const cloneData = cloneDeep(state.requests.data)
            return {
                ...state,
                requests: {
                    ...state.requests,
                    data: (action.count === 0 ? action.requests : cloneData.concat(action.requests)),
                    status: 'fetched',
                    fetchedAll: (action.requests.length === 0)
                }
            };
        }
        case GETTING_USER_DONATES:
            return {
                ...state,
                donates: {
                    ...state.donates,
                    status: 'fetching'
                }
            };
        case GET_USER_DONATES_SUCCESS: {
          const cloneData = cloneDeep(state.donates.data)
            return {
                ...state,
                donates: {
                    ...state.donates,
                    status: 'fetched',
                    fetchedAll: (action.donates.length === 0),
                    data: (action.count === 0 ? action.donates : cloneData.concat(action.donates)),
                }
            };
        }
        case GET_USER_REQUESTED_SWAPS_SUCCESS:
            return {
                ...state,
                requestedSwaps: {
                    ...state.requestedSwaps,
                    data: action.data,
                    status: 'fetched'
                }
            };
        case GETTING_USER_REQUESTED_SWAPS:
            return {
                ...state,
                requestedSwaps: {
                    ...state.requestedSwaps,
                    status: 'fetching'
                }
            };
        case UPDATE_REQUEST_STATUS_SUCCESS: {
            const cloneData = cloneDeep(state.requests.data)
            const reqIndex = cloneData.findIndex(obj => (obj.id === action.data.request_id))
            if (reqIndex >= 0) {
                cloneData[reqIndex].status = action.data.status
            }
            return {
                ...state,
                requests: {
                    ...state.requests,
                    data: cloneData
                }
            };
        }
        case GET_USER_REQUESTS_COUNT_SUCCESS: {
            return {
                ...state,
                requests: {
                    ...state.requests,
                    count: action.count
                }
            };
        }
        case UPDATE_REQUEST_BOOK_STATUS_SUCCESS: {
            const cloneData = cloneDeep(state.requests.data)
            const reqIndex = cloneData.findIndex(obj => (obj.id === action.data.request_id))
            if (reqIndex >= 0) {
                if (action.data.book_status === 'Book received') {
                    cloneData.splice(reqIndex, 1)
                } else {
                    cloneData[reqIndex].book_status = action.data.book_status
                }
            }
            return {
                ...state,
                requests: {
                    ...state.requests,
                    data: cloneData
                }
            };
        }
        case INIT_REDUCER:
            return initialState;
        default:
            return state
    }
};

export default collectionReducer
