/*
 *
 * Login reducer
 *
 */
import {
	LOGIN_SUCCESS,
	PROCEED_TO_LOGOUT,
	LOGIN_FAILURE,
	UPDATE_VIEW_TYPE,
	START_LOGIN_WITH_FACEBOOK,
    PROCESSING_MANUAL_SIGNUP,
    PROCESSING_MANUAL_LOGIN,
    MANUAL_SIGNUP_SUCCESS,
    MANUAL_LOGIN_SUCCESS,
    GET_USER_DATA_SUCCESS,
    GETTING_USER_DETAILS,
    GET_USER_DETAILS_SUCCESS,
    LOGOUT,
    UPLOAD_PROFILE_IMAGE_SUCCESS,
    UPLOAD_COVER_IMAGE_SUCCESS,
    MANUAL_SIGNUP_FAILURE,
	INIT_REDUCER,
    UPDATE_FIRST_TIME_USER
} from '../../actionTypes'
import cloneDeep from 'lodash/cloneDeep'
import { getUserTokenFromCookie } from '../../helpers/cookie';

const initialState = {
	loggedIn: getUserTokenFromCookie(),
	viewType: 'login',
	loginStatus: true,
	userData: {},
	userDetails: {
		status: false,
		data: {}
	},
	processingManualSignup: false,
    processingManualLogin: false,
	profileImage: '',
	coverImage: ''
};

const loginReducer = (state = initialState, action) => {
	switch (action.type) {
	case LOGIN_SUCCESS:
		return {
			...state,
			loggedIn: true,
			loginStatus: 'fetched'
		};
	case START_LOGIN_WITH_FACEBOOK:
		return {
			...state,
			loginStatus: 'fetching'
		};
	case UPDATE_VIEW_TYPE:
		return {
			...state,
			viewType: action.viewType
		};
	case PROCESSING_MANUAL_SIGNUP:
		return {
			...state,
            processingManualSignup: true,
		};
	case PROCESSING_MANUAL_LOGIN:
		return {
            ...state,
            processingManualLogin: true,
		};
	case MANUAL_SIGNUP_SUCCESS:
		return {
			...state,
            processingManualSignup: false,
		};
	case MANUAL_SIGNUP_FAILURE:
		return {
			...state,
			processingManualSignup: false,
		};
	case MANUAL_LOGIN_SUCCESS:
		return {
			...state,
			processingManualLogin: false,
            loggedIn: true
		};
	case GETTING_USER_DETAILS:
		return {
			...state,
            userDetails: {
				...state.userDetails,
                status: 'fetching',
            }
		};
	case GET_USER_DETAILS_SUCCESS:
        return {
            ...state,
            userDetails: {
                ...state.userDetails,
                status: 'fetched',
                data: action.data
            }
        };
	case UPLOAD_PROFILE_IMAGE_SUCCESS:
		return {
			...state,
            profileImage: action.data
		};
	case UPLOAD_COVER_IMAGE_SUCCESS:
        return {
            ...state,
            coverImage: action.data
        };
	case GET_USER_DATA_SUCCESS:
		return {
			...state,
            userData: action.data
		};
	case UPDATE_FIRST_TIME_USER: {
		const cloneData = cloneDeep(state.userData)
        cloneData.firstTimeUser = false;
		return {
			...state,
            userData: cloneData
		}
	}
	case PROCEED_TO_LOGOUT:
	case LOGIN_FAILURE:
	case LOGOUT:
		return {
			...state,
			loggedIn: false
		};
	case INIT_REDUCER:
		return initialState;
	default:
		return state
	}
}

export default loginReducer
