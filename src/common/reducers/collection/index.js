/*
 *
 * collection reducer
 *
 */
import {
  GETTING_BOOKS_OF_COLLECTION,
  GETTING_USER_COLLECTION,
  GET_USER_COLLECTION_SUCCESS,
} from 'common/actionTypes'
import cloneDeep from 'lodash/cloneDeep'
import uniqBy from 'lodash/uniqBy'
import {INIT_REDUCER} from "../../actionTypes";

const initialState = {
  collection: [],
  addBookState: false,
  getBookState: false,
  getCollection: false,
}

const collectionReducer = (state = initialState, action) => {
	switch (action.type) {
      case GETTING_BOOKS_OF_COLLECTION:
        return {
          ...state,
          getBookState: 'fetching'
        };
      case GETTING_USER_COLLECTION:
        return {
          ...state,
          getCollection: 'fetching',
          collection: []
        };
      case GET_USER_COLLECTION_SUCCESS: {
        const cloneData = cloneDeep(state.collection)
        return {
          ...state,
          getCollection: 'fetched',
          collection: uniqBy(cloneData.concat(action.book), 'category')
        };
      }
      case INIT_REDUCER:
        return initialState;
  default:
    return state
  }
};

export default collectionReducer
