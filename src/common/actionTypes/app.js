export const HEADER_MENU_CHANGE = 'HEADER_MENU_CHANGE';
export const RECEIVED_NEW_USER_DATA = 'RECEIVED_NEW_USER_DATA';
export const RECEIVING_NEW_USER_DATA = 'RECEIVING_NEW_USER_DATA';
export const SHOW_MESSAGE_POPUP = 'SHOW_MESSAGE_POPUP';
export const HIDE_MESSAGE_POPUP = 'HIDE_MESSAGE_POPUP';
export const RECEIVED_USER_PROFILE_SEARCH = 'RECEIVED_USER_PROFILE_SEARCH';
export const RECEIVING_PROFILES_DATA = 'RECEIVING_PROFILES_DATA';
export const RECEIVED_PROFILES_DATA = 'RECEIVED_PROFILES_DATA';
export const RESET_PROFILES_DATA = 'RESET_PROFILES_DATA';
export const INIT_REDUCER = 'INIT_REDUCER';
export const UPDATE_FIRST_TIME_USER = 'UPDATE_FIRST_TIME_USER';
export const OPEN_SEARCH_BAR = 'OPEN_SEARCH_BAR';
export const CLOSE_SEARCH_BAR = 'CLOSE_SEARCH_BAR';
export const SAVE_READERS_STATS = 'SAVE_READERS_STATS';
