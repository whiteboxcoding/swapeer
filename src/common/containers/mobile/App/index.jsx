/**
 * @flow
 */
import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import { Menu, Segment, Header, Button, Sidebar } from 'semantic-ui-react'
import { selectLoginState, selectCurrentPath } from 'common/selectors'
import BookSearchPusher from 'components/BookSearchPusher'
import MenuSideBar from 'components/MenuSideBar'
import requiresAuth from 'common/hoc/requiresAuth'
import { tabNameToPathMap } from 'common/constants'

import './App.scss'

type Props = {
	login: Object,
	location: Object,
	history: Object,
	children: React$Node,
	pathname: String,
}

class App extends PureComponent<Props> {
	constructor () {
		super()
		this.state = {
			showSearchbar: false,
			showMenuBar: false
		}
		this.updateRoute = this.updateRoute.bind(this)
		this.showSearchbar = this.showSearchbar.bind(this)
		this.hideSearchbar = this.hideSearchbar.bind(this)
		this.showMenuBar = this.showMenuBar.bind(this)
		this.hideMenuBar = this.hideMenuBar.bind(this)
	}
	updateRoute (e, { name }) {
		this.props.history.push(tabNameToPathMap('1234')[name])
	}
	showSearchbar () {
		this.setState({
			showSearchbar: true
		})
	}
	hideSearchbar () {
		this.setState({
			showSearchbar: false
		})
	}

	showMenuBar () {
		this.setState({
			showMenuBar: true
		})
	}
	hideMenuBar () {
		this.setState({
			showMenuBar: false
		})
	}
	render () {
		const { login, pathname } = this.props
		const { loggedIn } = login
		return (
			<MenuSideBar hideMenuSideBar={this.hideMenuBar} show={this.state.showMenuBar} pathname={pathname} updateRoute={this.updateRoute}>
				<BookSearchPusher hidePusher={this.hideSearchbar} show={this.state.showSearchbar}>
					<Menu pointing secondary>
						<Menu.Item>
							<Button className='menuBtn' onClick={this.showMenuBar} inverted primary icon='bars' />
						</Menu.Item>
						<Menu.Item>
							<Header as={'h3'}>Swapeer</Header>
						</Menu.Item>
						{
							!loggedIn
								? <Menu.Menu position='right'>
									<Menu.Item
										name='Login'
										active={!pathname || (pathname && pathname.includes('/login'))}
										onClick={this.updateRoute}
									/>
									<Menu.Item
										name='Sign up'
										active={pathname && pathname.includes('/signup')}
										onClick={this.updateRoute}
									/>
								</Menu.Menu>
								: <Menu.Menu position='right'>
									<Button.Group>
										<Button basic icon='power' />
										<Button primary icon='search' onClick={this.showSearchbar} />
									</Button.Group>
								</Menu.Menu>
						}
					</Menu>
					{this.props.children}
				</BookSearchPusher>
			</MenuSideBar>
		)
	}
}

const mapStateToProps = (state) => ({
	login: selectLoginState(state),
	pathname: selectCurrentPath(state)
})
const mapDispatchToProps = {}

export default withRouter(requiresAuth(connect(mapStateToProps, mapDispatchToProps)(App)))
