/**
 * @flow
 */
import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import { Button, Image, Menu, Card, Grid } from 'semantic-ui-react'
import BookCollection from 'components/BookCollection'
import Dashboard from 'components/mobile/Dashboard'
import { getQueryParamsInURL } from 'common/helpers'
import {
	loginWithFacebook,
	loginWithGoodReads,
	loginWithEmail
} from 'common/actions'

import './HomePage.scss'

type Props = {
  loginWithFacebook: Function,
  loginWithGoodReads: Function,
  loginWithEmail: Function,
  history: Object
}

class HomePage extends PureComponent<Props> {
	render () {
		const { history } = this.props
		const queryObj = getQueryParamsInURL(history.location.search)
		return (
			queryObj.collection
				? <BookCollection collection={queryObj.collection} />
				: <Dashboard />
		)
	}
}

const mapStateToProps = (state) => ({})
const mapDispatchToProps = {}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomePage))
