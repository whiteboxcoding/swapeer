// @flow
import React from 'react'
import { Grid, Image } from 'semantic-ui-react'

const NotFound = () => {
	return (
		<div style={{ background: '#eefafa', height: '90vh' }}>
			<Image style={{ width: '100%' }}centered src={'/images/404-error-page_animate.gif'} />
		</div>
	)
}

export default NotFound
