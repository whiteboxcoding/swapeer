/**
 * @flow
 */
import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import { change, formValueSelector, touch } from 'redux-form'
import get from 'lodash/get'
import { Grid, Button, Divider, Segment } from 'semantic-ui-react'
import { validEmail } from '../../validators'
import {
	loginWithFacebook,
	loginWithGoodReads,
	loginWithEmail,
    signupWithEmail,
	processFinalLogin,
    processManualLogin,
    processManualSignup,
    getUserData,
    uploadProfileImage,
    searchBookByName,
    showMessagePopup,
    submitForgotPassword,
    resetUserPassword
} from '../../actions'
import { selectLoginState, selectSearchedBooksState } from '../../selectors'
import ExtraLoginInfoForm from '../../components/ExtraLoginInfoForm'
import ReadingHabitForm from '../../components/ReadingHabitForm'
import SigninForm from '../../components/SigninForm'
import SignupForm from '../../components/SignupForm'
import ResetPasswordForm from '../../components/ResetPasswordForm';
import { Wrapper, SignupButton } from './style.jsx'
import { getQueryParamsInURL } from '../../helpers'
import config from "../../../../config/environments";

type Props = {
	loginWithFacebook: Function,
	loginWithGoodReads: Function,
	loginWithEmail: Function,
    signupWithEmail: Function,
    processFinalLogin: Function,
    processManualLogin: Function,
    processManualSignup: Function,
    uploadProfileImage: Function,
    getUserData: Function,
    changeLocationValue: Function,
    searchBookByName: Function,
    showMessagePopup: Function,
    submitForgotPassword: Function,
    resetUserPassword: Function,
    signInFormVal: String,
	login: Object
}

class LoginPage extends PureComponent<Props> {
	constructor () {
		super();
        this.loginData = {}
        this.location = {}
        this.resetPasswordEmail = '';
		this.onLoginFormSubmit = this.onLoginFormSubmit.bind(this);
		this.getLoginView = this.getLoginView.bind(this);
		this.saveLatLngValue = this.saveLatLngValue.bind(this);
		this.saveReadingHabbitForm = this.saveReadingHabbitForm.bind(this);
		this.goToPrevStep = this.goToPrevStep.bind(this)
        this.skipStep = this.skipStep.bind(this)
        this.forgotPassword = this.forgotPassword.bind(this)
        this.goBackToLogin = this.goBackToLogin.bind(this)
        this.onResetPasswordSubmit = this.onResetPasswordSubmit.bind(this)
	}

	componentDidMount() {
        const { location } = this.props;
        const queryObj = getQueryParamsInURL(location.search)
        if (queryObj.readingHabits) {
            this.props.history.push({
                search: '?readingSpace=true'
            })
        }
        if (queryObj.forgotpassword && !this.resetPasswordEmail) {
            this.props.history.push({
                search: ''
            })
        }
    }

    onResetPasswordSubmit (data) {
	    if (data.password === data.retryPassword) {
	        delete data.retryPassword;
	        data.email = this.resetPasswordEmail;
            this.props.resetUserPassword(data)
        } else {
	        this.props.showMessagePopup('Password are not matching, Please try again')
        }
    }

    forgotPassword(e) {
	    const { signInFormVal } = this.props;
	    e.preventDefault();
	    this.props.forgotPassword();
	    if (signInFormVal && !validEmail(signInFormVal)) {
	        this.resetPasswordEmail = signInFormVal;
	        this.props.submitForgotPassword(signInFormVal)
        }
    }

    goBackToLogin (e) {
	    e.preventDefault()
        this.props.history.push({
            search: ''
        })
    }

    skipStep(e) {
	    e.preventDefault()
        e.stopPropagation()
        this.props.processFinalLogin(this.loginData)
    }

	onLoginFormSubmit (data) {
        const { login = {} } = this.props
        const { userData = {} } = login;
        this.loginData = {
            ...data,
            id: userData.id,
            location: this.location,
            address: this.address,
            profile_pic: login.profileImage || ''
        };
        this.props.history.push({
            search: '?readingHabits=true'
        })
	}

    goToPrevStep() {
        this.props.history.push({
            search: '?readingSpace=true'
        })
    }

    saveLatLngValue (latLngData, address) {
		this.location = {
			type : 'Point',
			coordinates : [ latLngData.lat ,latLngData.lng]
		}
		this.address = address
    }

    saveReadingHabbitForm(data) {
        this.props.processFinalLogin({
			...this.loginData,
			...data
		})
	}

	getLoginView ({ viewType, processingManualSignup, processingManualSignin, userData, profileImage }) {
		const { location, searchedBooks, favoriteBooks = [] } = this.props;
		const queryObj = getQueryParamsInURL(location.search)
		if (queryObj.readingHabits) {
			return (
                <Segment basic padded='very'>
                    <ReadingHabitForm
                        onSubmit={this.saveReadingHabbitForm}
                        initialValue={get(userData, 'user_profile', {})}
                        favoriteBooks={favoriteBooks}
                        searchBookByName={this.props.searchBookByName}
                        searchedBooks={searchedBooks.data}
                        searchQuery={searchedBooks.searchQuery}
                        goToPrevStep={this.goToPrevStep}
                        skipStep={this.skipStep}
                        showMessagePopup={this.props.showMessagePopup}
                    />
                </Segment>
			)
		} else if (queryObj.readingSpace) {
			return (
                <Segment basic padded>
                    <ExtraLoginInfoForm
                        getUserData={this.props.getUserData}
                        changeLocationValue={this.props.changeLocationValue}
                        saveLatLngValue={this.saveLatLngValue}
                        onSubmit={this.onLoginFormSubmit}
                        uploadProfileImage={this.props.uploadProfileImage}
                        initialValues={userData}
                        profileImage={profileImage}
                    />
                </Segment>
			)
		} else if (queryObj.forgotpassword) {
            return (
                <Segment basic padded>
                    <ResetPasswordForm
                        onSubmit={this.onResetPasswordSubmit}
                        goBackToLogin={this.goBackToLogin}
                    />
                </Segment>
            )
        } else {
            switch (viewType) {
                case 'login':
                    return (
                        location.pathname === '/signup'
                            ?  (
                                <Wrapper columns={1} verticalAlign='middle' textAlign='center'>
                                    <Grid.Column>
                                        <a href={`${config.frontendBase}/auth/facebook`}>
                                            <SignupButton color='blue'>Sign up using Facebook</SignupButton>
                                        </a>
                                        <Divider hidden />
                                        <a href={`${config.frontendBase}/auth/goodreads`}>
                                            <SignupButton color='brown'>Sign up using GoodReads</SignupButton>
                                        </a>
                                        <Divider hidden />
                                        <SignupButton color='black' onClick={this.props.signupWithEmail}>Sign up using Email</SignupButton>
                                    </Grid.Column>
                                </Wrapper>
                            ) : (
                                <Wrapper columns={1} verticalAlign='middle' textAlign='center'>
                                    <Grid.Column>
                                        <a href={`${config.frontendBase}/auth/facebook`}>
                                            <SignupButton color='blue'>Sign in using Facebook</SignupButton>
                                        </a>
                                        <Divider hidden />
                                        <a href={`${config.frontendBase}/auth/goodreads`}>
                                            <SignupButton color='brown'>Sign in using GoodReads</SignupButton>
                                        </a>
                                        <Divider hidden />
                                        <SignupButton color='black' onClick={this.props.loginWithEmail}>Sign in using Email</SignupButton>
                                    </Grid.Column>
                                </Wrapper>
                            )
                    )
                case 'signinWithEmail':
                    return (
                        <SigninForm forgotPassword={this.forgotPassword} loading={processingManualSignin} onSubmit={this.props.processManualLogin} />
                    );
                case 'signupWithEmail':
                    return (
                        <SignupForm loading={processingManualSignup} onSubmit={this.props.processManualSignup} />
                    );
                default:
                    return (
                        <Wrapper columns={1} verticalAlign='middle' textAlign='center'>
                            <Grid.Column>
                                <a href={`${config.frontendBase}/auth/facebook`}>
                                    <SignupButton color='blue'>Sign up using Facebook</SignupButton>
                                </a>
                                <Divider hidden />
                                <a href={`${config.frontendBase}/auth/goodreads`}>
                                    <SignupButton color='brown'>Sign up using GoodReads</SignupButton>
                                </a>
                                <Divider hidden />
                                <SignupButton color='black' onClick={this.props.signupWithEmail}>Sign up using Email</SignupButton>
                            </Grid.Column>
                        </Wrapper>
                    )
            }
		}
	}
	render () {
		const { login } = this.props
		return this.getLoginView(login)
	}
}

const mapStateToProps = (state) => ({
	login: selectLoginState(state),
    favoriteBooks: formValueSelector('readingHabit')(state, 'bookmark_use'),
    signInFormVal: formValueSelector('signinForm')(state, 'email'),
    searchedBooks: selectSearchedBooksState(state),
});

const mapDispatchToProps = {
	loginWithFacebook,
	loginWithGoodReads,
	loginWithEmail,
    signupWithEmail,
	processFinalLogin,
    processManualLogin,
    uploadProfileImage,
    getUserData,
    searchBookByName,
    submitForgotPassword,
    resetUserPassword,
    forgotPassword: () => (dispatch) => dispatch(touch('signinForm', 'email')),
    changeLocationValue: (data) => (dispatch) => dispatch(change('extraInfoForm', 'address', data)),
    processManualSignup,
    showMessagePopup
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginPage))
