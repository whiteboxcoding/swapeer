import styled from 'styled-components'
import { PrimeryButton } from '../../styles/common';

import { Grid, Button } from 'semantic-ui-react'

export const Wrapper = styled(Grid)`
    height: 93%;
`;

export const SignupButton = styled(PrimeryButton)`
    font-size: 1.5rem;
    width: 26rem;
`;
