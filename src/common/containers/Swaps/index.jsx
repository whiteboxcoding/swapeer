/**
 * @flow
 */
import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import { Menu } from 'semantic-ui-react'
import qs from 'qs'
import { getQueryParamsInURL } from 'common/helpers'
import Exchange from '../../components/Exchange'
import Donates from '../../components/Donates'
import Requests from '../../components/Requests'
import Messages from '../../components/Messages'
import {
	getUserMatchesList,
	getUserRequestsList,
	getUserDonatesList,
	getChatMessages,
	sendChatMessage,
	updateFollowStatus,
	getUserDetails,
	requestUserBookSwap,
	requestForGiveaway,
    getUserRequestedSwaps,
    updateRequestBookStatus,
    updateRequestStatus,
    saveNewMessages,
    getUserRequestCount
} from '../../actions'
import {
	selectExchangeState,
	selectUserChat,
	selectUserDetailsState,
	selectRequestedSwaps,
    selectRequestsState,
    selectDonatesState
} from '../../selectors'

type Props = {
	history: Object,
	location: Object,
    getUserMatchesList: Function,
    requestUserBookSwap: Function,
	getUserRequestsList: Function,
    getUserDonatesList: Function,
    getChatMessages: Function,
    sendChatMessage: Function,
    updateFollowStatus: Function,
    getUserDetails: Function,
    requestForGiveaway: Function,
    getUserRequestedSwaps: Function,
    updateRequestBookStatus: Function,
    updateRequestStatus: Function,
    saveNewMessages: Function,
    getUserRequestCount: Function,
    match: Object,
    exchanges: Object,
    userChat: Object,
    userDetails: Object,
    requestedSwaps: Object,
}

class Swaps extends PureComponent<Props> {
	constructor () {
		super()
		this.updateRoute = this.updateRoute.bind(this)
		this.updatePageUrl = this.updatePageUrl.bind(this)
	}

	componentDidMount() {
        const { match } = this.props;
        if (match.params.user_id) {
            this.props.getUserRequestCount();
		}
    }

	updateRoute (e, data) {
		this.props.history.replace({
			search: qs.stringify({
				tab: data.name.toLowerCase()
			})
		})
	}

    updatePageUrl (pathname, queryObj) {
		const obj = {};
		if (pathname) {
            obj.pathname = pathname;
		}
		if (queryObj) {
            obj.search = qs.stringify(queryObj)
		}
		this.props.history.push(obj)
	}

	getChildComponent ({ tab, userId }) {
		const { match, exchanges, donates, requests, userChat, userDetails, requestedSwaps } = this.props;
		switch (tab) {
		case 'exchange':
			return (
				<Exchange
					params={match.params}
					getUserMatchesList={this.props.getUserMatchesList}
                    exchanges={exchanges}
                    userDetails={userDetails}
                    getUserRequestedSwaps={this.props.getUserRequestedSwaps}
                    requestedSwaps={requestedSwaps}
                    requestUserBookSwap={this.props.requestUserBookSwap}
                    getUserDetails={this.props.getUserDetails}
                    updateFollowStatus={this.props.updateFollowStatus}
				/>
			);
		case 'donate':
			return (
				<Donates
					params={match.params}
                    requestForGiveaway={this.props.requestForGiveaway}
					getUserDonatesList={this.props.getUserDonatesList}
					donates={donates}
                    userDetails={userDetails}
                    getUserDetails={this.props.getUserDetails}
                    updateFollowStatus={this.props.updateFollowStatus}
				/>
			);
		case 'requests':
			return (
				<Requests
                    params={match.params}
                    getUserRequestsList={this.props.getUserRequestsList}
                    requests={requests}
                    updateRequestStatus={this.props.updateRequestStatus}
                    updateBookStatus={this.props.updateRequestBookStatus}
				/>
			);
		case 'messages':
			return (
				<Messages
					params={match.params}
					userChat={userChat}
					exchanges={exchanges}
					donates={donates}
					userId={userId}
					userDetails={userDetails}
                    requestedSwaps={requestedSwaps}
					sendChatMessage={this.props.sendChatMessage}
					getUserDetails={this.props.getUserDetails}
					getChatMessages={this.props.getChatMessages}
					saveNewMessages={this.props.saveNewMessages}
					getUserDonatesList={this.props.getUserDonatesList}
					getUserMatchesList={this.props.getUserMatchesList}
					updatePageUrl={this.updatePageUrl}
                    getUserRequestedSwaps={this.props.getUserRequestedSwaps}
				/>
			)
		default:
			return null;
		}
	}
	render () {
		const { location } = this.props
		return (
			<div>
				<Menu size={'massive'} pointing secondary>
					<Menu.Item name='Exchange' onClick={this.updateRoute} active={location.search.includes('exchange')} />
                    <Menu.Item name='Donate' onClick={this.updateRoute} active={location.search.includes('donate')} />
					<Menu.Item name='Requests' onClick={this.updateRoute} active={location.search.includes('requests')} />
					<Menu.Item name='Messages' onClick={this.updateRoute} active={location.search.includes('messages')} />
				</Menu>
				{this.getChildComponent(getQueryParamsInURL(location.search))}
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	exchanges: selectExchangeState(state),
	donates: selectDonatesState(state),
	requests: selectRequestsState(state),
	userChat: selectUserChat(state),
	userDetails: selectUserDetailsState(state),
	requestedSwaps: selectRequestedSwaps(state),
});

const mapDispatchToProps = {
    getUserMatchesList,
    getUserRequestsList,
    getUserDonatesList,
    getChatMessages,
    sendChatMessage,
    updateFollowStatus,
    getUserDetails,
    requestUserBookSwap,
    requestForGiveaway,
    getUserRequestedSwaps,
    updateRequestBookStatus,
    updateRequestStatus,
    saveNewMessages,
    getUserRequestCount,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Swaps))
