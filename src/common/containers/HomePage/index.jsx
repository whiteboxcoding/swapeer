/**
 * @flow
 */
import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import { change, submit } from 'redux-form'
import {withRouter} from 'react-router'
import { Button, Image, Menu, Card, Grid } from 'semantic-ui-react'
import BookCollection from '../../components/BookCollection'
import Dashboard from '../../components/Dashboard'
import NonLoggedinHomepage from '../../components/NonLoggedinHomepage'
import DemoCarousel from '../../components/DemoCarousel';
import DataLoadingScreen from '../../components/DataLoadingScreen';
import { getQueryParamsInURL } from '../../helpers';
import get from 'lodash/get';
import {
    selectUserCollections,
    selectCollectionsBook,
    selectLoginState,
    selectAddBookImage,
    selectUserWishlist,
    selectNewUserData,
    selectFollowers,
    selectFollowings,
    selectUserBooksState,
    selectUserDetailsState,
    selectSearchedUsersData,
    selectSearchedBooksState,
    selectUserBookHistory,
    selectReadersStats
} from '../../selectors'
import qs from 'qs'
import {
	loginWithFacebook,
	loginWithGoodReads,
	loginWithEmail,
  getCollectionBooks,
  getUserCollection,
    getUserWishlist,
    getFirstTimeUserData,
    saveUploadedFile,
    updateNewUserData,
    addBookToWishlist,
    fetchFollowersUsers,
    fetchFollowingUsers,
    getUserDetails,
    getUserBooksList,
    removeFromWishlist,
    updateBookStatus,
    updateFollowStatus,
    removeUploadImage,
    getUsersList,
    requestUserBookSwap,
    requestForGiveaway,
    getUserBookHistory,
    updateViewType,
    openSearchBar,
    getReeadersStats,
    uploadProfileImage,
    uploadCoverPicImage,
    updateUserProfile,
    removeBookFromCollection,
    showMessagePopup,
    getUserRequestCount
} from '../../actions'
import { getUserTokenFromCookie } from "../../helpers/cookie";


type Props = {
  loginWithFacebook: Function,
  loginWithGoodReads: Function,
  loginWithEmail: Function,
  getCollectionBooks: Function,
  getUserCollection: Function,
    saveUploadedFile: Function,
    getUserWishlist: Function,
    getFirstTimeUserData: Function,
    updateNewUserData: Function,
    setCollectionInputVal: Function,
    fetchFollowersUsers: Function,
    fetchFollowingUsers: Function,
    updateFollowStatus: Function,
    removeUploadImage: Function,
    getUserDetails: Function,
  history: Object,
  collections: Array,
  books: Object,
  login: Object,
    bookImage: Object,
    newUserData: Object,
    addBookToWishlist: Function,
    followers: Object,
    followings: Object,
    getUserBooksList: Function,
    removeFromWishlist: Function,
    updateBookStatus: Function,
    getUsersList: Function,
    requestUserBookSwap: Function,
    requestForGiveaway: Function,
    getUserBookHistory: Function,
    updateViewType: Function,
    openSearchBar: Function,
    getReeadersStats: Function,
    uploadProfileImage: Function,
    uploadCoverPicImage: Function,
    updateUserProfile: Function,
    removeBookFromCollection: Function,
    showMessagePopup: Function,
    getUserRequestCount: Function,
    searchedBooks: Object
};

class HomePage extends PureComponent<Props> {
  constructor (props) {
    super();
    this.state = {
      closeModal: false
    };
    props.getFirstTimeUserData();
    this.closeDemoModal = this.closeDemoModal.bind(this)
    this.onCollectionFormSubmit = this.onCollectionFormSubmit.bind(this)
      this.handleMenuChange = this.handleMenuChange.bind(this)
      this.goToLoginView = this.goToLoginView.bind(this)
  }
  componentDidMount() {
      this.props.getUserRequestCount();
  }

  onCollectionFormSubmit (data) {
    this.props.history.push({
      search: qs.stringify({ collection: data.collection })
    })
  }

    closeDemoModal () {
      this.setState({
          closeModal: true
      })
    }

    handleMenuChange(e, { link }) {
      this.props.history.push({
          pathname: link
      })
    }
    goToLoginView(e, data) {
      this.props.history.push({
          pathname: '/login'
      })
		      this.props.updateViewType('login');
	  }
	render () {
		const {history, collections, books, login, bookImage, match, wishlist, newUserData, followers,
            followings, userBooksList, userDetails, searchedUser, searchedBooks, userBookHistory, readersStats } = this.props;
        const { loggedIn, userData = {} } = login;
        const { closeModal } = this.state;
		const queryObj = getQueryParamsInURL(history.location.search);
		return (
		    <div>
                {
                  loggedIn
                    ? (
                          <div>
                              {
                                newUserData.status !== 'fetched'
                                ? <DataLoadingScreen />
                                : (
                                  <div>
                                    {
                                        (get(userData, 'firstTimeUser', true) && !closeModal)
                                            ? (
                                                <div style={{ width: '80%', margin: '0 auto' }}>
                                                    <DemoCarousel
                                                        closeDemoModal={this.closeDemoModal}
                                                    />
                                                </div>
                                            ) : (
                                                <div>
                                                    {
                                                        queryObj.collection
                                                            ?
                                                              <div>
                                                                  {
                                                                      userData.id &&
                                                                      <BookCollection
                                                                          collection={queryObj.collection}
                                                                          params={match.params}
                                                                          userData={userData}
                                                                          getCollectionBooks={this.props.getCollectionBooks}
                                                                          saveUploadedFile={this.props.saveUploadedFile}
                                                                          removeUploadImage={this.props.removeUploadImage}
                                                                          books={books}
                                                                          bookImage={bookImage}
                                                                          wishlist={wishlist}
                                                                          updateBookStatus={this.props.updateBookStatus}
                                                                          removeFromWishlist={this.props.removeFromWishlist}
                                                                          getUserWishlist={this.props.getUserWishlist}
                                                                          addBookToWishlist={this.props.addBookToWishlist}
                                                                          openSearchBar={this.props.openSearchBar}
                                                                          removeBookFromCollection={this.props.removeBookFromCollection}
                                                                      />
                                                                  }
                                                              </div>
                                                            : <div>
                                                                {
                                                                    userData.id &&
                                                                    <Dashboard
                                                                        collections={collections}
                                                                        removeFromWishlist={this.props.removeFromWishlist}
                                                                        fetchFollowersUsers={this.props.fetchFollowersUsers}
                                                                        fetchFollowingUsers={this.props.fetchFollowingUsers}
                                                                        followers={followers}
                                                                        followings={followings}
                                                                        params={match.params}
                                                                        userData={userData}
                                                                        userDetails={userDetails}
                                                                        wishlist={wishlist}
                                                                        coverImage={this.props.login.coverImage}
                                                                        profileImage={this.props.login.profileImage}
                                                                        setCollectionInputVal={this.props.setCollectionInputVal}
                                                                        handleMenuChange={this.handleMenuChange}
                                                                        queryObj={queryObj}
                                                                        removeUploadImage={this.props.removeUploadImage}
                                                                        addBookToWishlist={this.props.addBookToWishlist}
                                                                        bookImage={bookImage}
                                                                        userBooksList={userBooksList}
                                                                        getUserDetails={this.props.getUserDetails}
                                                                        getUserWishlist={this.props.getUserWishlist}
                                                                        onCollectionFormSubmit={this.onCollectionFormSubmit}
                                                                        getUserBooksList={this.props.getUserBooksList}
                                                                        updateFollowStatus={this.props.updateFollowStatus}
                                                                        getUsersList={this.props.getUsersList}
                                                                        searchedUser={searchedUser}
                                                                        saveUploadedFile={this.props.saveUploadedFile}
                                                                        searchedBooks={searchedBooks}
                                                                        requestUserBookSwap={this.props.requestUserBookSwap}
                                                                        requestForGiveaway={this.props.requestForGiveaway}
                                                                        getUserBookHistory={this.props.getUserBookHistory}
                                                                        userBookHistory={userBookHistory}
                                                                        submitEditForm={this.props.submitEditForm}
                                                                        updateNewUserData={this.props.updateNewUserData}
                                                                        uploadProfileImage={this.props.uploadProfileImage}
                                                                        uploadCoverImage={this.props.uploadCoverPicImage}
                                                                        updateUserProfile={this.props.updateUserProfile}
                                                                        getUserCollection={this.props.getUserCollection}
                                                                        openSearchBar={this.props.openSearchBar}
                                                                        showMessagePopup={this.props.showMessagePopup}
                                                                    />
                                                                }
                                                            </div>
                                                    }
                                                </div>
                                            )
                                    }
                                  </div>
                                )
                              }
                          </div>
                      )
                      : <NonLoggedinHomepage
                          goToLoginView={this.goToLoginView}
                          getReeadersStats={this.props.getReeadersStats}
                          readersStats={readersStats}
                      />
                }
            </div>

		)
	}
}

const mapStateToProps = (state) => ({
  collections: selectUserCollections(state),
  books: selectCollectionsBook(state),
    wishlist: selectUserWishlist(state),
  login: selectLoginState(state),
  bookImage: selectAddBookImage(state),
    newUserData: selectNewUserData(state),
    followers: selectFollowers(state),
    followings: selectFollowings(state),
    userBooksList: selectUserBooksState(state),
    userBookHistory: selectUserBookHistory(state),
    userDetails: selectUserDetailsState(state),
    searchedUser: selectSearchedUsersData(state),
    searchedBooks: selectSearchedBooksState(state),
    readersStats: selectReadersStats(state),
});

const mapDispatchToProps = {
  getCollectionBooks,
  getUserCollection,
  saveUploadedFile,
    addBookToWishlist,
    fetchFollowersUsers,
    fetchFollowingUsers,
    getUserWishlist,
    getFirstTimeUserData,
    updateNewUserData,
    getUserDetails,
    getUserBooksList,
    removeFromWishlist,
    updateBookStatus,
    updateFollowStatus,
    getUsersList,
    removeUploadImage,
    requestUserBookSwap,
    requestForGiveaway,
    getUserBookHistory,
    updateViewType,
    openSearchBar,
    getReeadersStats,
    uploadProfileImage,
    uploadCoverPicImage,
    updateUserProfile,
    removeBookFromCollection,
    getUserRequestCount,
    showMessagePopup,
    submitEditForm: () => (dispatch) => dispatch(submit('editProfile')),
    setCollectionInputVal: (val) => (dispatch) => dispatch(change('addCollectionForm', 'collection', val))
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomePage))
