import styled from 'styled-components'
import { Modal, Dropdown } from 'semantic-ui-react'

export const BookModal = styled(Modal)`
    border-radius: 3.125rem;
`;

export const BookModalContent = styled(Modal.Content)`
    border-radius: 3.125rem;
`;

export const BookModalActions = styled(Modal.Actions)`
    border-bottom-left-radius: 3.125rem;
    border-bottom-right-radius: 3.125rem;
    padding: 1rem 4rem;
    border: none;
    background: white;
`;

export const StyledDropdown = styled(Dropdown)`
    .item {
        border: none;
    }
`;
