import compact from 'lodash/compact'
export const data = {
	logo: '/images/Readsnet.png',
	mainMenuItem: {
		beforeLogin: (pathname, cbs) => ([
            {
                name: 'Home',
                link: '/home',
				isActive: (!pathname || pathname.includes('/home')),
                onClick: cbs[0]
            },
            {
                name: 'ReadTreat',
                externalLink: 'https://medium.com/readtreats-by-readsnet',
                isActive: false,
            }
        ]),
		afterLogin: (pathname, cbs, id, isMobile) => (compact([
            {
                name: 'Home',
                link: `/home/collection/${id}`,
                isActive: (!pathname || pathname.includes('/home')),
                onClick: cbs[0]
            },
            !isMobile ? {
                name: 'Books',
                link: '/swaps',
                isActive: (pathname.includes('/swaps')),
                onClick: cbs[0]
            }: null,
            {
                name: 'ReadTreat',
                externalLink: 'https://medium.com/readtreats-by-readsnet',
                isActive: false,
            }
        ]))
	},
	rightMenu: {
		beforeLogin: (pathname, cbs) => {
			return ([
			{
				name: 'Login',
				isActive: (pathname && pathname.includes('/login')),
				onClick: cbs[0]
			},
			{
				name: 'Sign up',
				isActive: pathname && pathname.includes('/signup'),
				onClick: cbs[0]
			}
			])
		},
		afterLogin: (pathname, cbs) => ([
			{
				basic: true,
				icon: 'power',
				type: 'dropdown'
			},
			{
				primary: true,
				icon: 'search',
				onClick: cbs[0],
                type: 'button'
			}
		])
	}
};

export const logoutOptions = (logout, gotoRoute) => ([
    { key: 'About us', text: 'About us', value: 'About us', onClick: () => gotoRoute('https://medium.com/readtreats-by-readsnet', true) },
    { key: 'Authors Corner', text: 'Authors Corner', value: 'Authors Corner', onClick: () => gotoRoute('/authors-corner') },
    { key: 'Feature Request', text: 'Feature Request', value: 'Feature Request', onClick: () => gotoRoute('/feature/request') },
    { key: 'Privacy', text: 'Privacy Policy', value: 'Privacy', onClick: () => gotoRoute('/privacy/policy') },
    { key: 'Contact Us', text: 'Contact Us', value: 'Contact Us', onClick: () => gotoRoute('/contact-us') },
    { key: 'Logout', text: 'Logout', onClick: logout, value: 'Logout' },
]);
