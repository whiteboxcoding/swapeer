/**
 * @flow
 */

import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import { NavLink } from 'react-router-dom'
import { Menu, Header, Button, Message, Modal, Dropdown, Grid } from 'semantic-ui-react'
import BookSearchPusher from '../../components/BookSearchPusher'
import MessagePopup from '../../components/MessagePopup'
import SearchedBookModal from '../../components/SearchedBookModal'
import requiresAuth from '../../hoc/requiresAuth'
import { tabNameToPathMap, tabToPathMap } from 'common/constants'
import {
    updateViewType,
    getUserData,
    logoutUser,
    hideMessagePopup,
    openAddSearchedBookModal,
    getUserCollection,
    closeAddSearchedBookModal,
    saveUploadedFile,
    addBookToCollection,
    resetSearchedBookData,
    searchProfileByName,
    searchBookByName,
    resetSearchedProfileData,
    getUserWishlist,
    getCollectionBooks,
    fetchFollowersUsers,
    fetchFollowingUsers,
    getUserMatchesList,
    getUserDonatesList,
    getUserRequestsList,
    closeSearchBar,
    openSearchBar,
    getUserDetails,
    addBookToWishlist,
} from '../../actions'
import { data, logoutOptions } from './data'
import { getModalMountNode } from '../../helpers/functions'
import { BookModal, BookModalActions, BookModalContent, StyledDropdown } from "./style";
import { CloseBtn } from '../../styles/common'
import {change, submit} from "redux-form";
import get from 'lodash/get'
import {
    selectAddBookImage,
    selectUserCollections,
    selectSearchedProfileState,
    selectUserWishlist,
    selectCollectionsBook,
    selectFollowers,
    selectFollowings,
    selectLoginState,
    selectSearchedBookModal,
    selectCurrentPath,
    selectSearchedBooksState,
    selectMessagePopup,
    selectExchangeState,
    selectDonatesState,
    selectRequestsState,
    selectSearchBarState
} from "../../selectors";
import { getQueryParamsInURL } from '../../helpers'

type Props = {
	login: Object,
	location: Object,
    match: Object,
	history: Object,
  	searchedBooks: Object,
    searchedProfiles: Object,
	children: React$Node,
	pathname: String,
  	searchBookByName: Function,
    updateViewType: Function,
    getUserCollection: Function,
    resetSearchedBookData: Function,
    getUserData: Function,
    logoutUser: Function,
    hideMessagePopup: Function,
    openAddSearchedBookModal: Function,
    closeAddSearchedBookModal: Function,
    saveUploadedFile: Function,
    addBookToCollection: Function,
    clearSearchQueryData: Function,
    searchProfileByName: Function,
    resetSearchedProfileData: Function,
    getUserWishlist: Function,
    getCollectionBooks: Function,
    fetchFollowersUsers: Function,
    fetchFollowingUsers: Function,
    getUserMatchesList: Function,
    getUserDonatesList: Function,
    getUserRequestsList: Function,
    bookImage: Object,
    books: Object,
    followers: Object,
    followings: Object,
    closeSearchBar: Function,
    openSearchBar: Function,
    setCollectionInputVal: Function,
    getUserDetails: Function,
    submitCollectionForm: Function,
    addBookToWishlist: Function,
    searchBar: Boolean,
};

class App extends PureComponent<Props> {
	constructor () {
		super();
		this.state = {
			showSearchbar: false,
            type: 'collection'
		};
		this.updateRoute = this.updateRoute.bind(this);
		this.goToLoginView = this.goToLoginView.bind(this);
		this.showSearchbar = this.showSearchbar.bind(this);
		this.hideSearchbar = this.hideSearchbar.bind(this);
		this.getAppView = this.getAppView.bind(this);
		this.onCollectionFormSubmit = this.onCollectionFormSubmit.bind(this);
		this.updateRouteBeforeLogin = this.updateRouteBeforeLogin.bind(this);
		this.onAddBookModalClose = this.onAddBookModalClose.bind(this);
		this.onAddBookSubmit = this.onAddBookSubmit.bind(this);
		this.updatePageRoute = this.updatePageRoute.bind(this);
		this.onUserProfileClick = this.onUserProfileClick.bind(this)
        this.onSearchedBookModalOpen = this.onSearchedBookModalOpen.bind(this)
        this.onPageScrollEnds = this.onPageScrollEnds.bind(this)
        this.gotoRoute = this.gotoRoute.bind(this)
        this.onBookTypeChange = this.onBookTypeChange.bind(this)
        this.onAddBookClick = this.onAddBookClick.bind(this)
	}
	componentDidMount() {
		this.props.getUserData();
	}

    onBookTypeChange (e, { value }) {
	    this.setState({
            type: value
        })
    }

    gotoRoute(pathname, isExternal) {
	    if (isExternal) {
            const win = window.open(pathname, '_blank');
            win.focus();
        } else {
            this.props.history.push({
                pathname
            })
        }
    }

    onAddBookClick() {
	    const { type } = this.state;
	    if (type === 'collection') {
	        this.props.submitCollectionForm()
        } else {
	        const { searchedBookModal, login, location } = this.props;
            this.props.closeSearchBar()
            const userData = {
                user_id: get(login, 'userData.id', ''),
                category: 'wishlist',
                book: {
                    id: get(searchedBookModal, 'data.id', ''),
                    name: get(searchedBookModal, 'data.name', ''),
                    image: get(searchedBookModal, 'data.images.thumbnail', ''),
                    authors: get(searchedBookModal, 'data.authors', []),
                    averageRating: get(searchedBookModal, 'data.averageRating', 0)
                }
            };
            this.props.addBookToWishlist(userData)
            this.props.resetSearchedBookData();
            this.props.closeAddSearchedBookModal();
            setTimeout(() => this.props.getUserWishlist({ user_id: userData.id, count: 0 }), 0)
        }
    }

    onAddBookModalClose () {
        this.props.resetSearchedBookData();
        this.props.closeAddSearchedBookModal();
    }

    onCollectionFormSubmit(data) {
		const { searchedBookModal, login, location } = this.props;
        const paramObj = getQueryParamsInURL(location.search)
		this.props.closeSearchBar()
        const userData = {
            user_id: get(login, 'userData.id', ''),
            category: data.collection,
            book: {
                id: get(searchedBookModal, 'data.id', ''),
                name: get(searchedBookModal, 'data.name', ''),
                image: get(searchedBookModal, 'data.images.thumbnail', ''),
                authors: get(searchedBookModal, 'data.authors', []),
                averageRating: get(searchedBookModal, 'data.averageRating', 0)
            },
            match_type: 'giveaway'
        };
        this.props.addBookToCollection(userData);
		this.props.resetSearchedBookData();
		this.props.closeAddSearchedBookModal();
		if (data.collection !== paramObj.collection) {
		    this.props.history.push({
                pathname: location.pathname,
                search: ''
            });
            const arr = location.pathname.split('/')
            const userId = arr[arr.length - 1];
            setTimeout(() => this.props.getUserCollection({ user_id: userId, count: 0 }), 0)
        }
	}

	onAddBookSubmit(data, collection) {
        const { login } = this.props;
        this.props.closeSearchBar()
        const body = {
            user_id: get(login, 'userData.id', ''),
            category: collection,
            book: data,
            match_type: 'giveaway'
        };
        this.props.addBookToCollection(body);
    }

	updateRoute (e, { name }) {
        const { login } = this.props;
        const { userData = {} } = login;
		this.props.history.push(tabNameToPathMap(userData.id)[name])
	}

    updatePageRoute (data) {
        this.props.history.push(data)
    }

    updateRouteBeforeLogin (e, { name }) {
        this.props.history.push(tabToPathMap[name])
	}

    goToLoginView(e, data) {
		    this.updateRoute(e, data);
		      this.props.updateViewType('login');
	  }
	showSearchbar () {
        this.props.resetSearchedBookData();
        this.props.openSearchBar()
	}

  onSearchedBookModalOpen(book) {
    this.hideSearchbar()
    this.props.openAddSearchedBookModal(book)
  }

	hideSearchbar () {
        this.props.closeSearchBar()
        this.props.resetSearchedBookData();
		this.props.resetSearchedProfileData();
	}
    onUserProfileClick(profileId) {
        this.props.closeSearchBar()
        this.props.getUserDetails({
            user_id: profileId
        });
        this.props.getUserCollection({ user_id: profileId, count: 0 })
        this.props.history.push(`/home/collection/${profileId}`)
        this.props.resetSearchedProfileData();
    }

    onPageScrollEnds() {
        const { location, login, wishlist, books, followers, followings, exchanges, donates, requests } = this.props;
        const arr = location.pathname.split('/')
        const userId = arr[arr.length - 1];
        const { userData = {} } = login;
        if (location.pathname.includes('/home/wishlist')) {
            this.props.getUserWishlist({ user_id: userData.id, count: get(wishlist, `data.${userData.id}`, []).length })
            this.props.getUserWishlist({ user_id: userId, count: get(wishlist, `data.${userId}`, []).length })
        } else if (location.pathname.includes('/home/collection')) {
            const paramObj = getQueryParamsInURL(location.search)
            if (paramObj.collection) {
                if (userId !== userData.id) {
                    this.props.getUserWishlist({ user_id: userData.id, count: get(wishlist, `data.${userData.id}`, []).length })
                }
                if (!get(books, `${paramObj.collection}.fetchedAll`)) {
                    this.props.getCollectionBooks({ user_id: userId, category: paramObj.collection, count: get(books, `${paramObj.collection}.data`, []).length })
                }
            }
        } else if (location.pathname.includes('/home/followers')) {
            if (!followers.fetchedAll) {
                this.props.fetchFollowersUsers({ count: followers.data.length, user_id: userId })
            }
            if (!followings.fetchedAll) {
                this.props.fetchFollowingUsers({ count: followings.data.length, user_id: userId })
            }
        } else if (location.pathname.includes('/swaps')) {
          const paramObj = getQueryParamsInURL(location.search)
          if (paramObj.tab === 'exchange') {
              if (!get(exchanges, 'fetchedAll')) {
                  this.props.getUserMatchesList({
                      user_id: userId,
                      count: get(exchanges, 'data', []).length
                  })
              }
          } else if (paramObj.tab === 'donate') {
              if (!get(donates, 'fetchedAll')) {
                  this.props.getUserDonatesList({
                      user_id: userId,
                      count: get(donates, 'data', []).length
                  })
              }
          } else if (paramObj.tab === 'requests') {
              if (!get(requests, 'fetchedAll')) {
                  this.props.getUserRequestsList({
                      user_id: userId,
                      count: get(requests, 'data', []).length
                  });
              }
          }
        }
    }

    getName(name, count, externalLink) {
	    if (name === 'Books' && count > 0) {
	        return <span>Book <span style={{ fontSize: '1rem', verticalAlign: 'super' }}>{count}</span></span>
        } else if (externalLink) {
	        return <a style={{ lineHeight: 1 }} href={externalLink} target='_blank'>{name}</a>
        }
        return name
    }

	getAppView() {
        const { login, pathname, searchedBooks, searchedProfiles, location, bookImage, searchBar, requests } = this.props;
        const { loggedIn, userData = {} } = login;
		if (loggedIn) {
			return (
                <BookSearchPusher
                    hidePusher={this.hideSearchbar}
                    show={searchBar}
                    searchedBooks={searchedBooks}
                    searchedProfiles={searchedProfiles}
                    bookImage={bookImage}
                    location={location}
                    onAddBookSubmit={this.onAddBookSubmit}
                    onPageScrollEnds={this.onPageScrollEnds}
                    updateRoute={this.updatePageRoute}
                    resetSearchedBookData={this.props.resetSearchedBookData}
                    onUserProfileClick={this.onUserProfileClick}
                    resetSearchedProfileData={this.props.resetSearchedProfileData}
                    searchProfileByName={this.props.searchProfileByName}
                    saveUploadedFile={this.props.saveUploadedFile}
                    searchBookByName={this.props.searchBookByName}
                    getUserCollection={this.props.getUserCollection}
                    openAddSearchedBookModal={this.onSearchedBookModalOpen}
                    collections={this.props.collections}
                    setCollectionInputVal={this.props.setCollectionInputVal}
                    userId={get(login, 'userData.id', '')}
                    content={
                        <div>
                            <Grid columns={1}>
                                <Grid.Column only={'tablet computer'}>
                                    <Menu size={'massive'} pointing secondary style={{ margin: 0 }}>
                                        <Menu.Item>
                                            <NavLink to={`/home/collection/${userData.id}`}>
                                                <img style={{ width: '9rem' }} src={data.logo} />
                                            </NavLink>
                                        </Menu.Item>
                                        {
                                            data.mainMenuItem.afterLogin(pathname, [this.updateRoute], userData.id).map((menuItem, key) => (
                                                <Menu.Item
                                                    key={key}
                                                    content={this.getName(menuItem.name, requests.count, menuItem.externalLink)}
                                                    name={menuItem.name}
                                                    active={menuItem.isActive}
                                                    onClick={menuItem.onClick}
                                                />
                                            ))
                                        }
                                        <Menu.Menu position='right'>
                                            <Button.Group>
                                                {
                                                    data.rightMenu.afterLogin(pathname, [this.showSearchbar]).map((afterLogin, key) => (
                                                        (afterLogin.type === 'button')
                                                            ? <Button size='big' key={key} primary={afterLogin.primary} basic={afterLogin.basic} icon={afterLogin.icon} onClick={afterLogin.onClick} />
                                                            : (
                                                                <StyledDropdown
                                                                    text={' '}
                                                                    direction={'left'}
                                                                    style={{ paddingTop: '1.4rem' }}
                                                                    button
                                                                    key={key}
                                                                    className='icon'
                                                                    primary={afterLogin.primary}
                                                                    basic={afterLogin.basic}
                                                                    icon={{ name: afterLogin.icon, size: 'large' }}
                                                                    options={logoutOptions(this.props.logoutUser, this.gotoRoute)}
                                                                />
                                                            )
                                                    ))
                                                }
                                            </Button.Group>
                                        </Menu.Menu>
                                    </Menu>
                                </Grid.Column>
                                <Grid.Column only={'mobile'}>
                                    <Menu size={'massive'} pointing secondary style={{ margin: 0 }}>
                                        <Menu.Item>
                                            <NavLink to={`/home/collection/${userData.id}`}>
                                                <img style={{ width: '9rem' }} src={data.logo} />
                                            </NavLink>
                                        </Menu.Item>
                                        <Dropdown item text='Home'>
                                            <Dropdown.Menu>
                                                {
                                                    data.mainMenuItem.afterLogin(pathname, [this.updateRoute], userData.id, true).map((menuItem, key) => (
                                                        <Menu.Item
                                                            key={key}
                                                            content={this.getName(menuItem.name, requests.count, menuItem.externalLink)}
                                                            name={menuItem.name}
                                                            onClick={menuItem.onClick}
                                                        />
                                                    ))
                                                }
                                            </Dropdown.Menu>
                                        </Dropdown>
                                        <Menu.Item
                                            content={this.getName('Books', requests.count)}
                                            name={'Books'}
                                            active={pathname.includes('/swaps')}
                                            onClick={this.updateRoute}
                                        />
                                        <Menu.Menu position='right'>
                                            <Button.Group>
                                                {
                                                    data.rightMenu.afterLogin(pathname, [this.showSearchbar]).map((afterLogin, key) => (
                                                        (afterLogin.type === 'button')
                                                            ? <Button size='big' key={key} primary={afterLogin.primary} basic={afterLogin.basic} icon={afterLogin.icon} onClick={afterLogin.onClick} />
                                                            : (
                                                                <StyledDropdown
                                                                    text={' '}
                                                                    direction={'left'}
                                                                    style={{ paddingTop: '1.4rem' }}
                                                                    button
                                                                    key={key}
                                                                    className='icon'
                                                                    primary={afterLogin.primary}
                                                                    basic={afterLogin.basic}
                                                                    icon={{ name: afterLogin.icon, size: 'large' }}
                                                                    options={logoutOptions(this.props.logoutUser, this.gotoRoute)}
                                                                />
                                                            )
                                                    ))
                                                }
                                            </Button.Group>
                                        </Menu.Menu>
                                    </Menu>
                                </Grid.Column>
                            </Grid>
                            {this.props.children}
                        </div>
                    }
                />
			)
		}
		return (
            <div>
                <Grid columns={1}>
                    <Grid.Column only={'tablet computer'}>
                        <Menu size={'massive'} pointing secondary style={{ margin: 0 }}>
                            <Menu.Item>
                                <img style={{ width: '9rem' }} src={data.logo} />
                            </Menu.Item>
                            {
                                data.mainMenuItem.beforeLogin(pathname, [this.updateRouteBeforeLogin]).map((menuItem, key) => (
                                    <Menu.Item
                                        key={key}
                                        name={menuItem.name}
                                        content={this.getName(menuItem.name, requests.count, menuItem.externalLink)}
                                        active={menuItem.isActive}
                                        onClick={menuItem.onClick}
                                    />
                                ))
                            }
                            <Menu.Menu position='right'>
                                {
                                    data.rightMenu.beforeLogin(pathname, [this.goToLoginView]).map((beforeLogin, key) => (
                                        <Menu.Item
                                            key={key}
                                            name={beforeLogin.name}
                                            active={beforeLogin.isActive}
                                            onClick={beforeLogin.onClick}
                                        />
                                    ))
                                }
                            </Menu.Menu>
                        </Menu>
                    </Grid.Column>
                    <Grid.Column only={'mobile'}>
                        <Menu size={'massive'} pointing secondary style={{ margin: 0 }}>
                            <Menu.Item>
                                <img style={{ width: '9rem' }} src={data.logo} />
                            </Menu.Item>
                            <Dropdown item text='Home'>
                                <Dropdown.Menu>
                                    {
                                        data.mainMenuItem.beforeLogin(pathname, [this.updateRouteBeforeLogin]).map((menuItem, key) => (
                                            <Dropdown.Item
                                                key={key}
                                                name={menuItem.name}
                                                content={this.getName(menuItem.name, requests.count, menuItem.externalLink)}
                                                onClick={menuItem.onClick}
                                            />
                                        ))
                                    }
                                </Dropdown.Menu>
                            </Dropdown>
                            <Menu.Menu position='right'>
                                {
                                    data.rightMenu.beforeLogin(pathname, [this.goToLoginView]).map((beforeLogin, key) => (
                                        <Menu.Item
                                            key={key}
                                            name={beforeLogin.name}
                                            active={beforeLogin.isActive}
                                            onClick={beforeLogin.onClick}
                                        />
                                    ))
                                }
                            </Menu.Menu>
                        </Menu>
                    </Grid.Column>
                </Grid>
                {this.props.children}
            </div>
		)
	}
	render () {
		const { messagePopup, searchedBookModal, login } = this.props;
		return (
			<div>
				{
                    messagePopup.text &&
                    <MessagePopup hideMessagePopup={this.props.hideMessagePopup} data={messagePopup} />
				}
				{
                	this.getAppView()
				}
				{
                    searchedBookModal.open &&
					<BookModal
                        mountNode={getModalMountNode()}
                        open={searchedBookModal.open}
                        onClose={this.onAddBookModalClose}
                        closeOnDimmerClick
                        closeIcon={<CloseBtn><img style={{ width: '1.5rem' }} src={'/images/closeIcon.svg'} /></CloseBtn>}
                    >
						<BookModalContent>
							<SearchedBookModal
                                data={searchedBookModal.data}
                                userId={get(login, 'userData.id', '')}
                                onCollectionFormSubmit={this.onCollectionFormSubmit}
                                getUserCollection={this.props.getUserCollection}
                                setCollectionInputVal={this.props.setCollectionInputVal}
                                collections={this.props.collections}
                                onBookTypeChange={this.onBookTypeChange}
                                type={this.state.type}
                            />
						</BookModalContent>
                        <BookModalActions>
                            <Button primary onClick={this.onAddBookClick}>Add</Button>
                        </BookModalActions>
					</BookModal>
				}
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	login: selectLoginState(state),
	pathname: selectCurrentPath(state),
    searchedBooks: selectSearchedBooksState(state),
    searchedProfiles: selectSearchedProfileState(state),
	messagePopup: selectMessagePopup(state),
    searchedBookModal: selectSearchedBookModal(state),
    collections: selectUserCollections(state),
    bookImage: selectAddBookImage(state),
    wishlist: selectUserWishlist(state),
    books: selectCollectionsBook(state),
    followers: selectFollowers(state),
    followings: selectFollowings(state),
    exchanges: selectExchangeState(state),
  	donates: selectDonatesState(state),
  	requests: selectRequestsState(state),
    searchBar: selectSearchBarState(state),
});

const mapDispatchToProps = {
  searchBookByName,
    updateViewType,
    getUserData,
    logoutUser,
    hideMessagePopup,
    addBookToCollection,
    openAddSearchedBookModal,
    getUserCollection,
    resetSearchedBookData,
    searchProfileByName,
    closeAddSearchedBookModal,
    getUserWishlist,
    saveUploadedFile,
    resetSearchedProfileData,
    getCollectionBooks,
    fetchFollowersUsers,
    fetchFollowingUsers,
    getUserMatchesList,
    getUserDonatesList,
    getUserRequestsList,
    closeSearchBar,
    openSearchBar,
    getUserDetails,
    addBookToWishlist,
    submitCollectionForm: () => (dispatch) => dispatch(submit('addCollectionForm')),
    setCollectionInputVal: (val, form) => (dispatch) => dispatch(change(form, 'collection', val))
};

export default withRouter(requiresAuth(connect(mapStateToProps, mapDispatchToProps)(App)))
