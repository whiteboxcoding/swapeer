import Cookies from 'js-cookie';

export const getUserTokenFromCookie = () => Cookies.get('user-token');

export const setUserTokenInCookie = value => Cookies.set('user-token', value);

export const deleteUserTokenInCookie = () => {
  Cookies.remove('user-token');
  return true;
};
