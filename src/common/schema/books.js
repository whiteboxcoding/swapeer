import { schema } from 'normalizr'

const books = new schema.Entity('books')

export default books
