import { injectGlobal } from 'styled-components'

// NOTE: some styles are duplicated to make SSRed app looks better
const GlobalStyles = injectGlobal`
  html {
    font-size: 60%;
    @media only screen and (min-width: 600px) {
      font-size: 45%;
    }
    @media only screen and (min-width: 700px) {
      font-size: 55%;
    }
    @media only screen and (min-width: 800px) {
      font-size: 64%;
    }
    @media only screen and (min-width: 900px) {
      font-size: 72%;
    }
    @media only screen and (min-width: 1000px) {
      font-size: 80%;
    }
    @media only screen and (min-width: 1100px) {
      font-size: 90%;
    }
    @media only screen and (min-width: 1200px) {
      font-size: 100%;
    }
  }
`;

export default GlobalStyles
