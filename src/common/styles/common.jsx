import styled from 'styled-components'

import { Button } from 'semantic-ui-react'

export const PrimeryButton = styled(Button)`
    border-radius: 6.25rem;
    text-align: center;
`;

export const CloseBtn = styled.button`
    position: absolute;
    right: 2rem;
    top: 1rem;
    cursor: pointer;
    border: none;
    outline: none;
    z-index: 10;
    background: white;
`;

export const MobileCloseBtn = styled.button`
    position: absolute;
    right: -10px;
    top: -15px;
    cursor: pointer;
    border: none;
    outline: none;
    z-index: 10;
    background: white;
    padding: 4px;
`
