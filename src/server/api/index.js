import {Router} from 'express'
import request from 'request-promise'
import reqst from 'request'
import url from 'url';
import config from '../../../config/environments';

const router = Router();

const BASE_URL = config.server.base;

router.post('/search/book', (req, res) => {
  const { body } = req;
  request({
    method: 'POST',
    uri: `${BASE_URL}/book/search`,
    body: body.data,
    headers: {
      'x-auth-token': req.headers['x-auth-token'],
    },
    json: true // Automatically stringifies the body to JSON
  })
    .then((resp) => {
      res.send(resp)
    })
    .catch(function (err) {
      res.send(err)
        // POST failed...
    });
});

router.post('/user/chat', (req, res) => {
    const { body } = req
    request({
        method: 'POST',
        uri: `${BASE_URL}/messages`,
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/send/chat/message', (req, res) => {
    const { body } = req
    request({
        method: 'POST',
        uri: `${BASE_URL}/message/add`,
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/profile/update', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        uri: `${BASE_URL}/user/profile/add`,
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            req.session.user = body.data;
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/book/database/add', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        uri: `${BASE_URL}/book/add`,
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/user/matches/list', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        uri: `${BASE_URL}/user/matches/list`,
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/user/requests/list', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        uri: `${BASE_URL}/request/list`,
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/requests/count', (req, res) => {
    const { user = {} } = req.session;
    if (!user.id) {
        return res.send({ count: 0})
    }
    request({
        method: 'POST',
        uri: `${BASE_URL}/request/count`,
        body: { user_id: user.id},
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/user/donates/list', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        uri: `${BASE_URL}/user/giveaway/list`,
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/auth/signup', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        uri: `${BASE_URL}/auth/signup`,
        body: body.data,
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            if (resp.id) {
                req.session.user = {
                    id: resp.id,
                    email: body.data.email,
                    name: body.data.name,
                };
            }
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/auth/login', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        uri: `${BASE_URL}/auth/login`,
        body: body.data,
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            if (resp.id) {
                req.session.user = resp;
            }
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
});

router.post('/auth/logout', (req, res) => {
    req.session.user = {};
    res.send(true)
});

router.get('/firstTimeUser', (req, res) => {
    res.send(req.session.firstTimeUser || {})
});

router.post('/update/firstTimeUser', (req, res) => {
    req.session.user.firstTimeUser = false
    res.send({ status: true })
});

router.get('/get/userData', (req, res) => {
    if (req.session && req.session.user) {
        res.send({
            userData: req.session.user
        })
    } else {
        res.send({
            userData: {}
        })
    }
});

router.get('/login/facebook', (req, res) => {
  request({
    method: 'GET',
    uri: `${BASE_URL}/auth/facebook`
  })
    .then((resp) => {
      res.send(resp)
    })
    .catch(function (err) {
      res.send(err)
    });
})

router.get('/login/facebook', (req, res) => {
    request({
        method: 'GET',
        uri: `${BASE_URL}/auth/facebook`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/user/book/:type', (req, res) => {
  const { type } = req.params;
    const { body } = req;
    const { data = {} } = body;
    if (!data.user_id) {
        const { user = {} } = req.session;
        data.user_id = user.id;
    }
  request({
    method: 'POST',
    uri: `${BASE_URL}/user/book/${type}`,
      body: data,
      json: true,
    headers: {
      'x-auth-token': req.headers['x-auth-token'],
    },
  })
    .then((resp) => {
      res.send(resp)
    })
    .catch(function (err) {
      res.send(err)
    });
})

router.post('/book/:type/remove', (req, res) => {
    // body - {user_id, book, category, images}
    const { type } = req.params;
    const { body } = req;
    request({
        method: 'POST',
        uri: `${BASE_URL}/user/book/${type}/delete`,
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
            // console.log('ewrwer', resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
})

router.post('/update/user/book', (req, res) => {
    // body - {user_id, book, category, images}
    const { body } = req;
    request({
        method: 'POST',
        uri: `${BASE_URL}/user/book/collection/update`,
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
            // console.log('ewrwer', resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
})

router.post('/book/:type/add', (req, res) => {
    // body - {user_id, book, category, images}
    const { type } = req.params;
    const { body } = req;
    const { data = {} } = body;
    const { user = {} } = req.session;
    data.user_id = user.id;
    request({
        method: 'POST',
        uri: `${BASE_URL}/user/book/${type}/add`,
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        json: true // Automatically stringifies the body to JSON
    })
        .then((resp) => {
            res.send(resp)
            // console.log('ewrwer', resp)
        })
        .catch(function (err) {
            res.send(err)
            // POST failed...
        });
})

router.post('/user/book/collection/count', (req, res) => {
    const { body } = req;
  request({
    method: 'POST',
      json: true, // Automatically stringifies the body to JSON
      body: body.data,
    headers: {
      'x-auth-token': req.headers['x-auth-token'],
    },
    uri: `${BASE_URL}/user/book/collection/count`
  })
    .then((resp) => {
      res.send(resp)
    })
    .catch(function (err) {
      res.send(err)
    });
})

router.post('/user/followers', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/followers`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/update/follow/status', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    if (!data.user_id) {
        const { user = {} } = req.session;
        data.user_id = user.id;
    }
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/follow`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/user/history/book', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/history/book`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/request/user/book/swap', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    const { user = {} } = req.session;
    if (data.reverse) {
        data.user_from = {
            ...data.user_from,
            id: user.id,
            name: user.name,
            profile_pic: user.profile_pic,
        };
    } else {
        data.user_to = {
            ...data.user_to,
            id: user.id,
            name: user.name,
            profile_pic: user.profile_pic,
        };
    }
    delete data.reverse
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/request/add/swap`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/request/book/giveaway', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    const { user = {} } = req.session;
    if (data.reverse) {
        data.user_from = {
            ...data.user_from,
            id: user.id,
            name: user.name,
            profile_pic: user.profile_pic,
        };
    } else {
        data.user_to = {
            ...data.user_to,
            id: user.id,
            name: user.name,
            profile_pic: user.profile_pic,
        };
    }
    delete data.reverse
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/request/add/giveaway`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/user/followings', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/following`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.get('/external/login', (req, res) => {
    const urlParts = url.parse(req.url, true);
    const query = urlParts.query;
    query.firstTimeUser = (query.firstTimeUser === 'true')
    req.session.user = query;
    if (query.id) {
        res.cookie('user-token',query.token);
        if (query.firstTimeUser) {
            return res.redirect('/signup?readingSpace=true')
        }
        return res.redirect(`/home/collection/${query.id}`)
    }
    return res.redirect('/login')
});

router.post('/get/user/details', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/profile/card`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
});

router.get('/readers/stats', (req, res) => {
    request({
        method: 'GET',
        json: true, // Automatically stringifies the body to JSON
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/stats`
    })
        .then((resp) => {
            res.json(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
});

router.post('/get/user/books/list', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/book/search`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/get/swaps/list', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/matches/list`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/user/profile/search', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/profile/search`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/get/user/swaps', (req, res) => {
    const { body } = req;
    const { data = {} } = body;
    if (!data.user_id) {
        const { user = {} } = req.session;
        data.user_id = user.id;
    }
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/request/user/swap`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/update/request/status', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/request/update/status`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

router.post('/update/book/status', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/request/update/book/status`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
});

router.post('/search/user/profile', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/profile/search`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
})

// router.post('/user/book/wishlist', (req, res) => {
//     // body - {user_id, count}
//     const { body } = req;
//     console.log('bodybodybody', body);
//     request({
//         method: 'POST',
//         uri: `${BASE_URL}/user/book/wishlist`,
//         body: body.data,
//         headers: {
//             'x-auth-token': req.headers['x-auth-token'],
//         },
//         json: true // Automatically stringifies the body to JSON
//     })
//         .then((resp) => {
//             res.send(resp)
//             // console.log('ewrwer', resp)
//         })
//         .catch(function (err) {
//             res.send(err)
//             // POST failed...
//         });
// })

router.post('/user/forgot/password', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/forgot/password`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
});

router.post('/reset/user/password', (req, res) => {
    const { body } = req;
    request({
        method: 'POST',
        json: true, // Automatically stringifies the body to JSON
        body: body.data,
        headers: {
            'x-auth-token': req.headers['x-auth-token'],
        },
        uri: `${BASE_URL}/user/forgot/password/verify`
    })
        .then((resp) => {
            res.send(resp)
        })
        .catch(function (err) {
            res.send(err)
        });
});

router.post('/book/image/upload', (req, res) => {
  // output the headers
  // capture the encoded form data
  let str = ''
  req.on('data', (chunk) => {
    str += chunk
  });

  req.on('end', () => {
    const formReq = reqst({
      method: 'POST',
      uri: `${BASE_URL}/book/image/upload`,
    }, (err, resp, body) => {
      console.log(body)
    });
    const form = formReq.form();
    form.append('files', str, {
      filename: 'myfile.jpeg',
      contentType: 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
    });
  })

  // var form = new formidable.IncomingForm();
  // form.parse(req, function (err, fields, files) {
  //   const formReq = reqst({
  //     method: 'POST',
  //     uri: `${BASE_URL}/book/image/upload`,
  //   }, (err, resp, body) => {
  //     console.log(body)
  //   })
  //   const form = formReq.form();
  //   form.append('files', fs.createReadStream(files.file), {
  //     filename: 'myfile.jpeg',
  //     contentType: 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
  //   });
  // });
})

export default router
