/// @flow
import serealize from 'serialize-javascript'
import pick from 'lodash/pick'

type args = {
	// rendered to string application
	app: string,
	// Styled components' styles
	// react-async-component state
	asyncState: Object,
    css: string,
	// react-helmet
	helmet: Object,
	// redux preloaded state
	initialState: Object,
	// client assets manifest
	assets: Object
}

const HTMLComponent = ({css, asyncState, initialState, assets, app, helmet}: args) => {
	const stringifiedAsyncState: string = serealize(asyncState)
	const stringifiedState: string = serealize(initialState)
	const wrapFuncs = {
		css: ({path}) => `<link rel="stylesheet" href="${path}" />`,
		js: ({path}) => `<script src="${path}" type="text/javascript"></script>`
	}
	const assetsOrdered = ['manifest', 'polyfills', 'vendor', 'client']
	const getTags = assets => funcs => ext => {
		// sort assets to be injected in right order
		// const assetsOrdered = ['manifest', 'vendor', 'client']
		return Object.keys(assets)
			.filter(bundleName => assets[bundleName][ext])
			.sort((a, b) => assetsOrdered.indexOf(a) - assetsOrdered.indexOf(b))
			.map(bundleName => {
				const path = assets[bundleName][ext]
				return funcs[ext]({path})
			})
			.join('')
	}
	const getTagsFromAssets = getTags(pick(assets, assetsOrdered))(wrapFuncs)
	const cssTags = getTagsFromAssets('css')
	const jsTags = getTagsFromAssets('js')
	return `<html ${helmet.htmlAttributes.toString()}>
			<head>
				${helmet.title.toString()}
				${helmet.meta.toString()}
				${helmet.base.toString()}
				${helmet.link.toString()}
				${helmet.noscript.toString()}
				<script async type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtm4Ep60qZa1DYd33fHw_hKsACxmTZ-OY&libraries=places"></script>
				${css}
				${cssTags}
			<head>
			<body>
				<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140028295-1"></script>
				<script>
  					window.dataLayer = window.dataLayer || [];
  					function gtag(){
  					    window.dataLayer.push(arguments);
  					}
  					gtag('js', new Date());
  					gtag('config', 'UA-140028295-1');
			</script>
			<script>window.__ASYNC_STATE__ = ${stringifiedAsyncState}</script>
			<script>window.__INITIAL_STATE__ = ${stringifiedState}</script>
			<div id="app"></div>
			${jsTags}
			</body>
			</html>`
}

export default HTMLComponent
