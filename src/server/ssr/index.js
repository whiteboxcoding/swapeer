/**
 * @flow
 * @desc
 */
import React from 'react'
import Helmet from 'react-helmet'
import {renderToString} from 'react-dom/server'
import {configureRootComponent, configureApp} from 'common/app'
import asyncBootstrapper from 'react-async-bootstrapper'
import {ServerStyleSheet, StyleSheetManager} from 'styled-components'
import {AsyncComponentProvider, createAsyncContext} from 'react-async-component'
import HTMLComponent from './HTMLComponent'
import {matchPath} from 'react-router'
import {getRouterRoutes} from 'routing'
import getAssets from './stats'
// import ContextProvider from '../../client/ContextProvider.js'

export default async (req: express$Request, res: express$Response) => {
	const assets = await getAssets()
	const initialState: Object = {}
    const sheet = new ServerStyleSheet()
	const location: string = req.url
	const context = {}
	const {store, history} = configureApp(initialState)
	const ua = req.headers['user-agent']
	const isMobile = /mobile/i.test(ua)
	const RootComponent: React$Node = configureRootComponent({
		store,
		history,
		SSR: {location, context},
		isMobile
	})
	const asyncContext = createAsyncContext()

	// const css = new Set()
	// const cssContext = { insertCss: (...styles) =>
	// 	styles.forEach(style => css.add(style._getCss()))}

	const app = (
		<AsyncComponentProvider asyncContext={asyncContext}>
            <StyleSheetManager sheet={sheet.instance}>{RootComponent}</StyleSheetManager>
		</AsyncComponentProvider>
	)

	const routes = getRouterRoutes()
	// if true - > throw 404, if match found -> 200
	const noRequestURLMatch = !routes.filter(r => !!r.path).find(r => matchPath(req.url, r))

	asyncBootstrapper(app).then(() => {
		const renderedApp = renderToString(app)
		const helmet = Helmet.renderStatic();
        const css: string = sheet.getStyleTags();
        const isLoggedin = (req.session && req.session.user && req.session.user.id) ? true : false;
        const loginState = {
            loggedIn: isLoggedin
        }
		const preloadedState: Object = store.getState()
        const updatedState = isLoggedin ? {
            ...preloadedState,
            login: loginState
        } : preloadedState
		const responseStatusCode = noRequestURLMatch ? 404 : 200
		const asyncState = asyncContext.getState()
		const props = {
            css,
			assets,
			asyncState,
            initialState: updatedState,
			app: renderedApp,
			helmet
		}
		res.status(responseStatusCode).send(HTMLComponent(props))
	})
}
