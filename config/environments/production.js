/**
 * Development Environment Configuration
 */

module.exports = {
  env: 'prod',
  server: {
      base: 'http://localhost:4000',
  },
  frontendBase: 'http://35.193.49.40',
};
