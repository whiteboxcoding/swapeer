/**
 * Development Environment Configuration
 */

module.exports = {
  env: 'dev',
  server: {
    base: 'http://localhost:4000',
  },
  frontendBase: 'http://localhost:4000',
};
